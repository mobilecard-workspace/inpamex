package com.addcel.inpamex.task;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.addcel.inpamex.data.dao.TransferenciasInpamexDao;
import com.addcel.inpamex.data.entity.TransferenciasInpamex;
import com.addcel.inpamex.payload.ws.inpamex.ClientResponseConfirmTransfer;
import com.addcel.inpamex.service.ws.inpamex.InpamexWsService;

@Component
public class ScheduledTask {

	private final static Logger logger = Logger.getLogger(ScheduledTask.class);

	@Value("${checktransfers.dias}")
	private String diasVencimiento;
	
	@Value("${trans.estatus.codigo.disponible}")
	private String transCodigoEstatusDisponible;
	
	@Value("${trans.estatus.codigo.rechazada}")
	private String transCodigoEstatusRechazada;
	
	@Value("${trans.estatus.desc.rechazada}")
	private String transDescEstatusRechazada;

	@Autowired
	private TransferenciasInpamexDao transferenciasInpamexDao;
	
	@Autowired
	private InpamexWsService inpamexWsService;

	@Scheduled(cron = "${checktransfers.horario}")
	public int checarTransferencias() {

		logger.info("####################################################");
		logger.info("STARTING CHECK TRANSFERENCIAS VENCIDAS");
		
		int contador = 0;

		try {
			
			Long numeroDiasVencimiento = Long.parseLong(diasVencimiento);

			List<Optional<TransferenciasInpamex>> listaTransferencias = new ArrayList<Optional<TransferenciasInpamex>>();
			ResponseEntity<ClientResponseConfirmTransfer> responseEntityInpamex = null;
			
			LocalDate fechaActual = LocalDate.now();
			
			LocalDate fechaRegistro = fechaActual.minus(numeroDiasVencimiento, ChronoUnit.DAYS);
//			LocalDate fechaRegistro = fechaActual.minus(31L, ChronoUnit.DAYS);

			String comentarioCancelada = "TRANSFERENCIA RECHAZADA POR EXCEDER LOS 30 DIAS PERMITIDOS, FECHA DE CANCELACION - " + fechaActual;
			
			logger.info("DIAS DE VENCIMIENTO - " + numeroDiasVencimiento);
			logger.info("FECHA ACTUAL - " + fechaActual);
			logger.info("FECHA REGISTRO - " + fechaRegistro);

			listaTransferencias = transferenciasInpamexDao.findTransfersVencidas(transCodigoEstatusDisponible.charAt(0),fechaRegistro.toString());

			if (!listaTransferencias.isEmpty()) {
				for (Optional<TransferenciasInpamex> optional : listaTransferencias) {
					logger.info("TRANSFERENCIA VENCIDA ID - " + optional.get().getIdTransferencia());
					
//					Actualizando Estatus de Transferencia a Rechazada
					TransferenciasInpamex transferenciasInpamex = optional.get();
					
					transferenciasInpamex.setEstatus(transCodigoEstatusRechazada.charAt(0));
					transferenciasInpamex.setEstatusDescripcion(transDescEstatusRechazada);
					transferenciasInpamex.setComentario(comentarioCancelada);
					
					transferenciasInpamexDao.save(transferenciasInpamex);
					
//					Llamando WS Inpamex para Transferencia Rechazada
					responseEntityInpamex = inpamexWsService.confirmTransfer(optional.get().getIdTransferencia());
					
					logger.info("ACTUALIZANDO ESTATUS DE TRANSFERENCIA A RECHAZADA - " + optional.get().getIdTransferencia());
					contador = contador + 1;
				}
			}
			logger.info("END CHECK TRANSFERENCIAS VENCIDAS TOTAL DE TRANSFERENCIA VENCIDAS - " + contador);
		} catch (Exception e) {
			logger.error("ERROR AL OBTENER TRANSFERENCIAS VENCIDAS - " + e.getLocalizedMessage());
		}
		return contador;
	}

}
