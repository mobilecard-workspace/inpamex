package com.addcel.inpamex.payload.ws.inpamex;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ClientResponseConfirmTransfer implements Serializable {
	
	private static final long serialVersionUID = -5517681623087259918L;
	
	private String idMto;
	private String estatus;
	private String estatusDescripcion;

}
