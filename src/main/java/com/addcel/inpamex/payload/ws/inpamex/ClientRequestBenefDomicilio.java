package com.addcel.inpamex.payload.ws.inpamex;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ClientRequestBenefDomicilio implements Serializable {

	private static final long serialVersionUID = -521829438075951077L;

	private String calle;
	private String noExterior;
	private String noInterior;
	private String municipio;
	private String colonia;
	private String cp;
	private String ciudad;
	private String estado;
	private String pais;

}
