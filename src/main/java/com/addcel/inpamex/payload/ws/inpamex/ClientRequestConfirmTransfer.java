package com.addcel.inpamex.payload.ws.inpamex;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ClientRequestConfirmTransfer implements Serializable {

	private static final long serialVersionUID = -1315583323375782141L;

	private String idMto;
	private Long idUsuario;
	private String noAutorizacion;
	private String estatus;
	private String estatusDescripcion;

	private ClientRequestBeneficiario beneficiario;
	private ClientRequestBenefDoc docRemittance;
}
