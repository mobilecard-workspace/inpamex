package com.addcel.inpamex.payload.ws.inpamex;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ClientRequestBeneficiario implements Serializable {

	private static final long serialVersionUID = 77041773283278172L;

	private String birthday;
	private Integer nacionalidad;
	private String paisNacimiento;
	private String estadoNacimiento;
	private String genero;
	private String telefono;
	private String claveActEconomica;

	private ClientRequestBenefDomicilio domicilio;

}
