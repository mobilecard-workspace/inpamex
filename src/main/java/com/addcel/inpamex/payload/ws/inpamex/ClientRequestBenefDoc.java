package com.addcel.inpamex.payload.ws.inpamex;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ClientRequestBenefDoc implements Serializable {
	
	private static final long serialVersionUID = -1908319595823949616L;
	
	private String tipoDocumento;
	private String noDocumento;

}
