package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TarjetaResponse implements Serializable {
	
	private static final long serialVersionUID = 5397392921144569487L;
	
	private Long idTarjeta;
	private Long idUsuario;
	private Double saldo;
	private Character estatus;
	private String estatusDescripcion;
	
	private List<String> errors;
	
	private int idError;
	private String mensajeError;
	private String nombreCompleto;

}
