package com.addcel.inpamex.payload.response;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TarjetaTransaccionCodigoBarras implements Serializable {
	
	private static final long serialVersionUID = 6092780105257051999L;
	
	@NotEmpty(message = "EL VALOR codigoBarras ES REQUERIDO")
	private String codigoBarras;
	
	@NotNull(message = "EL VALOR monto ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR monto DEBE SER MAYOR O IGUAL A 1")
	private Double monto;
	
	@NotEmpty(message = "EL VALOR concepto ES REQUERIDO")
	private String concepto;
	
	@NotEmpty(message = "EL VALOR claveCadena ES REQUERIDO")
	private String claveCadena;

}
