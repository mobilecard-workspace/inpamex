package com.addcel.inpamex.payload.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TransferAsignResponse implements Serializable {
	
	private static final long serialVersionUID = -2218556674561085372L;
	
	private Long idTransferencia;
	private Double montoTransferencia;
	private ConfirmTransfer confirmTransfer;

}
