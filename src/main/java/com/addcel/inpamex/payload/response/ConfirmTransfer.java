package com.addcel.inpamex.payload.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ConfirmTransfer implements Serializable {

	private static final long serialVersionUID = 595486904550888402L;
	
	private String codigo;
	private String descripcion;

}
