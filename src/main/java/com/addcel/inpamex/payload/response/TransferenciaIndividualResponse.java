package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TransferenciaIndividualResponse implements Serializable {

	private static final long serialVersionUID = 7921715265321248156L;

	private Long idTransferencia;
	private Long idUsuario;
	private String idMto;
	private Long idTarjeta;
	private String noAutorizacionTransferencia;
	private Double montoOriginal;
	private Double montoFinal;
	private Double tipoCambio;
	private Date fecha;
	private Character estatus;
	private String estatusDescripcion;
	
	private String remitenteNombre;
	private String remitenteApaterno;
	private String remitenteAmaterno;
	
	private String beneficiarioNombre;
	private String beneficiarioApaterno;
	private String beneficiarioAmaterno;
	
	private String comentario;

	private List<String> errors;

	private int idError;
	private String mensajeError;

}
