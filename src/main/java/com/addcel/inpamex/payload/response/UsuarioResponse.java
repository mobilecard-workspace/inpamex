package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class UsuarioResponse implements Serializable {
	
	private static final long serialVersionUID = -6362473498994022997L;

	private Long idUsuario;
	private String nombre;
	private String apaterno;
	private String amaterno;
	
	private List<String> errors;
	
	private int idError;
	private String mensajeError;
	
}
