package com.addcel.inpamex.payload.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TarjetaIndividualResponse implements Serializable {
	
	private static final long serialVersionUID = -242423030534428735L;
	
	private Long idTarjeta;
	private Long idUsuario;
	private String numeroTarjeta;
	private Double saldo;
	private Character estatus;
	private String estatusDescripcion;

}
