package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TransaccionGenericResponse implements Serializable {
	
	private static final long serialVersionUID = 3829743877843415890L;
	
	private Long idTransaccion;
	private Long idUsuario;
	private Long idTransferencia;
	private Long idTarjeta;
	private String idMto;
	private String tipo;
	private Double monto;
	private String concepto;
	private String claveCadena;
	private Date fecha;
	private Character estatus;
	private String estatusDescripcion;
	private String noAutorizacion;
	private String comentario;

}
