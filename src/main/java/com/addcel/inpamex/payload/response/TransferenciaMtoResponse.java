package com.addcel.inpamex.payload.response;

import java.io.Serializable;

import com.addcel.inpamex.data.entity.TransferenciasInpamex;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TransferenciaMtoResponse implements Serializable {
	
	static final long serialVersionUID = -6359914714703589228L;
	
	public TransferenciasInpamex transferencia;
	
	private int idError;
	private String mensajeError;

}
