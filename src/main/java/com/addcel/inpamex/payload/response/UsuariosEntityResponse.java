package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.List;

import com.addcel.inpamex.data.entity.UsuariosInpamex;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class UsuariosEntityResponse implements Serializable {

	private static final long serialVersionUID = -1307744481314957802L;

	private List<UsuariosInpamex> usuario;

	private int idError;
	private String mensajeError;

}
