package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TarjetasGeneralResponse implements Serializable {

	private static final long serialVersionUID = 1041937037459368460L;

	private List<TarjetaIndividualResponse> tarjetas;
	
	private List<String> errors;
	
	private int idError;
	private String mensajeError;
	
}
