package com.addcel.inpamex.payload.response;

import java.io.Serializable;

import com.addcel.inpamex.data.entity.UsuariosInpamex;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class UsuarioEntityResponse implements Serializable {

	private static final long serialVersionUID = 675784670332983454L;
	
	private UsuariosInpamex usuario;
	
	private int idError;
	private String mensajeError;

}
