package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class CatalogoResponse implements Serializable {

	private static final long serialVersionUID = 7319567557832759980L;

	private List<CatalogoGeneralResponse> response;

	private List<String> errors;

	private int idError;
	private String mensajeError;

}
