package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TransaccionFullResponse implements Serializable {
	
	private static final long serialVersionUID = 319260912001683972L;
	
	private List<TransaccionGenericResponse> transacciones;
	
	private List<String> errors;

	private int idError;
	private String mensajeError;

}
