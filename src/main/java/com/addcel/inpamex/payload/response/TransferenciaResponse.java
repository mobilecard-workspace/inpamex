package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TransferenciaResponse implements Serializable {

	private static final long serialVersionUID = 4232227797548766846L;
	
	private Long idTransferencia;
	private Long idUsuario;
	private Long idTarjeta;
	private Long idTransaccion;
	private String idMto;
	private String noAutorizacionTransferencia;
	private String noAutorizacionTransaccion;
	private Character estatus;
	private String estatusDescripcion;

	private List<String> errors;
	
	private int idError;
	private String mensajeError;
	
}
