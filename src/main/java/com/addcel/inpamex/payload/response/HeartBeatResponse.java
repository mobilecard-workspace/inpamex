package com.addcel.inpamex.payload.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class HeartBeatResponse implements Serializable {
	
	private static final long serialVersionUID = -1164435444958259073L;
	
	private int idError;
	private String mensajeError;

}
