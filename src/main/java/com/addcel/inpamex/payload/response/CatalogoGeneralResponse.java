package com.addcel.inpamex.payload.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class CatalogoGeneralResponse implements Serializable {
	
	private static final long serialVersionUID = 3074511272873807084L;
	
	private Long id;
	private String clave;
	private String descripcion;
	private Character estatus;

}
