package com.addcel.inpamex.payload.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TransaccionResponse implements Serializable {

	private static final long serialVersionUID = 7107389602916158427L;

	private Long idTransaccion;
	private Long idUsuario;
	private Long idTarjeta;
	private Double saldo;
	private String noAutorizacion;
	private Character estatus;
	private String estatusDescripcion;
	private String nombreCompleto;
	private List<String> errors;

	private int idError;
	private String mensajeError;

}
