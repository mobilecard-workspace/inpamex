package com.addcel.inpamex.payload.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ListaCatalogos implements Serializable {
	
	private static final long serialVersionUID = 837638751377005252L;
	
	@Valid
	@NotNull(message = "EL OBJETO catalogos ES REQUERIDO")
	@NotEmpty(message = "EL ARREGO catalogos NO PUEDE ESTAR VACIO")
	private List<CatalogoRequest> catalogos;

}
