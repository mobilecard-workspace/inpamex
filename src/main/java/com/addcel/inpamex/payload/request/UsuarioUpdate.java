package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class UsuarioUpdate implements Serializable {
	
	private static final long serialVersionUID = 489050602388989053L;
	
	@NotNull(message = "EL VALOR idUsuario DE BENEFICIARIO ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR idUsuario DE BENEFICIARIO DEBE SER MAYOR A 0")
	private Long idUsuario;
	
	@NotEmpty(message = "EL VALOR nombre ES REQUERIDO")
	@Size(max = 512)
	private String nombre;
	
	@NotEmpty(message = "EL VALOR apaterno ES REQUERIDO")
	@Size(max = 512)
	private String apaterno;
	
	@NotEmpty(message = "EL VALOR amaterno ES REQUERIDO")
	@Size(max = 512)
	private String amaterno;

	@NotEmpty(message = "EL VALOR fechaNacimiento ES REQUERIDO")
	@Pattern(message="LA fechaNacimiento NO CUMPLE CON EL FORMATO dd-MM-yyyy" , regexp="\\d{2}-\\d{2}-\\d{4}")
	private String fechaNacimiento;
	
	@Min(value = 0, message = "EL VALOR nacionalidad DEBE SER MAYOR O IGUAL A 0")
	@Max(value = 2, message = "EL VALOR nacionalidad DE DEBE SER MAYOR A 2")
	private Integer nacionalidad;
	
	@NotEmpty(message = "EL VALOR paisNacimiento DE SUCURSAL ES REQUERIDO")
	@Pattern(message="EL VALOR paisNacimiento DEBE CORRESPONDER AL ISO ALFANUMERICO DE 3 CARACTERES MEX, USA, ETC" , regexp="\\b[a-zA-Z]{3}\\b")
	private String paisNacimiento;
	
	@NotNull(message = "EL VALOR estadoNacimiento NO PUEDE SER NULO")
	@Size(max = 50)
	private String estadoNacimiento;
	
	@NotEmpty(message = "EL VALOR genero ES REQUERIDO")
	@Pattern(message="EL VALOR genero DEBE SER F O M" , regexp="\\b[F|M]{1}\\b")
	private String genero;
	
	@Valid
	@NotNull(message = "EL OBJETO domicilio ES REQUERIDO")
	private Domicilio domicilio;
	
	@NotEmpty(message = "EL VALOR telefono ES REQUERIDO")
	@Size(max = 16)
	private String telefono;

	@NotNull(message = "EL VALOR curp NO PUEDE SER NULO")
	@Size(max = 32)
	private String curp;
	
	@NotNull(message = "EL VALOR rfc NO PUEDE SER NULO")
	@Size(max = 32)
	private String rfc;
	
	@NotNull(message = "EL VALOR email NO PUEDE SER NULO")
	@Size(max = 255)
	private String email;
	
	@NotEmpty(message = "EL VALOR claveActividadEconomica ES REQUERIDO")
	@Size(max = 7)
	private String claveActividadEconomica;
	
	@NotEmpty(message = "EL VALOR tipoDocumento apaterno ES REQUERIDO")
	@Size(max = 50)
	private String tipoDocumento;
	
	@NotEmpty(message = "EL VALOR noDocumento apaterno ES REQUERIDO")
	@Size(max = 50)
	private String noDocumento;

}
