package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Tarjeta implements Serializable {
	
	private static final long serialVersionUID = -6512874964775103164L;

	@NotEmpty(message = "EL VALOR numero DE TARJETA ES REQUERIDO")
	@Size(max = 60, message = "EL VALOR DE numero NO DEBE SUPERAR LOS 60 CARACTERES")
	private String numero;
	
	@NotEmpty(message = "EL VALOR serie DE TARJETA ES REQUERIDO")
	@Size(max = 60, message = "EL VALOR DE serie NO DEBE SUPERAR LOS 60 CARACTERES")
	private String serie;
	
	@NotEmpty(message = "EL VALOR codigoBarras DE TARJETA ES REQUERIDO")
	@Size(max = 60, message = "EL VALOR DE codigoBarras NO DEBE SUPERAR LOS 60 CARACTERES")
	private String codigoBarras;

	@Size(max = 3, message = "EL VALOR DE CODIGO VERIFICACION NO DEBE SUPERAR LOS 3 CARACTERES")
	private String codigoVerificacion;
	
	@NotEmpty(message = "EL VALOR claveCadena DE TARJETA ES REQUERIDO")
	@Size(max = 512, message = "EL VALOR DE claveCadena NO DEBE SUPERAR LOS 512 CARACTERES")
	private String claveCadena;

}
