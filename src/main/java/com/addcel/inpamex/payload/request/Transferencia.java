package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Transferencia implements Serializable {
	
	private static final long serialVersionUID = -1081291571353988236L;
	
	@NotEmpty(message = "EL VALOR idMto ES REQUERIDO")
	private String idMto;
	
	@NotNull(message = "EL VALOR montoOriginal ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR montoOriginal DEBE SER MAYOR O IGUAL A 1")
	private Double montoOriginal;
	
	@NotNull(message = "EL VALOR montoFinal ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR montoFinal DEBE SER MAYOR O IGUAL A 1")
	private Double montoFinal;
	
	@NotNull(message = "EL VALOR tipoCambio ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR tipoCambio DEBE SER MAYOR O IGUAL A 1")
	private Double tipoCambio;
	
	@NotEmpty(message = "EL VALOR fecha ES REQUERIDO")
	@Pattern(message="LA FECHA NO CUMPLE CON EL FORMATO dd/MM/yyyy HH:mm:ss" , regexp="^\\s*(3[01]|[12][0-9]|0?[1-9])\\/(1[012]|0?[1-9])\\/((?:19|20)\\d{2})\\s.*")
	private String fecha;
	
	@Valid
	@NotNull(message = "EL OBJETO remitente ES REQUERIDO")
	private Remitente remitente;
	 
	@Valid
	@NotNull(message = "EL OBJETO beneficiario ES REQUERIDO")
	private Beneficiario beneficiario;

}
