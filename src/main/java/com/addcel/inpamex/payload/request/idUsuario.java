package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class idUsuario implements Serializable {
	
	private static final long serialVersionUID = 864414770277916954L;
	
	@NotNull(message = "EL VALOR idUsuario ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR idUsuario DEBE SER MAYOR O IGUAL A 1")
	private Long idUsuario;

}
