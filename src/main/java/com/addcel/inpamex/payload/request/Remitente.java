package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Remitente implements Serializable {
	
	private static final long serialVersionUID = -2084541346522436435L;

	@NotEmpty(message = "EL VALOR nombre DE REMITENTE ES REQUERIDO")
	@Size(max = 512)
	private String nombre;
	
	@NotEmpty(message = "EL VALOR apaterno DE REMITENTE ES REQUERIDO")
	@Size(max = 512)
	private String apaterno;

	@Size(max = 512)
	private String amaterno;

}
