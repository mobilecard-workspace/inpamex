package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TarjetaTransaccion implements Serializable {
	
	private static final long serialVersionUID = 5708297093385041400L;
	
	@NotNull(message = "EL VALOR idUsuario ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR idUsuario DEBE SER MAYOR O IGUAL A 1")
	private Long idUsuario;
	
	@NotNull(message = "EL VALOR idTarjeta ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR idTarjeta DEBE SER MAYOR O IGUAL A 1")
	private Long idTarjeta;
	
	@NotNull(message = "EL VALOR monto ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR monto DEBE SER MAYOR O IGUAL A 1")
	private Double monto;
	
	@NotEmpty(message = "EL VALOR concepto ES REQUERIDO")
	private String concepto;
	
	@NotEmpty(message = "EL VALOR claveCadena ES REQUERIDO")
	private String claveCadena;
	

}
