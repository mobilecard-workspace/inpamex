package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Domicilio implements Serializable {
	
	private static final long serialVersionUID = -7759771850284563793L;
	
	@NotEmpty(message = "EL VALOR calle ES REQUERIDO")
	@Size(max = 255)
	private String calle;
	
	@NotEmpty(message = "EL VALOR noExterior ES REQUERIDO")
	@Size(max = 80)
	private String noExterior;
	
	@NotNull(message = "EL VALOR noInterior NO PUEDE SER NULO")
	@Size(max = 80)
	private String noInterior;
	
	@NotEmpty(message = "EL VALOR municipio ES REQUERIDO")
	@Size(max = 255)
	private String municipio;
	
	@NotEmpty(message = "EL VALOR colonia ES REQUERIDO")
	@Size(max = 255)
	private String colonia;
	
	@NotEmpty(message = "EL VALOR cp ES REQUERIDO")
	@Pattern(message="EL VALOR cp DEBE SER DE 5 DIGITOS" , regexp="\\b[0-9]{5}\\b")
	private String cp;
	
	@NotEmpty(message = "EL VALOR ciudad ES REQUERIDO")
	@Size(max = 50)
	private String ciudad;
	
	@NotEmpty(message = "EL VALOR estado ES REQUERIDO")
	@Size(max = 32)
	private String estado;
	
	@NotEmpty(message = "EL VALOR pais ES REQUERIDO")
	@Pattern(message="EL VALOR pais DEBE CORRESPONDER AL ISO ALFANUMERICO DE 3 CARACTERES MEX, USA, ETC" , regexp="\\b[a-zA-Z]{3}\\b")
	private String pais;
	
//	@NotEmpty(message = "EL VALOR pais DEL DOMICILIO DEL BENEFICIARIO ES REQUERIDO")
//	@Pattern(message="EL VALOR pais DEBE CORRESPONDER AL ISO ALFANUMERICO DE 3 CARACTERES MEX, USA, ETC" , regexp="\\b[a-zA-Z]{3}\\b")
//	private String pais;
//	
//	@NotEmpty(message = "EL VALOR estado DEL DOMICILIO DEL BENEFICIARIO ES REQUERIDO")
//	@Pattern(message="EL VALOR estado DEBE CORRESPONDER AL ISO ALFANUMERICO DE 3 CARACTERES AGU, CMX, ETC" , regexp="\\b[a-zA-Z]{3}\\b")
//	private String estado;
//	
//	@NotEmpty(message = "EL VALOR ciudad DEL DOMICILIO DEL BENEFICIARIO ES REQUERIDO")
//	private String ciudad;
//	
//	@NotEmpty(message = "EL VALOR cp DEL DOMICILIO DEL BENEFICIARIO ES REQUERIDO")
//	@Pattern(message="EL VALOR cp DEL DOMICILIO DEL BENEFICIARIO DEBE SER DE 5 DIGITOS" , regexp="\\b[0-9]{5}\\b")
//	private String cp;
//	
//	@NotEmpty(message = "EL VALOR calle DEL DOMICILIO DEL BENEFICIARIO ES REQUERIDO")
//	@Size(max = 512, message = "EL VALOR calle DEL DOMICILIO DEL BENEFICIARIO NO DEBER SUPERAR LOS 512 CARACTERES")
//	private String calle;
//	
//	@NotNull(message = "EL VALOR noExterior DEL DOMICILIO DEL BENEFICIARIO ES REQUERIDO")
//	@Min(value = 0, message = "EL VALOR noExterior DEL DOMICILIO DEL BENEFICIARIO DEBE SER MAYOR O IGUAL A 0")
//	private Integer noExterior;
//	
//	@NotNull(message = "EL VALOR noInterior DEL DOMICILIO DEL BENEFICIARIO ES REQUERIDO")
//	@Min(value = 0, message = "EL VALOR noInterior DEL DOMICILIO DEL BENEFICIARIO DEBE SER MAYOR O IGUAL A 0")
//	private Integer noInterior;

}
