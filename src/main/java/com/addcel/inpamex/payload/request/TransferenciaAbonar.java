package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TransferenciaAbonar implements Serializable {
	
	private static final long serialVersionUID = -1399295618446127898L;
	
	private String idMto;
	private Long idUsuario;

}
