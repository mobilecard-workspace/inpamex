package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Beneficiario implements Serializable {

	private static final long serialVersionUID = -3069867964996868412L;

	@NotNull(message = "EL VALOR idUsuario DE BENEFICIARIO ES REQUERIDO")
	@Min(value = 0, message = "EL VALOR idUsuario DEBE SER MAYOR O IGUAL A 0")
	private Long idUsuario;
	
	@NotEmpty(message = "EL VALOR nombre DE BENEFICIARIO ES REQUERIDO")
	@Size(max = 512)
	private String nombre;
	
	@NotEmpty(message = "EL VALOR apaterno DE BENEFICIARIO ES REQUERIDO")
	@Size(max = 512)
	private String apaterno;
	
	@Size(max = 512)
	private String amaterno;

}
