package com.addcel.inpamex.payload.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ListaTarjetas implements Serializable {
	
	private static final long serialVersionUID = -197989940928779978L;
	
	@Valid
	@NotNull(message = "EL OBJETO tarjetas ES REQUERIDO")
	@NotEmpty(message = "EL ARREGO tarjetas NO PUEDE ESTAR VACIO")
	private List<Tarjeta> tarjetas;

}
