package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class CatalogoRequest implements Serializable {

	private static final long serialVersionUID = 3542879679574061356L;

	@NotEmpty(message = "EL VALOR clave DEL CATALOGO ES REQUERIDO")
	@Size(max = 128, message = "EL VALOR DE clave NO DEBE SUPERAR LOS 128 CARACTERES")
	private String clave;

	@NotEmpty(message = "EL VALOR descripcion DEL CATALOGO ES REQUERIDO")
	@Size(max = 512, message = "EL VALOR DE descripcion NO DEBE SUPERAR LOS 512 CARACTERES")
	private String descripcion;
}
