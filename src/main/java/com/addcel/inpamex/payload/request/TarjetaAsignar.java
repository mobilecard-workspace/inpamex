package com.addcel.inpamex.payload.request;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class TarjetaAsignar implements Serializable {
	
	private static final long serialVersionUID = -4870870867445923691L;

	@NotNull(message = "EL VALOR idUsuario DE BENEFICIARIO ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR idUsuario DE BENEFICIARIO DEBE SER MAYOR A 0")
	private Long idUsuario;
	
	@NotNull(message = "EL VALOR idTarjeta DE BENEFICIARIO ES REQUERIDO")
	@Min(value = 1, message = "EL VALOR idTarjeta DE BENEFICIARIO DEBE SER MAYOR A 0")
	private Long idTarjeta;

}
