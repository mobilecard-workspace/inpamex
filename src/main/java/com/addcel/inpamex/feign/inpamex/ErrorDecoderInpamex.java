package com.addcel.inpamex.feign.inpamex;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import com.addcel.inpamex.utils.Commons;

import feign.Response;
import feign.codec.ErrorDecoder;

@Component
public class ErrorDecoderInpamex implements ErrorDecoder {
	
	private final static Logger logger = Logger.getLogger(ErrorDecoderInpamex.class);
	
	@Autowired
	private Commons commons;
	
	@Override
	public Exception decode(String methodKey, Response response) {
		String message = "";
		String serviceError = "";
		String fullMessage = "";
		
		switch (response.status()) {
		case 400:
			message = "Bad Request or Failed Transaction";
			serviceError = commons.parseFeignError(response);
			fullMessage = message + " - " + serviceError;

			logger.error(message);
			logger.error("Response: " + serviceError);
			logger.error("codigo de status: "+response.status()+", metodo: "+methodKey);
			return new ResponseStatusException(HttpStatus.valueOf(response.status()), fullMessage);
		case 401:
			message = "Unauthorized";
			serviceError = commons.parseFeignError(response);
			fullMessage = message + " - " + serviceError;
			
			logger.error(message);
			logger.error("Response: " + serviceError);
			logger.error("codigo de status: "+response.status()+", metodo: "+methodKey);
			return new ResponseStatusException(HttpStatus.valueOf(response.status()), fullMessage);
		case 404:
			message = "Not Found";
			serviceError = commons.parseFeignError(response);
			fullMessage = message + " - " + serviceError;
			
			logger.error(message);
			logger.error("Response: " + serviceError);
			logger.error("codigo de status: "+response.status()+", metodo: "+methodKey);
			return new ResponseStatusException(HttpStatus.valueOf(response.status()), fullMessage);
		case 500:
			message = "Internal Error";
			serviceError = commons.parseFeignError(response);
			fullMessage = message + " - " + serviceError;
			
			logger.error(message);
			logger.error("Response: " + serviceError);
			logger.error("codigo de status: "+response.status()+", metodo: "+methodKey);
			return new ResponseStatusException(HttpStatus.valueOf(response.status()), fullMessage);
		case 504:
			message = "Gateway Timeout";
			serviceError = commons.parseFeignError(response);
			fullMessage = message + " - " + serviceError;
			
			logger.error(message);
			logger.error("Response: " + serviceError);
			logger.error("codigo de status: "+response.status()+", metodo: "+methodKey);
			return new ResponseStatusException(HttpStatus.valueOf(response.status()), fullMessage);
		default:			
			return new Exception(response.reason());
		}
	}

}
