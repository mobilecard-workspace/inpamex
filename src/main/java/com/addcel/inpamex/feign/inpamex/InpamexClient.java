package com.addcel.inpamex.feign.inpamex;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

import com.addcel.inpamex.payload.ws.inpamex.ClientRequestConfirmTransfer;
import com.addcel.inpamex.payload.ws.inpamex.ClientResponseConfirmTransfer;

@FeignClient(name = "inpamexClient", url = "${inpamex.ws.url.base}", configuration = InpamexClientConfiguration.class)
public interface InpamexClient {
	
	@PostMapping(value = "${inpamex.ws.url.confirmacionPago}")
	ResponseEntity<ClientResponseConfirmTransfer> confirmTransfer(ClientRequestConfirmTransfer requestCinfirmTransfer);

}
