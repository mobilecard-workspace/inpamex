package com.addcel.inpamex.feign.inpamex;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;
import feign.auth.BasicAuthRequestInterceptor;
import feign.slf4j.Slf4jLogger;

@Configuration
public class InpamexClientConfiguration {
	
	@Value("${inpamex.ws.user}")
	private String inpamexUser;
	
	@Value("${inpamex.ws.pass}")
	private String inpamexPass;
	
	@Bean
	Logger feignLoggerLevel(){
		return new Slf4jLogger();
	}
	
	@Bean
	public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
		return new BasicAuthRequestInterceptor(inpamexUser, inpamexPass);
	}

}
