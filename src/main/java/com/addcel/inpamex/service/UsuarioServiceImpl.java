package com.addcel.inpamex.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.addcel.inpamex.data.dao.UsuariosInpamexDao;
import com.addcel.inpamex.data.entity.UsuariosInpamex;
import com.addcel.inpamex.payload.request.Usuario;
import com.addcel.inpamex.payload.request.UsuarioUpdate;
import com.addcel.inpamex.payload.response.UsuarioEntityResponse;
import com.addcel.inpamex.payload.response.UsuarioResponse;
import com.addcel.inpamex.payload.response.UsuariosEntityResponse;
import com.addcel.inpamex.utils.Utils;

@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService {
	
	@Value("${usuario.estatus.codigo.activo}")
	private String usuarioCodigoEstatusActivo;

	@Value("${usuario.estatus.desc.activo}")
	private String usuarioDescEstatusActivo;
	
	@Value("${usuario.estatus.codigo.inactivo}")
	private String usuarioCodigoEstatusInactivo;
	
	@Value("${usuario.estatus.desc.inactivo}")
	private String usuarioDescEstatusInactivo;

	@Value("${error.codigo.generico}")
	private String errorCodigoGenerico;

	@Value("${error.mensaje.generico}")
	private String errorMensajeGenerico;
	
	@Value("${error.codigo.usuario}")
	private String errorCodigoUsuario;

	@Value("${error.mensaje.usuario}")
	private String errorMensajeUsuario;
	
	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;

	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;

	@Value("${exito.codigo}")
	private String exitoCodigo;

	@Value("${exito.mensaje}")
	private String exitoMensaje;

	@Autowired
	private UsuariosInpamexDao usuariosInpamexDao;

	private final static Logger logger = Logger.getLogger(TarjetaServiceImpl.class);
	
	@Override
	public ResponseEntity<UsuarioResponse> saveUsuario(Usuario usuario) {
		
		logger.info("####################################################");
		logger.info("STARTING SAVE USUARIO - " + usuario.toString());
		
		ResponseEntity<UsuarioResponse> responseEntity = null;
		UsuarioResponse usuarioResponse = null;
		Optional<UsuariosInpamex> usuarioResult = Optional.empty();
		
		try {
			
//			Validando Existencia de Documentos
			logger.info("STARTING ENCONTRAR DOCUMENTO EN BD - " + usuario.getNoDocumento());
			usuarioResult = usuariosInpamexDao.findUsuarioDocumento(usuario.getNoDocumento().trim().toUpperCase());
			logger.info("END ENCONTRAR DOCUMENTO EN BD - " + usuarioResult.isPresent());
			
			if (usuarioResult.isPresent()) {
				logger.error("ERROR AL REALIZAR LA GUARDAR EL USUARIO EN LA BD DOCUMENTO REGISTRADO PREVIAMENTE - "
						+ usuario.getNoDocumento());
				String errorMensaje = errorMensajeGenerico + " - DOCUMENTO EXISTENTE";
				usuarioResponse = new UsuarioResponse();
				usuarioResponse.setIdError(Integer.parseInt(errorCodigoGenerico));
				usuarioResponse.setMensajeError(errorMensaje);
				return new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.BAD_REQUEST);
			} else {
			
				UsuariosInpamex usuariosInpamex = new UsuariosInpamex();
				Utils utils = new Utils();
				
				usuariosInpamex.setNombre(utils.removerAcentos(usuario.getNombre().toUpperCase().trim()));
				usuariosInpamex.setApaterno(utils.removerAcentos(usuario.getApaterno().toUpperCase().trim()));
				usuariosInpamex.setAmaterno(utils.removerAcentos(usuario.getAmaterno().toUpperCase().trim()));
				
				DateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy");
				String fechaNacimientoString = usuario.getFechaNacimiento();
				Date fechaNacimiento = sourceFormat.parse(fechaNacimientoString);
				usuariosInpamex.setFechaNacimiento(fechaNacimiento);
				
				usuariosInpamex.setNacionalidad(usuario.getNacionalidad());
				usuariosInpamex.setPaisNacimiento(usuario.getPaisNacimiento().toUpperCase().trim());
				usuariosInpamex.setEstadoNacimiento(this.validarDatoVacio(usuario.getEstadoNacimiento()));
				usuariosInpamex.setGenero(usuario.getGenero().toUpperCase().trim());
				
				usuariosInpamex.setDomicilioCalle(usuario.getDomicilio().getCalle().toUpperCase().trim());
				usuariosInpamex.setDomicilioNoExterior(usuario.getDomicilio().getNoExterior().toUpperCase().trim());
				usuariosInpamex.setDomicilioNoInterior(this.validarDatoVacio(usuario.getDomicilio().getNoInterior()));
				usuariosInpamex.setDomicilioMunicipio(usuario.getDomicilio().getMunicipio().toUpperCase().trim());
				usuariosInpamex.setDomicilioColonia(usuario.getDomicilio().getColonia().toUpperCase().trim());
				usuariosInpamex.setDomicilioCp(usuario.getDomicilio().getCp().toUpperCase().trim());
				usuariosInpamex.setDomicilioCiudad(usuario.getDomicilio().getCiudad().toUpperCase().trim());
				usuariosInpamex.setDomicilioEstado(usuario.getDomicilio().getEstado().toUpperCase().trim());
				usuariosInpamex.setDomicilioPais(usuario.getDomicilio().getPais().toUpperCase().trim());
				
				usuariosInpamex.setTelefono(usuario.getTelefono());
				usuariosInpamex.setCurp(this.validarDatoVacio(usuario.getCurp()));
				usuariosInpamex.setRfc(this.validarDatoVacio(usuario.getRfc()));
				usuariosInpamex.setEmail(this.validarDatoVacio(usuario.getEmail()));
				usuariosInpamex.setClaveActividadEconomica(usuario.getClaveActividadEconomica().toUpperCase().trim());
				usuariosInpamex.setTipoDocumento(usuario.getTipoDocumento().toUpperCase().trim());
				usuariosInpamex.setNoDocumento(usuario.getNoDocumento().toUpperCase().trim());
				
				usuariosInpamex.setFechaRegistro(new Date());
				usuariosInpamex.setEstatus(usuarioCodigoEstatusActivo.charAt(0));
				usuariosInpamex.setEstatusDescripcion(usuarioDescEstatusActivo);
				usuariosInpamex.setFechaModificacion(new Date());
				
				logger.info("GUARDANDO USUARIO EN BD");
				UsuariosInpamex usuariosInpamexResult = usuariosInpamexDao.save(usuariosInpamex);
				logger.info("END GUARDAR USUARIO EN BD");
				
				if (usuariosInpamexResult.getIdUsuario() != null & usuariosInpamexResult.getIdUsuario() != 0) {
				
					logger.info("END GUARDAR USUARIO EN BD IDUSUARIO - " + usuariosInpamexResult.getIdUsuario());
					usuarioResponse = new UsuarioResponse();
					
					usuarioResponse.setIdUsuario(usuariosInpamexResult.getIdUsuario());
					usuarioResponse.setNombre(usuariosInpamexResult.getNombre());
					usuarioResponse.setApaterno(usuariosInpamexResult.getApaterno());
					usuarioResponse.setAmaterno(usuariosInpamexResult.getAmaterno());
					
					usuarioResponse.setIdError(Integer.parseInt(exitoCodigo));
					usuarioResponse.setMensajeError(exitoMensaje);
					
					responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
							HttpStatus.CREATED);
					
				} else {
					
					logger.error("ERROR AL REALIZAR LA OPERACION GUARDAR USUARIO");
					usuarioResponse = new UsuarioResponse();

					usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
					usuarioResponse.setMensajeError(errorMensajeErrorInterno);
					responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
							HttpStatus.INTERNAL_SERVER_ERROR);
					
				}
				
			}
			
		} catch (Exception e) {
			logger.error("ERROR AL REALIZAR LA OPERACION GUARDAR USUARIO - " + e.getMessage());
			usuarioResponse = new UsuarioResponse();

			usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@Override
	public ResponseEntity<UsuarioResponse> updateUsuario(UsuarioUpdate usuario) {
		
		logger.info("####################################################");
		logger.info("STARTING UPDATE USUARIO - " + usuario.toString());
		
		ResponseEntity<UsuarioResponse> responseEntity = null;
		UsuarioResponse usuarioResponse = null;
		
		Optional<UsuariosInpamex> resultUsuario = Optional.empty();
		
		try {
			
//			Validando Existencia de Usuario
			logger.info("STARTING ENCONTRAR USUARIO EN BD - " + usuario.getIdUsuario());
			resultUsuario = usuariosInpamexDao.findById(usuario.getIdUsuario());
			logger.info("END ENCONTRAR USUARIO EN BD - " + resultUsuario.isPresent());

			if (!resultUsuario.isPresent()) {
				logger.error("ERROR AL REALIZAR LA OPERACION USUARIO NO ENCONTRADO EN LA BD - "
						+ usuario.getIdUsuario());
				usuarioResponse = new UsuarioResponse();
				usuarioResponse.setIdError(Integer.parseInt(errorCodigoUsuario));
				usuarioResponse.setMensajeError(errorMensajeUsuario);
				return new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.BAD_REQUEST);
			} else {
				
				UsuariosInpamex usuariosInpamex = new UsuariosInpamex();
				Utils utils = new Utils();
				
				usuariosInpamex.setIdUsuario(usuario.getIdUsuario());
				
				usuariosInpamex.setNombre(utils.removerAcentos(usuario.getNombre().toUpperCase().trim()));
				usuariosInpamex.setApaterno(utils.removerAcentos(usuario.getApaterno().toUpperCase().trim()));
				usuariosInpamex.setAmaterno(utils.removerAcentos(usuario.getAmaterno().toUpperCase().trim()));
				
//				usuariosInpamex.setNombre(usuario.getNombre().toUpperCase().trim());
//				usuariosInpamex.setApaterno(usuario.getApaterno().toUpperCase().trim());
//				usuariosInpamex.setAmaterno(usuario.getAmaterno().toUpperCase().trim());
				
				DateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy");
				String fechaNacimientoString = usuario.getFechaNacimiento();
				Date fechaNacimiento = sourceFormat.parse(fechaNacimientoString);
				usuariosInpamex.setFechaNacimiento(fechaNacimiento);
				
				usuariosInpamex.setNacionalidad(usuario.getNacionalidad());
				usuariosInpamex.setPaisNacimiento(usuario.getPaisNacimiento().toUpperCase().trim());
				usuariosInpamex.setEstadoNacimiento(this.validarDatoVacio(usuario.getEstadoNacimiento()));
				usuariosInpamex.setGenero(usuario.getGenero().toUpperCase().trim());
				
				usuariosInpamex.setDomicilioCalle(usuario.getDomicilio().getCalle().toUpperCase().trim());
				usuariosInpamex.setDomicilioNoExterior(usuario.getDomicilio().getNoExterior().toUpperCase().trim());
				usuariosInpamex.setDomicilioNoInterior(this.validarDatoVacio(usuario.getDomicilio().getNoInterior()));
				usuariosInpamex.setDomicilioMunicipio(usuario.getDomicilio().getMunicipio().toUpperCase().trim());
				usuariosInpamex.setDomicilioColonia(usuario.getDomicilio().getColonia().toUpperCase().trim());
				usuariosInpamex.setDomicilioCp(usuario.getDomicilio().getCp().toUpperCase().trim());
				usuariosInpamex.setDomicilioCiudad(usuario.getDomicilio().getCiudad().toUpperCase().trim());
				usuariosInpamex.setDomicilioEstado(usuario.getDomicilio().getEstado().toUpperCase().trim());
				usuariosInpamex.setDomicilioPais(usuario.getDomicilio().getPais().toUpperCase().trim());
				
				usuariosInpamex.setTelefono(usuario.getTelefono());
				usuariosInpamex.setCurp(this.validarDatoVacio(usuario.getCurp()));
				usuariosInpamex.setRfc(this.validarDatoVacio(usuario.getRfc()));
				usuariosInpamex.setEmail(this.validarDatoVacio(usuario.getEmail()));
				usuariosInpamex.setClaveActividadEconomica(usuario.getClaveActividadEconomica().toUpperCase().trim());
				usuariosInpamex.setTipoDocumento(usuario.getTipoDocumento().toUpperCase().trim());
				usuariosInpamex.setNoDocumento(usuario.getNoDocumento().toUpperCase().trim());
				
				usuariosInpamex.setFechaRegistro(new Date());
				usuariosInpamex.setEstatus(usuarioCodigoEstatusActivo.charAt(0));
				usuariosInpamex.setEstatusDescripcion(usuarioDescEstatusActivo);
				usuariosInpamex.setFechaModificacion(new Date());
				
				logger.info("ACTUALIZANDO USUARIO EN BD");
				UsuariosInpamex usuariosInpamexResult = usuariosInpamexDao.save(usuariosInpamex);
				logger.info("END ACTUALIZAR USUARIO EN BD ");
				
				if (usuariosInpamexResult.getIdUsuario() != null & usuariosInpamexResult.getIdUsuario() != 0) {
					
					logger.info("END GUARDAR USUARIO EN BD IDUSUARIO - " + usuariosInpamexResult.getIdUsuario());
					usuarioResponse = new UsuarioResponse();
					
					usuarioResponse.setIdUsuario(usuariosInpamexResult.getIdUsuario());
					usuarioResponse.setNombre(usuariosInpamexResult.getNombre());
					usuarioResponse.setApaterno(usuariosInpamexResult.getApaterno());
					usuarioResponse.setAmaterno(usuariosInpamexResult.getAmaterno());
					
					usuarioResponse.setIdError(Integer.parseInt(exitoCodigo));
					usuarioResponse.setMensajeError(exitoMensaje);
					
					responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
							HttpStatus.OK);
					
				} else {
					
					logger.error("ERROR AL REALIZAR LA OPERACION ACTUALIZAR USUARIO");
					usuarioResponse = new UsuarioResponse();

					usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
					usuarioResponse.setMensajeError(errorMensajeErrorInterno);
					responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
							HttpStatus.INTERNAL_SERVER_ERROR);
					
				}
				
			}
			
		} catch (Exception e) {
			logger.error("ERROR AL REALIZAR LA OPERACION ACTUALIZAR USUARIO - " + e.getMessage());
			usuarioResponse = new UsuarioResponse();

			usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
	}
	
	@Override
	public ResponseEntity<UsuarioResponse> disableUsuario(Long idUsuario) {
		
		logger.info("####################################################");
		logger.info("STARTING DESHABILITAR USUARIO IDUSUARIO  - " + idUsuario);

		ResponseEntity<UsuarioResponse> responseEntity = null;
		UsuarioResponse usuarioResponse = null;
		
		Optional<UsuariosInpamex> resultUsuario = Optional.empty();
		UsuariosInpamex usuariosInpamex = null;
		
		try {
			
//			Validando Existencia de Usuario
			logger.info("STARTING ENCONTRAR USUARIO EN BD - " + idUsuario);
			resultUsuario = usuariosInpamexDao.findById(idUsuario);
			logger.info("END ENCONTRAR USUARIO EN BD - " + resultUsuario.isPresent());

			if (!resultUsuario.isPresent()) {
				logger.error("ERROR AL REALIZAR LA OPERACION USUARIO NO ENCONTRADO EN LA BD - "
						+ idUsuario);
				usuarioResponse = new UsuarioResponse();
				usuarioResponse.setIdError(Integer.parseInt(errorCodigoUsuario));
				usuarioResponse.setMensajeError(errorMensajeUsuario);
				return new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.BAD_REQUEST);
			} else {
				
//				Deshabilitando Usuario
				usuariosInpamex = this.deshabilitarUsuario(resultUsuario);
				if (usuariosInpamex == null & usuariosInpamex.getIdUsuario() != null) {
					logger.error("ERROR AL DESHABILITAR USUARIO");
					usuarioResponse = new UsuarioResponse();
					usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
					usuarioResponse.setMensajeError(errorMensajeErrorInterno);
					return new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.INTERNAL_SERVER_ERROR);
				} else {
					logger.info("PROCESO FINALIZADO EXITOSAMENTE DESHABILITAR USUARIO IDUSUARIO - " + usuariosInpamex.getIdUsuario());
					usuarioResponse = new UsuarioResponse();
					usuarioResponse.setIdError(Integer.parseInt(exitoCodigo));
					usuarioResponse.setMensajeError(exitoMensaje);
					usuarioResponse.setIdUsuario(idUsuario);
					usuarioResponse.setNombre(usuariosInpamex.getNombre());
					usuarioResponse.setApaterno(usuariosInpamex.getApaterno());
					usuarioResponse.setAmaterno(usuariosInpamex.getAmaterno());
					responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
							HttpStatus.OK);
				}
				
			}
			
		} catch (Exception e) {
			logger.error("ERROR AL DESHABILITAR USUARIO - " + e.getMessage());
			usuarioResponse = new UsuarioResponse();
			usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@Override
	public ResponseEntity<UsuarioResponse> enableUsuario(Long idUsuario) {

		logger.info("####################################################");
		logger.info("STARTING DHABILITAR USUARIO IDUSUARIO  - " + idUsuario);

		ResponseEntity<UsuarioResponse> responseEntity = null;
		UsuarioResponse usuarioResponse = null;
		
		Optional<UsuariosInpamex> resultUsuario = Optional.empty();
		UsuariosInpamex usuariosInpamex = null;
		
		try {
			
//			Validando Existencia de Usuario
			logger.info("STARTING ENCONTRAR USUARIO EN BD - " + idUsuario);
			resultUsuario = usuariosInpamexDao.findById(idUsuario);
			logger.info("END ENCONTRAR USUARIO EN BD - " + resultUsuario.isPresent());

			if (!resultUsuario.isPresent()) {
				logger.error("ERROR AL REALIZAR LA OPERACION USUARIO NO ENCONTRADO EN LA BD - "
						+ idUsuario);
				usuarioResponse = new UsuarioResponse();
				usuarioResponse.setIdError(Integer.parseInt(errorCodigoUsuario));
				usuarioResponse.setMensajeError(errorMensajeUsuario);
				return new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.BAD_REQUEST);
			} else {
				
//				Habilitando Usuario
				usuariosInpamex = this.habilitarUsuario(resultUsuario);
				if (usuariosInpamex == null & usuariosInpamex.getIdUsuario() != null) {
					logger.error("ERROR AL HABILITAR USUARIO");
					usuarioResponse = new UsuarioResponse();
					usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
					usuarioResponse.setMensajeError(errorMensajeErrorInterno);
					return new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.INTERNAL_SERVER_ERROR);
				} else {
					logger.info("PROCESO FINALIZADO EXITOSAMENTE HABILITAR USUARIO IDUSUARIO - " + usuariosInpamex.getIdUsuario());
					usuarioResponse = new UsuarioResponse();
					usuarioResponse.setIdError(Integer.parseInt(exitoCodigo));
					usuarioResponse.setMensajeError(exitoMensaje);
					usuarioResponse.setIdUsuario(idUsuario);
					usuarioResponse.setNombre(usuariosInpamex.getNombre());
					usuarioResponse.setApaterno(usuariosInpamex.getApaterno());
					usuarioResponse.setAmaterno(usuariosInpamex.getAmaterno());
					responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
							HttpStatus.OK);
				}
			}
			
		} catch (Exception e) {
			logger.error("ERROR AL DESHABILITAR USUARIO - " + e.getMessage());
			usuarioResponse = new UsuarioResponse();
			usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@Override
	public ResponseEntity<UsuarioEntityResponse> getUsuario(Long idUsuario) {
		
		logger.info("####################################################");
		logger.info("STARTING CONSULTA DE USUARIO IDUSUARIO  - " + idUsuario);

		ResponseEntity<UsuarioEntityResponse> responseEntity = null;
		Optional<UsuariosInpamex> resultUsuario = Optional.empty();

		try {

			resultUsuario = usuariosInpamexDao.findById(idUsuario);
			UsuarioEntityResponse usuarioEntityResponse = new UsuarioEntityResponse();

			if (resultUsuario.isPresent()) {
				usuarioEntityResponse.setUsuario(resultUsuario.get());
			} else {
				usuarioEntityResponse.setUsuario(null);
			}

			usuarioEntityResponse.setIdError(Integer.parseInt(exitoCodigo));
			usuarioEntityResponse.setMensajeError(exitoMensaje);
			responseEntity = new ResponseEntity<UsuarioEntityResponse>(usuarioEntityResponse, HttpStatus.OK);

			logger.info("PROCESO FINALIZADO EXITOSAMENTE CONSULTA DE USUARIO - " + idUsuario);

		} catch (Exception e) {
			logger.error("ERROR AL CONSULTAR USUARIO IDUSUARIO - " + e.getMessage());
			UsuarioEntityResponse usuarioEntityResponse = new UsuarioEntityResponse();
			usuarioEntityResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioEntityResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioEntityResponse>(usuarioEntityResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@Override
	public ResponseEntity<UsuariosEntityResponse> getUsuarios() {
		
		logger.info("####################################################");
		logger.info("STARTING CONSULTA DE USUARIOS");

		ResponseEntity<UsuariosEntityResponse> responseEntity = null;
		List<UsuariosInpamex> listaUsuarios = new ArrayList<UsuariosInpamex>();

		try {

			UsuariosEntityResponse usuariosEntityResponse = new UsuariosEntityResponse();

			listaUsuarios = usuariosInpamexDao.findUsuarioAll();

			usuariosEntityResponse.setIdError(Integer.parseInt(exitoCodigo));
			usuariosEntityResponse.setMensajeError(exitoMensaje);
			usuariosEntityResponse.setUsuario(listaUsuarios);
			responseEntity = new ResponseEntity<UsuariosEntityResponse>(usuariosEntityResponse, HttpStatus.OK);

			logger.info("END CONSULTA DE USUARIOS");
		} catch (Exception e) {
			logger.error("ERROR AL CONSULTAR USUARIOS - " + e.getMessage());
			UsuariosEntityResponse usuariosEntityResponse = new UsuariosEntityResponse();
			usuariosEntityResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuariosEntityResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuariosEntityResponse>(usuariosEntityResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	private String validarDatoVacio(String dato) {
		String datoReturn = "";
		
		try {
			
			if (dato != null & !"".equals(dato)) {
				datoReturn = dato.toUpperCase().trim();
			} else {
				datoReturn = "ND";
			}
			
		} catch (Exception e) {
			logger.error("ERROR AL VALIDAR DATO VACIO - " + e.getLocalizedMessage());
		}
		
		return datoReturn;
	}
	
	public UsuariosInpamex deshabilitarUsuario(Optional<UsuariosInpamex> optUsuario) {
		logger.info("STARTING DESHABILITAR USUARIO IDUSUARIO - " + optUsuario.get().getIdUsuario());
		UsuariosInpamex usuariosInpamexResult = null;
		try {
			UsuariosInpamex usuariosInpamex = optUsuario.get();
			usuariosInpamex.setEstatus(usuarioCodigoEstatusInactivo.charAt(0));
			usuariosInpamex.setEstatusDescripcion(usuarioDescEstatusInactivo);
			usuariosInpamex.setFechaModificacion(new Date());
			usuariosInpamexResult = usuariosInpamexDao.save(usuariosInpamex);
			logger.info("END DESHABILITAR USUARIO - " + usuariosInpamexResult.toString());
		} catch (Exception e) {
			logger.error("ERROR DESHABILITAR USUARIO DESHABILITARUSUARIO - " + e.getLocalizedMessage());
		}
		return usuariosInpamexResult;
	}
	
	public UsuariosInpamex habilitarUsuario(Optional<UsuariosInpamex> optUsuario) {
		logger.info("STARTING HABILITAR USUARIO IDUSUARIO - " + optUsuario.get().getIdUsuario());
		UsuariosInpamex usuariosInpamexResult = null;
		try {
			UsuariosInpamex usuariosInpamex = optUsuario.get();
			usuariosInpamex.setEstatus(usuarioCodigoEstatusActivo.charAt(0));
			usuariosInpamex.setEstatusDescripcion(usuarioDescEstatusActivo);
			usuariosInpamex.setFechaModificacion(new Date());
			usuariosInpamexResult = usuariosInpamexDao.save(usuariosInpamex);
			logger.info("END HABILITAR USUARIO - " + usuariosInpamexResult.toString());
		} catch (Exception e) {
			logger.error("ERROR HABILITAR USUARIO DESHABILITARUSUARIO - " + e.getLocalizedMessage());
		}
		return usuariosInpamexResult;
	}
	
	@Override
	public ResponseEntity<UsuarioEntityResponse> getUsuario(String curp) {
		
		logger.info("####################################################");
		logger.info("STARTING CONSULTA DE USUARIO CURP  - " + curp);

		ResponseEntity<UsuarioEntityResponse> responseEntity = null;
		Optional<UsuariosInpamex> resultUsuario = Optional.empty();

		try {

			resultUsuario = usuariosInpamexDao.findUsuarioByCURP(curp);
			UsuarioEntityResponse usuarioEntityResponse = new UsuarioEntityResponse();

			if (resultUsuario.isPresent()) {
				usuarioEntityResponse.setUsuario(resultUsuario.get());
			} else {
				usuarioEntityResponse.setUsuario(null);
			}

			usuarioEntityResponse.setIdError(Integer.parseInt(exitoCodigo));
			usuarioEntityResponse.setMensajeError(exitoMensaje);
			responseEntity = new ResponseEntity<UsuarioEntityResponse>(usuarioEntityResponse, HttpStatus.OK);

			logger.info("PROCESO FINALIZADO EXITOSAMENTE CONSULTA DE CURP - " + curp);

		} catch (Exception e) {
			logger.error("ERROR AL CONSULTAR USUARIO IDUSUARIO - " + e.getMessage());
			UsuarioEntityResponse usuarioEntityResponse = new UsuarioEntityResponse();
			usuarioEntityResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioEntityResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioEntityResponse>(usuarioEntityResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

}
