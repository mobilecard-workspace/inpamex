package com.addcel.inpamex.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.addcel.inpamex.payload.request.Tarjeta;
import com.addcel.inpamex.payload.request.TarjetaAsignar;
import com.addcel.inpamex.payload.response.TarjetaAsignResponse;
import com.addcel.inpamex.payload.response.TarjetaIndividualResponse;
import com.addcel.inpamex.payload.response.TarjetaResponse;
import com.addcel.inpamex.payload.response.TarjetaTransaccionCodigoBarras;
import com.addcel.inpamex.payload.response.TarjetasGeneralResponse;
import com.addcel.inpamex.payload.response.TransaccionResponse;

public interface TarjetaService {

	public ResponseEntity<TarjetaResponse> getBalanceTarjetaCodigoBarras(String codigoBarras, String claveCadena);
	
	public ResponseEntity<TarjetaResponse> findTarjetaNumero(String numero);

	public ResponseEntity<TransaccionResponse> doTransaccionCodigoBarras(TarjetaTransaccionCodigoBarras transaccion);

	public ResponseEntity<TarjetasGeneralResponse> saveTarjetas(List<Tarjeta> tarjetas);
	
	public ResponseEntity<TarjetasGeneralResponse> saveTarjeta(Tarjeta tarjetas);
	
	public ResponseEntity<TarjetaAsignResponse> asignTarjeta(TarjetaAsignar tarjetaAsignar);

}
