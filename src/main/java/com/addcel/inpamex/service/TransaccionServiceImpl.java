package com.addcel.inpamex.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.addcel.inpamex.data.dao.TransaccionesInpamexDao;
import com.addcel.inpamex.data.entity.TransaccionesInpamex;
import com.addcel.inpamex.payload.request.TransaccionRequest;
import com.addcel.inpamex.payload.response.GeneralResponse;
import com.addcel.inpamex.payload.response.TransaccionFullResponse;
import com.addcel.inpamex.payload.response.TransaccionGenericResponse;
import com.addcel.inpamex.utils.Constants;

@Service
@Transactional
public class TransaccionServiceImpl implements TransaccionService {
	
	private final static Logger logger = Logger.getLogger(TransaccionServiceImpl.class);

	@Value("${error.codigo.transacciones.inexistente}")
	private String errorCodigoTransacciones;

	@Value("${error.mensaje.transacciones.inexistente}")
	private String errorMensajeTransacciones;
	
	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;

	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;
	
	@Autowired
	private TransaccionesInpamexDao transaccionesInpamexDao;
	
	@Override
	public ResponseEntity<TransaccionFullResponse> findTransaccionsByTarjeta(Long idTarjeta) {
		
		logger.info("####################################################");
		logger.info("STARTING CONSULTA DE TRANSACCIONES POR TARJETA - " + idTarjeta);

		ResponseEntity<TransaccionFullResponse> responseEntity = null;
		Optional<List<TransaccionesInpamex>> optionalListaTransacctions = Optional.empty();
		
		TransaccionFullResponse transaccionFullResponse = new TransaccionFullResponse();
		List<TransaccionGenericResponse> transacciones;
		
		try {
			
			optionalListaTransacctions = transaccionesInpamexDao.findTransaccionsByTarjeta(idTarjeta);
			
			if (!optionalListaTransacctions.isPresent()) {
				
				logger.error("LA TARJETA NO CUENTA CON TRANSACCIONES IDTARJETA - " + idTarjeta);
				transaccionFullResponse = new TransaccionFullResponse();
				transaccionFullResponse.setIdError(Integer.parseInt(errorCodigoTransacciones));
				transaccionFullResponse.setMensajeError(errorMensajeTransacciones);
				return new ResponseEntity<TransaccionFullResponse>(transaccionFullResponse, HttpStatus.OK);
				
			} else {

				transaccionFullResponse = new TransaccionFullResponse();
				transacciones = new ArrayList<>();
				
				for (TransaccionesInpamex transaccion : optionalListaTransacctions.get()) {
					
					TransaccionGenericResponse transaccionGenericResponse = new TransaccionGenericResponse();
					
					transaccionGenericResponse.setIdTransaccion(transaccion.getIdTransaccion());
					transaccionGenericResponse.setIdUsuario(transaccion.getIdUsuario());
					transaccionGenericResponse.setIdTransferencia(transaccion.getIdTransferencia());
					transaccionGenericResponse.setIdTarjeta(transaccion.getIdTarjeta());
					transaccionGenericResponse.setIdMto(transaccion.getIdMto());
					transaccionGenericResponse.setTipo(transaccion.getTipo());
					transaccionGenericResponse.setMonto(transaccion.getMonto());
					transaccionGenericResponse.setConcepto(transaccion.getConcepto());
					transaccionGenericResponse.setClaveCadena(transaccion.getClaveCadena());
					transaccionGenericResponse.setFecha(transaccion.getFecha());
					transaccionGenericResponse.setEstatus(transaccion.getEstatus());
					transaccionGenericResponse.setEstatusDescripcion(transaccion.getEstatusDescripcion());
					transaccionGenericResponse.setNoAutorizacion(transaccion.getNumeroAutorizacion());
					transaccionGenericResponse.setComentario(transaccion.getComentario());
					
					transacciones.add(transaccionGenericResponse);
				}
				
				transaccionFullResponse.setTransacciones(transacciones);
				logger.info(
						"PROCESO FINALIZADO EXITOSAMENTE CONSULTA TRANSACCIONES POR TARJETA IDTARJETA - " + idTarjeta);
				responseEntity = new ResponseEntity<TransaccionFullResponse>(transaccionFullResponse, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE TRANSACCIONES POR TARJETA IDTARJETA - " + e.getMessage());
			transaccionFullResponse = new TransaccionFullResponse();

			transaccionFullResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			transaccionFullResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TransaccionFullResponse>(transaccionFullResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
	}

	@Override
	public ResponseEntity<GeneralResponse> transaccionDevolucion(TransaccionRequest request) {
		ResponseEntity<GeneralResponse> response = null;
		GeneralResponse payloadResponse = null;
		try {
			logger.info("Se inicia el negocio del servicio de devolucion");
			TransaccionesInpamex inpamex = transaccionesInpamexDao.findByNumeroAutorizacion(request.getNumero());
			if(inpamex == null) {
				payloadResponse = new GeneralResponse(Constants.ERROR_404, Constants.DESC_ERROR_404+request.getNumero());
				response = new ResponseEntity<GeneralResponse>(payloadResponse, HttpStatus.NOT_FOUND);
				return response;
			}
			else if(inpamex.getEstatusDescripcion().equals(Constants.DEVOLUCION)) {
				payloadResponse = new GeneralResponse(Constants.ERROR_400, Constants.DESC_ERROR_400+request.getNumero());
				response = new ResponseEntity<GeneralResponse>(payloadResponse, HttpStatus.BAD_REQUEST);
				return response;
			}
//			double valor1 = inpamex.getMonto();
//			double valor2 = request.getMonto();
			if(!inpamex.getMonto().equals(request.getMonto())) {
				payloadResponse = new GeneralResponse(Constants.ERROR_400, Constants.DESC_ERROR_400_MONTO+request.getNumero());
				response = new ResponseEntity<GeneralResponse>(payloadResponse, HttpStatus.BAD_REQUEST);
				return response;
			}
			else {
				transaccionesInpamexDao.actualizaEstadoDevolucion(request.getNumero(),Constants.DEVOLUCION);
			
			payloadResponse= new GeneralResponse(Constants.OK_200, Constants.OK_DEVOLUCION);
			response = new ResponseEntity<GeneralResponse>(payloadResponse, HttpStatus.OK);
			return response;
			}
		} catch (Exception e) {
			logger.info("Hubo un error interno "+e.getMessage());
			payloadResponse = new GeneralResponse(Constants.ERROR_500, Constants.DESC_ERROR_500+e.getMessage());
			response = new ResponseEntity<GeneralResponse>(payloadResponse, HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}
	
	}

	

}
