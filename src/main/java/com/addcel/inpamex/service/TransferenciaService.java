package com.addcel.inpamex.service;

import org.springframework.http.ResponseEntity;

import com.addcel.inpamex.payload.request.Transferencia;
import com.addcel.inpamex.payload.request.TransferenciaAbonar;
import com.addcel.inpamex.payload.response.TransferenciaIndividualResponse;
import com.addcel.inpamex.payload.response.TransferenciaResponse;

public interface TransferenciaService {

	public ResponseEntity<TransferenciaResponse> doTransferencia(Transferencia transferencia);
	
	public ResponseEntity<TransferenciaIndividualResponse> getTransferencia(String idMto);
	
	public ResponseEntity<TransferenciaResponse> addTransferencia(TransferenciaAbonar transferencia);

}
