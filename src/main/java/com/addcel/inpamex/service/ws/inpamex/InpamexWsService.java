package com.addcel.inpamex.service.ws.inpamex;

import org.springframework.http.ResponseEntity;

import com.addcel.inpamex.payload.ws.inpamex.ClientResponseConfirmTransfer;

public interface InpamexWsService {
	
	public ResponseEntity<ClientResponseConfirmTransfer> confirmTransfer(Long idTransferencia);

}
