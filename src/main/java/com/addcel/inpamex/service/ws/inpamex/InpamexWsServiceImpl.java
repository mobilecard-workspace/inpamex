package com.addcel.inpamex.service.ws.inpamex;

import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.addcel.inpamex.data.dao.TransferenciasInpamexDao;
import com.addcel.inpamex.data.dao.UsuariosInpamexDao;
import com.addcel.inpamex.data.entity.TransferenciasInpamex;
import com.addcel.inpamex.data.entity.UsuariosInpamex;
import com.addcel.inpamex.feign.inpamex.InpamexClient;
import com.addcel.inpamex.payload.ws.inpamex.ClientRequestBenefDoc;
import com.addcel.inpamex.payload.ws.inpamex.ClientRequestBenefDomicilio;
import com.addcel.inpamex.payload.ws.inpamex.ClientRequestBeneficiario;
import com.addcel.inpamex.payload.ws.inpamex.ClientRequestConfirmTransfer;
import com.addcel.inpamex.payload.ws.inpamex.ClientResponseConfirmTransfer;

@Service
@Transactional
public class InpamexWsServiceImpl implements InpamexWsService {

	private final static Logger logger = Logger.getLogger(InpamexWsServiceImpl.class);

	@Value("${trans.estatus.codigo.abonada}")
	private String transCodigoEstatusAbonada;

	@Autowired
	private TransferenciasInpamexDao transferenciasInpamexDao;

	@Autowired
	private UsuariosInpamexDao usuariosInpamexDao;

	@Autowired
	private InpamexClient inpamexClient;

	@Override
	public ResponseEntity<ClientResponseConfirmTransfer> confirmTransfer(Long idTransferencia) {

		logger.info("STARTING CONFIRMAR TRANSFERENCIA CON INPAMEX ID TRANSFERENCIA - " + idTransferencia);

		ResponseEntity<ClientResponseConfirmTransfer> responseEntity = null;

		Optional<TransferenciasInpamex> resultTransfer = Optional.empty();
		Optional<UsuariosInpamex> resultUsuario = Optional.empty();

		ClientRequestConfirmTransfer requestConfirmTransfer = null;
		ClientResponseConfirmTransfer responseConfirmTransfer = null;

		try {

//			Validando Existencia de Transferencia
			logger.info("STARTING ENCONTRAR TRANSFERENCIA EN BD");
			resultTransfer = transferenciasInpamexDao.findById(idTransferencia);
			logger.info("END ENCONTRAR TRANSFERENCIA EN BD - " + resultTransfer.isPresent());

			if (!resultTransfer.isPresent()) {
				logger.error("ERROR AL CONFIRMAR TRANSFERENCIA CON INPAMEX TRANSFERENCIA NO ENCONTRADA EN BD");
				responseConfirmTransfer = new ClientResponseConfirmTransfer();
				responseConfirmTransfer.setIdMto("");
				responseConfirmTransfer.setEstatus("");
				responseConfirmTransfer.setEstatusDescripcion("");
				return new ResponseEntity<ClientResponseConfirmTransfer>(responseConfirmTransfer,
						HttpStatus.INTERNAL_SERVER_ERROR);
			} 
//			else {
//				Validando estatus de Transferencia
//				if (!transCodigoEstatusAbonada.equals(resultTransfer.get().getEstatus().toString())) {
//					logger.error("ERROR AL CONFIRMAR TRANSFERENCIA CON INPAMEX TRANSFERENCIA NO TIENE ESTATUS ABONADA");
//					responseConfirmTransfer = new ClientResponseConfirmTransfer();
//					responseConfirmTransfer.setIdMto("");
//					responseConfirmTransfer.setEstatus(-500);
//					responseConfirmTransfer.setEstatusDescripcion("");
//					return new ResponseEntity<ClientResponseConfirmTransfer>(responseConfirmTransfer, HttpStatus.INTERNAL_SERVER_ERROR);
//				}
//			}

//			Validando existencia de Usuario
			logger.info("STARTING ENCONTRAR USUARIO DE TRANSFERENCIA EN BD - "
					+ resultTransfer.get().getIdUsuario());
			resultUsuario = usuariosInpamexDao.findById(resultTransfer.get().getIdUsuario());
			logger.info("END ENCONTRAR USUARIO DE TRANSFERENCIA EN BD - " + resultUsuario.isPresent());

			if (!resultUsuario.isPresent()) {
				logger.error("USUARIO NO ENCONTRADO EN LA BD - " + resultTransfer.get().getIdUsuario());
				requestConfirmTransfer = this.generateRequestSinUsuario(resultTransfer.get());
			} else {
				logger.error("USUARIO NO ENCONTRADO EN LA BD - " + resultTransfer.get().getIdUsuario());
				requestConfirmTransfer = this.generateRequest(resultTransfer.get(), resultUsuario.get());
			}

			if (resultTransfer.isPresent()) {

				logger.info("STARTING REQUEST CONFIRM TRANSFER INPAMEX - " + requestConfirmTransfer.toString());
				responseEntity = inpamexClient.confirmTransfer(requestConfirmTransfer);
				logger.info("END REQUEST CONFIRM TRANSFER INPAMEX HTTP STATUS - " + responseEntity.getStatusCode()
						+ " ID STATUS - " + responseEntity.getBody().getEstatus());
				
				this.updateTransferencia(resultTransfer.get(), requestConfirmTransfer, responseEntity.getBody());

			}
		} catch (Exception e) {
			logger.error("ERROR AL CONFIRMAR TRANSFERENCIA CON INPAMEX - " + e.getLocalizedMessage());
			responseConfirmTransfer = new ClientResponseConfirmTransfer();
			responseConfirmTransfer.setIdMto("");
			responseConfirmTransfer.setEstatus("");
			responseConfirmTransfer.setEstatusDescripcion("");
			responseEntity = new ResponseEntity<ClientResponseConfirmTransfer>(responseConfirmTransfer,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;
	}

	private ClientRequestConfirmTransfer generateRequest(TransferenciasInpamex transferencia, UsuariosInpamex usuario) {

		logger.info("STARTING GENERAR EL REQUEST PARA CONFIRMAR TRANSFERENCIA");
		ClientRequestConfirmTransfer requestConfirmTransfer = null;
		ClientRequestBeneficiario beneficiario = null;
		ClientRequestBenefDomicilio domicilio = null;
		ClientRequestBenefDoc docRemittance = null;

		try {

			requestConfirmTransfer = new ClientRequestConfirmTransfer();

			requestConfirmTransfer.setIdMto(transferencia.getIdMto());
			requestConfirmTransfer.setIdUsuario(transferencia.getIdUsuario());
			requestConfirmTransfer.setNoAutorizacion(transferencia.getNumeroAutorizacion());
			requestConfirmTransfer.setEstatus(transferencia.getEstatus().toString());
			requestConfirmTransfer.setEstatusDescripcion(transferencia.getEstatusDescripcion());

			beneficiario = new ClientRequestBeneficiario();

			String fechaNacimiento = usuario.getFechaNacimiento().toString();
			String birthDay = fechaNacimiento.substring(8, 10) + "-" + fechaNacimiento.substring(5, 7) + "-"
					+ fechaNacimiento.substring(0, 4);

			beneficiario.setBirthday(birthDay);
			beneficiario.setNacionalidad(usuario.getNacionalidad());
			beneficiario.setPaisNacimiento(usuario.getPaisNacimiento());
			beneficiario.setEstadoNacimiento(usuario.getEstadoNacimiento());
			beneficiario.setGenero(usuario.getGenero());
			beneficiario.setTelefono(usuario.getTelefono());
			beneficiario.setClaveActEconomica(usuario.getClaveActividadEconomica());

			domicilio = new ClientRequestBenefDomicilio();

			domicilio.setCalle(usuario.getDomicilioCalle());
			domicilio.setNoExterior(usuario.getDomicilioNoExterior());
			domicilio.setNoInterior(usuario.getDomicilioNoInterior());
			domicilio.setMunicipio(usuario.getDomicilioMunicipio());
			domicilio.setColonia(usuario.getDomicilioColonia());
			domicilio.setCp(usuario.getDomicilioCp());
			domicilio.setCiudad(usuario.getDomicilioCiudad());
			domicilio.setEstado(usuario.getDomicilioEstado());
			domicilio.setPais(usuario.getDomicilioPais());

			beneficiario.setDomicilio(domicilio);
			requestConfirmTransfer.setBeneficiario(beneficiario);

			docRemittance = new ClientRequestBenefDoc();

			docRemittance.setTipoDocumento(usuario.getTipoDocumento());
			docRemittance.setNoDocumento(usuario.getNoDocumento());
			requestConfirmTransfer.setDocRemittance(docRemittance);

			logger.info("END GENERAR EL REQUEST PARA CONFIRMAR TRANSFERENCIA");
			return requestConfirmTransfer;

		} catch (Exception e) {
			logger.error("ERROR AL GENERAR EL REQUEST PARA CONFIRMAR TRANSFERENCIA");
		}
		return requestConfirmTransfer;
	}
	
	private ClientRequestConfirmTransfer generateRequestSinUsuario(TransferenciasInpamex transferencia) {

		logger.info("STARTING GENERAR EL REQUEST SIN USUARIO PARA CONFIRMAR TRANSFERENCIA");
		ClientRequestConfirmTransfer requestConfirmTransfer = null;
		try {

			requestConfirmTransfer = new ClientRequestConfirmTransfer();

			requestConfirmTransfer.setIdMto(transferencia.getIdMto());
			requestConfirmTransfer.setIdUsuario(transferencia.getIdUsuario());
			requestConfirmTransfer.setNoAutorizacion(transferencia.getNumeroAutorizacion());
			requestConfirmTransfer.setEstatus(transferencia.getEstatus().toString());
			requestConfirmTransfer.setEstatusDescripcion(transferencia.getEstatusDescripcion());

			logger.info("END GENERAR EL REQUEST SIN USUARIO PARA CONFIRMAR TRANSFERENCIA");
			return requestConfirmTransfer;

		} catch (Exception e) {
			logger.error("ERROR AL GENERAR EL REQUEST SIN USUARIO PARA CONFIRMAR TRANSFERENCIA");
		}
		return requestConfirmTransfer;
	}

	private Optional<TransferenciasInpamex> updateTransferencia(TransferenciasInpamex transferencia,
			ClientRequestConfirmTransfer requestConfirmTransfer,
			ClientResponseConfirmTransfer responseConfirmTransfer) {

		logger.info("STARTING GUARDAR RESPONSE CONFIRM TRANSFER INPAMEX EN TRANSFERENCIA");
		Optional<TransferenciasInpamex> result = Optional.empty();

		try {
			TransferenciasInpamex transferenciasInpamex = transferencia;

			transferenciasInpamex.setInpamexEstatus(responseConfirmTransfer.getEstatus());
			transferenciasInpamex.setInpamexEstatusDesc(responseConfirmTransfer.getEstatusDescripcion());
			transferenciasInpamex.setInpamexRequest(requestConfirmTransfer.toString());
			transferenciasInpamex.setInpamexResponse(responseConfirmTransfer.toString());

			TransferenciasInpamex transferenciasInpamexResult = transferenciasInpamexDao.save(transferenciasInpamex);
			result = Optional.of(transferenciasInpamexResult);
			logger.info("STARTING GUARDAR RESPONSE CONFIRM TRANSFER INPAMEX EN TRANSFERENCIA - " + transferenciasInpamexResult.getIdTransferencia());
		} catch (Exception e) {
			logger.error("ERROR AL GUARDAR RESPONSE CONFIRM TRANSFER INPAMEX EN TRANSFERENCIA - " + e.getLocalizedMessage());
		}

		return result;
	}

}
