package com.addcel.inpamex.service;

import org.springframework.http.ResponseEntity;

import com.addcel.inpamex.payload.request.TransaccionRequest;
import com.addcel.inpamex.payload.response.GeneralResponse;
import com.addcel.inpamex.payload.response.TransaccionFullResponse;

public interface TransaccionService {
	
	public ResponseEntity<TransaccionFullResponse> findTransaccionsByTarjeta(Long idTarjeta);

	public ResponseEntity<GeneralResponse> transaccionDevolucion(TransaccionRequest request);

}
