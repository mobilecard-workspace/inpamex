package com.addcel.inpamex.service.catalogos;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.addcel.inpamex.data.dao.CatTipoDocumentoInpamexDao;
import com.addcel.inpamex.data.entity.CatTipoDocumentoInpamex;
import com.addcel.inpamex.payload.request.CatalogoRequest;
import com.addcel.inpamex.payload.request.ListaCatalogos;
import com.addcel.inpamex.payload.response.CatalogoGeneralResponse;
import com.addcel.inpamex.payload.response.CatalogoResponse;
import com.addcel.inpamex.utils.Commons;

@Service
@Transactional
public class CatTipoDocumentoServiceImpl implements CatTipoDocumentoService {
	
	private final static Logger logger = Logger.getLogger(CatTipoDocumentoServiceImpl.class);

	@Value("${error.codigo.catalogo.existente}")
	private String errorCodigoCatalogoExistente;
	
	@Value("${cat.estus.activo}")
	private String catEstatusActivo;

	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;

	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;

	@Value("${exito.codigo}")
	private String exitoCodigo;

	@Value("${exito.mensaje}")
	private String exitoMensaje;

	@Autowired
	private CatTipoDocumentoInpamexDao catTipoDocumentoDao;

	@Autowired
	private Commons commons;

	@Override
	public ResponseEntity<CatalogoResponse> save(ListaCatalogos listaCatalogos) {
		
		logger.info("####################################################");
		logger.info("STARTING GUARDAR CATALOGO TIPO DOCUMENTO NUMERO REGISTROS - " + listaCatalogos.getCatalogos().size());

		ResponseEntity<CatalogoResponse> responseEntity = null;
		CatalogoResponse catalogoResponse = null;

		Optional<CatTipoDocumentoInpamex> catOptionalResult = Optional.empty();
		List<CatalogoGeneralResponse> response = null;

		try {

			response = new ArrayList<>();

			for (CatalogoRequest catalogoRequest : listaCatalogos.getCatalogos()) {

//				Validando Existencia de Clave de Catalogo
				catOptionalResult = catTipoDocumentoDao
						.findByClave(catalogoRequest.getClave().toUpperCase().trim());

				if (catOptionalResult.isPresent()) {
					logger.error("ERROR AL GUARDAR EL CATALOGO CATALOGO EXISTENTE EN BD - "
							+ catalogoRequest.getClave().toUpperCase().trim());
					catalogoResponse = new CatalogoResponse();
					catalogoResponse.setIdError(Integer.parseInt(errorCodigoCatalogoExistente));
					catalogoResponse.setMensajeError(
							commons.generarMensajeErrorCatalogo(catalogoRequest.getClave().toUpperCase().trim()));
					return new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.BAD_REQUEST);
				}

			}

			logger.info("STARTING SAVE CATALOGOS NUMERO DE CATALOGOS - " + listaCatalogos.getCatalogos().size());
			for (CatalogoRequest catalogoRequest : listaCatalogos.getCatalogos()) {

//				Guardando Catalogo
				CatTipoDocumentoInpamex catalogoSave = new CatTipoDocumentoInpamex();
				catalogoSave.setClave(catalogoRequest.getClave().toUpperCase().trim());
				catalogoSave.setDescripcion(catalogoRequest.getDescripcion().toUpperCase().trim());
				catalogoSave.setEstatus(catEstatusActivo.charAt(0));

				CatTipoDocumentoInpamex catalogoSaved = catTipoDocumentoDao
						.save(catalogoSave);

				CatalogoGeneralResponse catalogoGeneralResponse = new CatalogoGeneralResponse();
				catalogoGeneralResponse.setId(catalogoSaved.getId());
				catalogoGeneralResponse.setClave(catalogoSaved.getClave());
				catalogoGeneralResponse.setDescripcion(catalogoSaved.getDescripcion());
				catalogoGeneralResponse.setEstatus(catalogoSaved.getEstatus());
				
				response.add(catalogoGeneralResponse);
			}
			
			if (response.size() > 0) {
				logger.info("PROCESO FINALIZADO EXITOSAMENTE GUARDAR CATALOGOS NUMERO DE CATALOGOS GUARDADOS - "
						+ response.size());
				catalogoResponse = new CatalogoResponse();

				catalogoResponse.setIdError(Integer.parseInt(exitoCodigo));
				catalogoResponse.setMensajeError(exitoMensaje);
				catalogoResponse.setResponse(response);
				responseEntity = new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.OK);
			}

		} catch (Exception e) {
			logger.error("ERROR AL GUARDAR EL CATALOGO - " + e.getMessage());
			catalogoResponse = new CatalogoResponse();
			catalogoResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			catalogoResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;
	}

	@Override
	public ResponseEntity<CatalogoResponse> findByEstatusActivo() {
		
		logger.info("####################################################");
		logger.info("STARTING CONSULTA DE CATALOGO TIPO DE DOCUMENTO");
		
		ResponseEntity<CatalogoResponse> responseEntity = null;
		CatalogoResponse catalogoResponse = null;
		
		Optional<List<CatTipoDocumentoInpamex>> optionalCatResult = Optional.empty();
		List<CatTipoDocumentoInpamex> listCatalogos = null;
		List<CatalogoGeneralResponse> response = null;
		
		try {
			
			listCatalogos = new ArrayList<>();
			response = new ArrayList<>();
			optionalCatResult = catTipoDocumentoDao.findByEstatus(catEstatusActivo.charAt(0));
			
			if (optionalCatResult.isPresent()) {
				listCatalogos = optionalCatResult.get();
				
				for (CatTipoDocumentoInpamex catalogo : listCatalogos) {
					CatalogoGeneralResponse catalogoGeneralResponse = new CatalogoGeneralResponse();
					catalogoGeneralResponse.setId(catalogo.getId());
					catalogoGeneralResponse.setClave(catalogo.getClave());
					catalogoGeneralResponse.setDescripcion(catalogo.getDescripcion());
					catalogoGeneralResponse.setEstatus(catalogo.getEstatus());
					
					response.add(catalogoGeneralResponse);
				}
			}
			
			logger.info("PROCESO FINALIZADO EXITOSAMENTE CONSULTA DE CATALOGO");
			catalogoResponse = new CatalogoResponse();

			catalogoResponse.setIdError(Integer.parseInt(exitoCodigo));
			catalogoResponse.setMensajeError(exitoMensaje);
			catalogoResponse.setResponse(response);
			responseEntity = new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.OK);
			
		} catch (Exception e) {
			logger.error("ERROR AL CONSULTAR CATALOGO - " + e.getMessage());
			catalogoResponse = new CatalogoResponse();
			catalogoResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			catalogoResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
	}

}
