package com.addcel.inpamex.service.catalogos;

import org.springframework.http.ResponseEntity;

import com.addcel.inpamex.payload.request.CatalogoRequest;
import com.addcel.inpamex.payload.request.ListaCatalogos;
import com.addcel.inpamex.payload.response.CatalogoResponse;

public interface CatPaisesService {
	
	public ResponseEntity<CatalogoResponse> saveSingle(CatalogoRequest catalogo);
	public ResponseEntity<CatalogoResponse> save(ListaCatalogos listaCatalogos);
	public ResponseEntity<CatalogoResponse> findByEstatusActivo();

}
