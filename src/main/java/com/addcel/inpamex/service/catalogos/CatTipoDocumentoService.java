package com.addcel.inpamex.service.catalogos;

import org.springframework.http.ResponseEntity;

import com.addcel.inpamex.payload.request.ListaCatalogos;
import com.addcel.inpamex.payload.response.CatalogoResponse;

public interface CatTipoDocumentoService {
	
	public ResponseEntity<CatalogoResponse> save(ListaCatalogos listaCatalogos);
	public ResponseEntity<CatalogoResponse> findByEstatusActivo();

}
