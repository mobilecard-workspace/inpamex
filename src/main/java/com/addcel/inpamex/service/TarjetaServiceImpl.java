package com.addcel.inpamex.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.addcel.inpamex.data.dao.TarjetaInpamexDao;
import com.addcel.inpamex.data.dao.TransaccionesInpamexDao;
import com.addcel.inpamex.data.dao.TransferenciasInpamexDao;
import com.addcel.inpamex.data.dao.UsuariosInpamexDao;
import com.addcel.inpamex.data.entity.TarjetaInpamex;
import com.addcel.inpamex.data.entity.TransaccionesInpamex;
import com.addcel.inpamex.data.entity.TransferenciasInpamex;
import com.addcel.inpamex.data.entity.UsuariosInpamex;
import com.addcel.inpamex.payload.request.Tarjeta;
import com.addcel.inpamex.payload.request.TarjetaAsignar;
import com.addcel.inpamex.payload.request.TarjetaTransaccion;
import com.addcel.inpamex.payload.response.TarjetaAsignResponse;
import com.addcel.inpamex.payload.response.TarjetaIndividualResponse;
import com.addcel.inpamex.payload.response.TarjetaResponse;
import com.addcel.inpamex.payload.response.TarjetaTransaccionCodigoBarras;
import com.addcel.inpamex.payload.response.TarjetasGeneralResponse;
import com.addcel.inpamex.payload.response.TransaccionResponse;
import com.addcel.inpamex.payload.ws.inpamex.ClientResponseConfirmTransfer;
import com.addcel.inpamex.service.ws.inpamex.InpamexWsService;
import com.addcel.inpamex.utils.Utils;
import com.addcel.utils.AddcelCrypto;

@Service
@Transactional
public class TarjetaServiceImpl implements TarjetaService {
	
	@Value("${error.codigo.tarjeta}")
	private String errorCodigoTarjeta;

	@Value("${error.mensaje.tarjeta}")
	private String errorMensajeTarjeta;
	
	@Value("${tarjeta.estatus.codigo.activa}")
	private String tarjetaEstatusCodigoActiva;
	
	@Value("${tarjeta.estatus.desc.activa}")
	private String tarjetaEstatusDescActiva;
	
	@Value("${error.codigo.tarjeta.inactiva}")
	private String errorCodigoTarjetaInactiva;

	@Value("${error.mensaje.tarjeta.inactiva}")
	private String errorMensajeTarjetaInactiva;
	
	@Value("${error.codigo.usuario}")
	private String errorCodigoUsuario;

	@Value("${error.mensaje.usuario}")
	private String errorMensajeUsuario;
	
	@Value("${usuario.estatus.codigo.activo}")
	private String usuarioCodigoEstatusActivo;
	
	@Value("${error.codigo.usuario.inactivo}")
	private String errorCodigoUsuarioInactivo;

	@Value("${error.mensaje.usuario.inactivo}")
	private String errorMensajeUsuarioInactivo;
	
	@Value("${error.codigo.tarjeta.saldo}")
	private String errorCodigoTarjetaSaldo;

	@Value("${error.mensaje.tarjeta.saldo}")
	private String errorMensajeTarjetaSaldo;
	
	@Value("${transaccion.tipo.prefix.cargo}")
	private String transaccionPrefijoCargo;
	
	@Value("${transac.estatus.codigo.cargo}")
	private String transaccionCodigoCargo;

	@Value("${transac.estatus.desc.cargo}")
	private String transaccionDescCargo;
	
	@Value("${error.codigo.tarjeta.existente}")
	private String errorCodigoTarjetaExistente;

	@Value("${error.mensaje.tarjeta.existente}")
	private String errorMensajeTarjetaExistente;
	
	@Value("${error.codigo.tarjeta.asignada}")
	private String tarjetaCodigoAsignada;
	
	@Value("${error.mensaje.tarjeta.asignada}")
	private String tarjetaMensajeAsignada;
	
	@Value("${tarjeta.estatus.codigo.inactiva}")
	private String tarjetaEstatusCodigoInactiva;

	@Value("${tarjeta.estatus.desc.inactiva}")
	private String tarjetaEstatusDescInactiva;
	
	@Value("${trans.estatus.codigo.disponible}")
	private String transCodigoEstatusDisponible;
	
	@Value("${trans.estatus.codigo.abonada}")
	private String transCodigoEstatusAbonada;

	@Value("${trans.estatus.desc.abonada}")
	private String transDescEstatusAbonada;
	
	@Value("${transaccion.tipo.prefix.abono}")
	private String prefijoTransAbono;
	
	@Value("${transac.estatus.codigo.abono}")
	private String transaccionCodigoAbono;

	@Value("${transac.estatus.desc.abono}")
	private String transaccionDescAbono;

	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;

	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;

	@Value("${exito.codigo}")
	private String exitoCodigo;

	@Value("${exito.mensaje}")
	private String exitoMensaje;

	@Autowired
	private TarjetaInpamexDao tarjetaInpamexDao;

	@Autowired
	private UsuariosInpamexDao usuariosInpamexDao;

	@Autowired
	private TransaccionesInpamexDao transaccionesInpamexDao;

	@Autowired
	private TransferenciasInpamexDao transferenciasInpamexDao;
	
	@Autowired
	private InpamexWsService inpamexWsService;

	private final static Logger logger = Logger.getLogger(TarjetaServiceImpl.class);

	@Override
	public ResponseEntity<TarjetaResponse> getBalanceTarjetaCodigoBarras(String codigoBarras, String claveCadena) {
		
		logger.info("####################################################");
		logger.info("STARTING CONSULTA SALDO TARJETA POR CODIGO BARRAS CODIGO - " + codigoBarras +" CLAVE CADENA - " + claveCadena);

		Optional<TarjetaInpamex> resultTarjeta = Optional.empty();

		ResponseEntity<TarjetaResponse> responseEntity = null;
		TarjetaResponse tarjetaResponse = null;

		try {
			
			String cryptoCb = AddcelCrypto.encryptHard(codigoBarras.trim());

//			Validando Existencia de Tarjeta
			resultTarjeta = tarjetaInpamexDao.findTarjetaCodigoBarrasClaveCadena(cryptoCb, claveCadena.trim());

			if (!resultTarjeta.isPresent()) {
				
				logger.error("ERROR AL REALIZAR LA TRANSACCION TARJETA NO ENCONTRADA EN BD CODIGO - " + codigoBarras);
				tarjetaResponse = new TarjetaResponse();
				tarjetaResponse.setIdError(Integer.parseInt(errorCodigoTarjeta));
				tarjetaResponse.setMensajeError(errorMensajeTarjeta);
				return new ResponseEntity<TarjetaResponse>(tarjetaResponse, HttpStatus.OK);
				
			} else {
				
				tarjetaResponse = new TarjetaResponse();

				tarjetaResponse.setIdTarjeta(resultTarjeta.get().getIdTarjeta());
				tarjetaResponse.setIdUsuario(resultTarjeta.get().getIdUsuario());
				tarjetaResponse.setSaldo(resultTarjeta.get().getSaldo());
				tarjetaResponse.setEstatus(resultTarjeta.get().getEstatus());
				tarjetaResponse.setEstatusDescripcion(resultTarjeta.get().getEstatusDescripcion());
				tarjetaResponse.setIdError(Integer.parseInt(exitoCodigo));
				tarjetaResponse.setMensajeError(exitoMensaje);
				Optional<UsuariosInpamex> inpamex = usuariosInpamexDao.findById(resultTarjeta.get().getIdUsuario());
				tarjetaResponse.setNombreCompleto(inpamex.get().getNombre()+" "+inpamex.get().getApaterno()+" "+inpamex.get().getAmaterno());
				logger.info(
						"PROCESO FINALIZADO EXITOSAMENTE CONSULTA DE SALDO POR CODIGO BARRAS CODIGO - " + codigoBarras);
				responseEntity = new ResponseEntity<TarjetaResponse>(tarjetaResponse, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			
			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE SALDO POR CODIGO BARRAS GETBALANCETARJETA - " + e.getMessage());
			tarjetaResponse = new TarjetaResponse();

			tarjetaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetaResponse>(tarjetaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		return responseEntity;
	}

		@Override
		public ResponseEntity<TransaccionResponse> doTransaccionCodigoBarras(TarjetaTransaccionCodigoBarras transaccion) {
		
			logger.info("####################################################");
			logger.info("STARTING TRANSACCION NUEVA POR CODIGO BARRAS - " + transaccion.toString());
	
			Optional<TarjetaInpamex> resultTarjeta = Optional.empty();
			Optional<UsuariosInpamex> resultUsuario = Optional.empty();
			Optional<TarjetaInpamex> resultTarjetaFinal = Optional.empty();
			Optional<TransaccionesInpamex> resultTransaccion = Optional.empty();
	
			ResponseEntity<TransaccionResponse> responseEntity = null;
			TransaccionResponse transaccionResponse = null;
			Double saldoFinal = 0.0;
	
			boolean saldoMayorCargo = false;
			
			try {
				
				String cryptoCodigoBarras = AddcelCrypto.encryptHard(transaccion.getCodigoBarras().trim());
	
	//			Validando Existencia de Tarjeta
				logger.info("STARTING ENCONTRAR TARJETA EN BD");
				resultTarjeta = tarjetaInpamexDao.findTarjetaCodigoBarrasClaveCadena(cryptoCodigoBarras, transaccion.getClaveCadena().trim());
				logger.info("END ENCONTRAR TARJETA EN BD - " + resultTarjeta.isPresent());
	
				if (!resultTarjeta.isPresent()) {
					logger.error("ERROR AL REALIZAR LA TRANSACCION TARJETA NO ENCONTRADA EN BD CODIGO - "
							+ transaccion.getCodigoBarras());
					transaccionResponse = new TransaccionResponse();
					transaccionResponse.setIdError(Integer.parseInt(errorCodigoTarjeta));
					transaccionResponse.setMensajeError(errorMensajeTarjeta);
					return new ResponseEntity<TransaccionResponse>(transaccionResponse, HttpStatus.BAD_REQUEST);
				} else {
	//				Valindando Estatus de Tarjeta
					logger.info("STARTING VALIDAR ESTATUS DE TARJETA EN BD - " + resultTarjeta.get().getEstatusDescripcion());
					if (!tarjetaEstatusCodigoActiva.equals(resultTarjeta.get().getEstatus().toString())) {
						logger.error("ERROR AL REALIZAR LA TRANSACCION TARJETA INACTIVA EN BD CODIGO - "
								+ transaccion.getCodigoBarras());
						transaccionResponse = new TransaccionResponse();
						transaccionResponse.setIdError(Integer.parseInt(errorCodigoTarjetaInactiva));
						transaccionResponse.setMensajeError(errorMensajeTarjetaInactiva);
						return new ResponseEntity<TransaccionResponse>(transaccionResponse, HttpStatus.BAD_REQUEST);
					}
				}
				
	//			Validando Existencia de Usuario
				if (resultTarjeta.isPresent()) {
					logger.info("STARTING ENCONTRAR USUARIO EN BD - " + resultTarjeta.get().getIdUsuario());
					resultUsuario = usuariosInpamexDao.findById(resultTarjeta.get().getIdUsuario());
					logger.info("END ENCONTRAR USUARIO EN BD - " + resultUsuario.isPresent());
	
					if (!resultUsuario.isPresent()) {
						logger.error("ERROR AL REALIZAR LA TRANSACCION USUARIO NO ENCONTRADO EN LA BD - "
								+ resultTarjeta.get().getIdUsuario());
						transaccionResponse = new TransaccionResponse();
						transaccionResponse.setIdError(Integer.parseInt(errorCodigoUsuario));
						transaccionResponse.setMensajeError(errorMensajeUsuario);
						return new ResponseEntity<TransaccionResponse>(transaccionResponse, HttpStatus.BAD_REQUEST);
					} else {
	
	//						Validando Estatus de Usuario
						logger.info("STARTING VALIDAR ESTATUS DE USUARIO EN BD - " + resultUsuario.get().getEstatusDescripcion());
						if (!usuarioCodigoEstatusActivo.equals(resultUsuario.get().getEstatus().toString())) {
							logger.error(
									"ERROR AL REALIZAR LA TRANSACCION USUARIO INACTIVO - " + resultUsuario.get().getIdUsuario());
							transaccionResponse = new TransaccionResponse();
							transaccionResponse.setIdError(Integer.parseInt(errorCodigoUsuarioInactivo));
							transaccionResponse.setMensajeError(errorMensajeUsuarioInactivo);
							return new ResponseEntity<TransaccionResponse>(transaccionResponse, HttpStatus.BAD_REQUEST);
						}
	
					}
				}
				
	//			Validando Saldo Mayor a Cobro
				if (resultTarjeta.isPresent() & resultTarjeta.isPresent()) {
					saldoMayorCargo = this.validarSaldoVsCargo(resultTarjeta.get().getSaldo(), transaccion.getMonto());
					if (!saldoMayorCargo) {
						logger.error("ERROR AL REALIZAR LA TRANSACCION POR CODIGO BARRAS SALDO NO MAYOR A CARGO, SALDO - "
								+ resultTarjeta.get().getSaldo() + " CARGO - " + transaccion.getMonto());
						transaccionResponse = new TransaccionResponse();
						transaccionResponse.setIdError(Integer.parseInt(errorCodigoTarjetaSaldo));
						transaccionResponse.setMensajeError(errorMensajeTarjetaSaldo);
						return new ResponseEntity<TransaccionResponse>(transaccionResponse, HttpStatus.OK);
					}
				}
				
	//			Aplicando Cargo a Tarjeta
				if (resultTarjeta.isPresent() & resultTarjeta.isPresent() & saldoMayorCargo) {
					saldoFinal = this.descontarCargo(resultTarjeta.get().getSaldo(), transaccion.getMonto());
	
	//				Actualizando Saldo de Tarjeta en BD
					resultTarjetaFinal = this.actualizarSaldo(resultTarjeta, saldoFinal);
					if (!resultTarjetaFinal.isPresent()) {
						logger.error(
								"ERROR AL REALIZAR LA TRANSACCION POR CODIGO BARRAS NO SE PUDO ACTUALIZAR SALDO EN TARJETA");
						transaccionResponse = new TransaccionResponse();
						transaccionResponse.setIdError(Integer.parseInt(errorCodigoTarjetaInactiva));
						transaccionResponse.setMensajeError(errorMensajeTarjetaInactiva);
						return new ResponseEntity<TransaccionResponse>(transaccionResponse,
								HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
				
	//			Insertar Transaccion
				if (resultTarjeta.isPresent() & resultTarjeta.isPresent() & resultTarjetaFinal.isPresent()) {
	
					TarjetaTransaccion tarjetaTransaccion = new TarjetaTransaccion();
	
					tarjetaTransaccion.setIdUsuario(resultUsuario.get().getIdUsuario());
					tarjetaTransaccion.setIdTarjeta(resultTarjeta.get().getIdTarjeta());
					tarjetaTransaccion.setMonto(transaccion.getMonto());
					tarjetaTransaccion.setConcepto(transaccion.getConcepto().trim());
					tarjetaTransaccion.setClaveCadena(transaccion.getClaveCadena().trim());
	
					resultTransaccion = this.guardarTransaccionCargo(tarjetaTransaccion);
					if (!resultTransaccion.isPresent()) {
						logger.error(
								"ERROR AL REALIZAR LA TRANSACCION POR CODIGO BARRAS NO SE PUDO GUARDAR LA TRANSACCION");
						transaccionResponse = new TransaccionResponse();
						transaccionResponse.setIdError(Integer.parseInt(errorCodigoTarjetaInactiva));
						transaccionResponse.setMensajeError(errorMensajeTarjetaInactiva);
						return new ResponseEntity<TransaccionResponse>(transaccionResponse,
								HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
				
				if (resultTransaccion.isPresent()) {
	
					transaccionResponse = new TransaccionResponse();
	
					transaccionResponse.setIdTransaccion(resultTransaccion.get().getIdTransaccion());
					transaccionResponse.setIdUsuario(resultTransaccion.get().getIdUsuario());
					transaccionResponse.setIdTarjeta(resultTransaccion.get().getIdTarjeta());
					transaccionResponse.setSaldo(resultTarjetaFinal.get().getSaldo());
					transaccionResponse.setNoAutorizacion(resultTransaccion.get().getNumeroAutorizacion());
					transaccionResponse.setEstatus(resultTransaccion.get().getEstatus());
					transaccionResponse.setEstatusDescripcion(resultTransaccion.get().getEstatusDescripcion());
					Optional<UsuariosInpamex> inpamex = usuariosInpamexDao.findById(resultTransaccion.get().getIdUsuario());
					transaccionResponse.setNombreCompleto(inpamex.get().getNombre()+" "+inpamex.get().getApaterno()+" "+inpamex.get().getAmaterno());
					logger.info("Estp es UsuarioInpamex "+inpamex);
					transaccionResponse.setIdError(Integer.parseInt(exitoCodigo));
					transaccionResponse.setMensajeError(exitoMensaje);
	
					logger.info("PROCESO FINALIZADO EXITOSAMENTE CARGO A TARJETA POR CODIGO BARRAS NOAUTORIZACION - "
							+ resultTransaccion.get().getNumeroAutorizacion() + " - IDTRANSACCION - "
							+ resultTransaccion.get().getIdTransaccion());
					return new ResponseEntity<TransaccionResponse>(transaccionResponse, HttpStatus.OK);
				}
				
				
			} catch (Exception e) {
				logger.error("ERROR AL REALIZAR TRANSACCION DOTRANSACCION - " + e.getMessage());
				transaccionResponse = new TransaccionResponse();
				transaccionResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
				transaccionResponse.setMensajeError(errorMensajeErrorInterno);
				responseEntity = new ResponseEntity<TransaccionResponse>(transaccionResponse,
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			return responseEntity;
		}
	
	@Override
	public ResponseEntity<TarjetasGeneralResponse> saveTarjeta(Tarjeta tarjeta) {
		
		logger.info("####################################################");
		logger.info("STARTING GUARDAR TARJETA NUMERO - " + tarjeta.getNumero());

		ResponseEntity<TarjetasGeneralResponse> responseEntity = null;
		List<TarjetaIndividualResponse> listTarjetaResponse = null;
		
		try {
			
//			Validar Existencia de Tarjeta
			String cryptoNumero = AddcelCrypto.encryptHard(tarjeta.getNumero().trim());

			Optional<TarjetaInpamex> optTarjeta = tarjetaInpamexDao.findTarjetaNumero(cryptoNumero);

			if (optTarjeta.isPresent()) {
				logger.error("ERROR AL GUARDAR TARJETA TARJETA EXISTENTE EN BD TARJETA - " + tarjeta.getNumero()
							+ " IDTARJETA - " + optTarjeta.get().getIdTarjeta());
				TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();
				tarjetasGeneralResponse.setIdError(Integer.parseInt(errorCodigoTarjetaExistente));
				tarjetasGeneralResponse.setMensajeError(this.generarMensajeErrorTarjeta(tarjeta.getNumero()));
				return new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse, HttpStatus.BAD_REQUEST);
			}
			
			List<Tarjeta> tarjetas = new ArrayList<>();
			tarjetas.add(tarjeta);
			
//			Guardar Tarjetas
			listTarjetaResponse = this.guardarTarjetas(tarjetas);
			if (listTarjetaResponse != null & listTarjetaResponse.size() > 0) {
				logger.info("PROCESO FINALIZADO EXITOSAMENTE GUARDAR TARJETA NUMERO - "
						+ tarjetas.size());
				TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();

				tarjetasGeneralResponse.setIdError(Integer.parseInt(exitoCodigo));
				tarjetasGeneralResponse.setMensajeError(exitoMensaje);
				tarjetasGeneralResponse.setTarjetas(listTarjetaResponse);
				responseEntity = new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			logger.error("ERROR AL GUARDAR TARJETAS - " + e.getMessage());
			TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();
			tarjetasGeneralResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetasGeneralResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
	}

	@Override
	public ResponseEntity<TarjetasGeneralResponse> saveTarjetas(List<Tarjeta> tarjetas) {
		
		logger.info("####################################################");
		logger.info("STARTING GUARDAR TARJETAS NUMERO TARJETAS - " + tarjetas.size());

		ResponseEntity<TarjetasGeneralResponse> responseEntity = null;
		List<TarjetaIndividualResponse> listTarjetaResponse = null;

		try {

//			Validar Existencia de Tarjeta
			for (Tarjeta tarjeta : tarjetas) {
				String cryptoNumero = AddcelCrypto.encryptHard(tarjeta.getNumero().trim());

				Optional<TarjetaInpamex> optTarjeta = tarjetaInpamexDao.findTarjetaNumero(cryptoNumero);

				if (optTarjeta.isPresent()) {
					logger.error("ERROR AL GUARDAR TARJETA TARJETA EXISTENTE EN BD TARJETA - " + tarjeta.getNumero()
							+ " IDTARJETA - " + optTarjeta.get().getIdTarjeta());
					TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();
					tarjetasGeneralResponse.setIdError(Integer.parseInt(errorCodigoTarjetaExistente));
					tarjetasGeneralResponse.setMensajeError(this.generarMensajeErrorTarjeta(tarjeta.getNumero()));
					return new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse, HttpStatus.BAD_REQUEST);
				}
			}

//			Guardar Tarjetas
			listTarjetaResponse = this.guardarTarjetas(tarjetas);
			if (listTarjetaResponse != null & listTarjetaResponse.size() > 0) {
				logger.info("PROCESO FINALIZADO EXITOSAMENTE GUARDAR TARJETAS NUMERO TARJETAS GUARDADAS - "
						+ tarjetas.size());
				TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();

				tarjetasGeneralResponse.setIdError(Integer.parseInt(exitoCodigo));
				tarjetasGeneralResponse.setMensajeError(exitoMensaje);
				tarjetasGeneralResponse.setTarjetas(listTarjetaResponse);
				responseEntity = new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse, HttpStatus.OK);
			}

		} catch (Exception e) {
			logger.error("ERROR AL GUARDAR TARJETAS - " + e.getMessage());
			TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();
			tarjetasGeneralResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetasGeneralResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
		
	}

	@Override
	public ResponseEntity<TarjetaAsignResponse> asignTarjeta(TarjetaAsignar tarjetaAsignar) {
		
		logger.info("####################################################");
		logger.info("STARTING ASIGNAR TARJETA A USUARIO - " + tarjetaAsignar.toString());

		ResponseEntity<TarjetaAsignResponse> responseEntity = null;
		ResponseEntity<ClientResponseConfirmTransfer> responseEntityInpamex = null;
		TarjetaAsignResponse tarjetaResponse = null;
		
		Optional<UsuariosInpamex> resultUsuario = Optional.empty();
		Optional<TarjetaInpamex> resultTarjeta = Optional.empty();
		Optional<List<TarjetaInpamex>> listaTarjetas = Optional.empty();
		Optional<TarjetaInpamex> resultTarjetaAsignada = Optional.empty();
		
		Integer tarjetasDesasignadas = 0;
		
		try {
			
//			Validando Existencia de Usuario
				logger.info("STARTING ENCONTRAR USUARIO EN BD - " + tarjetaAsignar.getIdUsuario());
				resultUsuario = usuariosInpamexDao.findById(tarjetaAsignar.getIdUsuario());
				logger.info("END ENCONTRAR USUARIO EN BD - " + resultUsuario.isPresent());

				if (!resultUsuario.isPresent()) {
					logger.error("ERROR AL REALIZAR LA OPERACION USUARIO NO ENCONTRADO EN LA BD - "
							+ tarjetaAsignar.getIdUsuario());
					tarjetaResponse = new TarjetaAsignResponse();
					tarjetaResponse.setIdError(Integer.parseInt(errorCodigoUsuario));
					tarjetaResponse.setMensajeError(errorMensajeUsuario);
					return new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.BAD_REQUEST);
				} else {

//						Validando Estatus de Usuario
					logger.info("STARTING VALIDAR ESTATUS DE USUARIO EN BD - " + resultUsuario.get().getEstatusDescripcion());
					if (!usuarioCodigoEstatusActivo.equals(resultUsuario.get().getEstatus().toString())) {
						logger.error(
								"ERROR AL REALIZAR LA OPERACION USUARIO INACTIVO - " + resultUsuario.get().getIdUsuario());
						tarjetaResponse = new TarjetaAsignResponse();
						tarjetaResponse.setIdError(Integer.parseInt(errorCodigoUsuarioInactivo));
						tarjetaResponse.setMensajeError(errorMensajeUsuarioInactivo);
						return new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.BAD_REQUEST);
					}
				}
				
//				Validando Existencia de Tarjeta
				if (resultUsuario.isPresent()) {
					logger.info("STARTING ENCONTRAR TARJETA EN BD");
					resultTarjeta = tarjetaInpamexDao.findById(tarjetaAsignar.getIdTarjeta());
					logger.info("END ENCONTRAR TARJETA EN BD - " + resultTarjeta.isPresent());

					if (!resultTarjeta.isPresent()) {
						logger.error("ERROR AL REALIZAR LA OPERACION TARJETA NO ENCONTRADA EN BD ID - "
								+ tarjetaAsignar.getIdTarjeta());
						tarjetaResponse = new TarjetaAsignResponse();
						tarjetaResponse.setIdError(Integer.parseInt(errorCodigoTarjeta));
						tarjetaResponse.setMensajeError(errorMensajeTarjeta);
						return new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.BAD_REQUEST);
					} else {
//						Valindando Estatus de Tarjeta
						logger.info("STARTING VALIDAR ESTATUS DE TARJETA EN BD - " + resultTarjeta.get().getEstatusDescripcion());
						if (!tarjetaEstatusCodigoActiva.equals(resultTarjeta.get().getEstatus().toString())) {
							logger.error("ERROR AL REALIZAR LA OPERACION TARJETA INACTIVA EN BD ID - "
									+ tarjetaAsignar.getIdTarjeta());
							tarjetaResponse = new TarjetaAsignResponse();
							tarjetaResponse.setIdError(Integer.parseInt(errorCodigoTarjetaInactiva));
							tarjetaResponse.setMensajeError(errorMensajeTarjetaInactiva);
							return new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.BAD_REQUEST);
						}
					}
				}
				
//				Validando si tarjeta a asignar tiene usuario asignado previamente
				if (resultUsuario.isPresent() & resultTarjeta.isPresent()) {
						if (resultTarjeta.get().getIdUsuario() > 0) {
							logger.error("LA TARJETA A ASIGNAR YA SE ENCUENTRA ASIGNADA IDTARJETA - " + tarjetaAsignar.getIdTarjeta());
							tarjetaResponse = new TarjetaAsignResponse();
							tarjetaResponse.setIdError(Integer.parseInt(tarjetaCodigoAsignada));
							tarjetaResponse.setMensajeError(tarjetaMensajeAsignada);
							return new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.BAD_REQUEST);
					}
				}
				
//				Validando Si Usuario tiene Tarjetas Asignadas Previamente
				if (resultUsuario.isPresent() & resultTarjeta.isPresent()) {
					listaTarjetas = this.findTarjetasUsuario(tarjetaAsignar.getIdUsuario());
//					En Caso de Tener Tarjetas Asignadas se Deshabilitan
					if (listaTarjetas.isPresent()) {
						tarjetasDesasignadas = this.deshabilitarTarjetasUsuario(listaTarjetas);

						if (tarjetasDesasignadas <= 0) {
							logger.error(
									"ERROR AL REALIZAR LA OPERACION NO SE PUDIERON DESASIGNAR LAS TARJETAS AL USUARIO IDUSUARIO "
											+ tarjetaAsignar.getIdUsuario());
							tarjetaResponse = new TarjetaAsignResponse();
							tarjetaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
							tarjetaResponse.setMensajeError(errorMensajeErrorInterno);
							return new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
						}
				
					}
				}
				
//				Asignar Tarjeta
				if (resultUsuario.isPresent() & resultTarjeta.isPresent()) {
					resultTarjetaAsignada = this.asignarTarjeta(resultTarjeta, tarjetaAsignar.getIdUsuario());
					if (!resultTarjetaAsignada.isPresent()) {
						logger.error(
								"ERROR AL REALIZAR LA OPERACION NO SE PUDO ASIGNAR LA TARJETA AL USUARIO IDUSUARIO - "
										+ tarjetaAsignar.getIdUsuario() + " TARJETA - " + resultTarjeta.get());
						tarjetaResponse = new TarjetaAsignResponse();
						tarjetaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
						tarjetaResponse.setMensajeError(errorMensajeErrorInterno);
						return new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
					}
				}
				
				if (resultUsuario.isPresent() & resultTarjeta.isPresent() & resultTarjetaAsignada.isPresent()) {
					tarjetaResponse = new TarjetaAsignResponse();
					tarjetaResponse.setIdTarjeta(resultTarjetaAsignada.get().getIdTarjeta());
					tarjetaResponse.setIdUsuario(resultTarjetaAsignada.get().getIdUsuario());
					tarjetaResponse.setSaldo(resultTarjetaAsignada.get().getSaldo());
					tarjetaResponse.setEstatus(resultTarjetaAsignada.get().getEstatus());
					tarjetaResponse.setEstatusDescripcion(resultTarjetaAsignada.get().getEstatusDescripcion());
					tarjetaResponse.setIdError(Integer.parseInt(exitoCodigo));
					tarjetaResponse.setMensajeError(exitoMensaje);
					logger.info("PROCESO FINALIZADO EXITOSAMENTE ASIGNAR TARJETA A USUARIO IDUSUARIO - "
							+ tarjetaAsignar.getIdTarjeta() + " TARJETA - " + resultTarjetaAsignada.get().getIdTarjeta());
					return new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.CREATED);
				}
				
		} catch (Exception e) {
			logger.error("ERROR AL ASIGNAR TARJETA - " + e.getLocalizedMessage());
			tarjetaResponse = new TarjetaAsignResponse();
			tarjetaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
	}
	
	@Override
	public ResponseEntity<TarjetaResponse> findTarjetaNumero(String numero) {
		
		logger.info("####################################################");
		logger.info("STARTING CONSULTA DE TARJETA POR NUMERO - " + numero);

		Optional<TarjetaInpamex> resultTarjeta = Optional.empty();

		ResponseEntity<TarjetaResponse> responseEntity = null;
		TarjetaResponse tarjetaResponse = null;
		
try {
			
			String cryptoCb = AddcelCrypto.encryptHard(numero.trim());

//			Validando Existencia de Tarjeta
			resultTarjeta = tarjetaInpamexDao.findTarjetaNumero(cryptoCb);

			if (!resultTarjeta.isPresent()) {
				
				logger.error("ERROR AL REALIZAR LA TRANSACCION TARJETA NO ENCONTRADA EN BD NUMERO - " + numero);
				tarjetaResponse = new TarjetaResponse();
				tarjetaResponse.setIdError(Integer.parseInt(errorCodigoTarjeta));
				tarjetaResponse.setMensajeError(errorMensajeTarjeta);
				return new ResponseEntity<TarjetaResponse>(tarjetaResponse, HttpStatus.OK);
				
			} else {
				
				tarjetaResponse = new TarjetaResponse();

				tarjetaResponse.setIdTarjeta(resultTarjeta.get().getIdTarjeta());
				tarjetaResponse.setIdUsuario(resultTarjeta.get().getIdUsuario());
				tarjetaResponse.setSaldo(resultTarjeta.get().getSaldo());
				tarjetaResponse.setEstatus(resultTarjeta.get().getEstatus());
				tarjetaResponse.setEstatusDescripcion(resultTarjeta.get().getEstatusDescripcion());
				tarjetaResponse.setIdError(Integer.parseInt(exitoCodigo));
				tarjetaResponse.setMensajeError(exitoMensaje);
				Optional<UsuariosInpamex> inpamex = usuariosInpamexDao.findById(resultTarjeta.get().getIdUsuario());
				tarjetaResponse.setNombreCompleto(inpamex.get().getNombre()+" "+inpamex.get().getApaterno()+" "+inpamex.get().getAmaterno());

				logger.info(
						"PROCESO FINALIZADO EXITOSAMENTE CONSULTA DE TARJETA POR NUMERO - " + numero);
				responseEntity = new ResponseEntity<TarjetaResponse>(tarjetaResponse, HttpStatus.OK);
			}
			
		} catch (Exception e) {
			
			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE SALDO POR CODIGO BARRAS GETBALANCETARJETA - " + e.getMessage());
			tarjetaResponse = new TarjetaResponse();

			tarjetaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetaResponse>(tarjetaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		return responseEntity;
	}

	public boolean validarSaldoVsCargo(Double saldo, Double cargo) {
		logger.info("STARTING VALIDAR SALDO VS COBRO SALDO - " + saldo + " COBRO - " + cargo);
		boolean result = false;
		try {
			if (saldo >= cargo) {
				result = true;
			}
			logger.info("END VALIDAR SALDO VS COBRO SALDO MAYOR A COBRO - " + result);
		} catch (Exception e) {
			logger.error("ERROR VALIDAR SALDO VS COBRO - " + e.getLocalizedMessage());
		}
		return result;
	}
	
	public Double descontarCargo(Double saldo, Double cargo) {
		logger.info("STARTING DESCONTAR CARGO A SALDO, SALDO - " + saldo + " CARGO - " + cargo);
		Double result = 0.00;
		try {
			result = saldo - cargo;
			logger.info("END DESCONTAR CARGO A SALDO " + result);
		} catch (Exception e) {
			logger.error("ERROR DESCONTAR CARGO - " + e.getLocalizedMessage());
		}
		return result;
	}
	
	public Optional<TarjetaInpamex> actualizarSaldo(Optional<TarjetaInpamex> optTarjeta, Double saldoActual) {
		logger.info("STARTING ACTUALIZAR SALDO EN TARJETA, SALDOACTUAL - " + saldoActual + " ID TARJETA - "
				+ optTarjeta.get().getIdTarjeta());
		Optional<TarjetaInpamex> result = Optional.empty();
		try {
			TarjetaInpamex tarjetaInpamex = optTarjeta.get();
			tarjetaInpamex.setSaldo(saldoActual);

			TarjetaInpamex tarjetaInpamexResult = tarjetaInpamexDao.save(tarjetaInpamex);
			result = Optional.of(tarjetaInpamexResult);
			logger.info("END ACTUALIZAR SALDO EN TARJETA, TARJETA - " + optTarjeta.get().getIdTarjeta());
		} catch (Exception e) {
			logger.error("ERROR ACTUALIZAR SALDO EN TARJETA - " + e.getLocalizedMessage());
		}
		return result;
	}
	
	public Optional<TransaccionesInpamex> guardarTransaccionCargo(TarjetaTransaccion transaccion) {
		logger.info("STARTING GUARDAR TRANSACCION DE CARGO EN BD - " + transaccion.toString());
		Optional<TransaccionesInpamex> result = Optional.empty();
		try {

			Utils utils = new Utils();
			String noAutorizacion = transaccionPrefijoCargo;
			noAutorizacion = transaccionPrefijoCargo.concat(utils.dateTimeMilliSecond());

			TransaccionesInpamex transaccionesInpamex = new TransaccionesInpamex();

			transaccionesInpamex.setIdUsuario(transaccion.getIdUsuario());
			transaccionesInpamex.setIdTransferencia(0L);
			transaccionesInpamex.setIdTarjeta(transaccion.getIdTarjeta());
			transaccionesInpamex.setIdMto("");

			transaccionesInpamex.setTipo("CARGO");
			transaccionesInpamex.setMonto(transaccion.getMonto());
			transaccionesInpamex.setConcepto(transaccion.getConcepto().toUpperCase().trim());
			transaccionesInpamex.setFecha(new Date());
			transaccionesInpamex.setEstatus(transaccionCodigoCargo.charAt(0));
			transaccionesInpamex.setEstatusDescripcion(transaccionDescCargo);
			transaccionesInpamex.setNumeroAutorizacion(noAutorizacion);
			transaccionesInpamex.setClaveCadena(transaccion.getClaveCadena().toUpperCase().trim());
			transaccionesInpamex.setComentario("CARGO APLICADO EXITOSAMENTE POR " + transaccion.getMonto());

			TransaccionesInpamex transaccionesInpamexResult = transaccionesInpamexDao.save(transaccionesInpamex);
			result = Optional.of(transaccionesInpamexResult);

			logger.info("END GUARDAR TRANSACCION DE CARGO EN BD IDTRANSACCION - "
					+ transaccionesInpamexResult.getIdTransaccion());
		} catch (Exception e) {
			logger.error("ERROR AL GUARDAR TRANSACCION DE CARGO - " + e.getLocalizedMessage());
		}
		return result;
	}
	
	public List<TarjetaIndividualResponse> guardarTarjetas(List<Tarjeta> tarjetas) {

		logger.info("STARTING SAVE TARJETAS NUMERO TARJETAS - " + tarjetas.size());
		List<TarjetaIndividualResponse> result = null;
		TarjetaIndividualResponse tarjetaResponse = null;

		try {

			result = new ArrayList<TarjetaIndividualResponse>();
			Long idUsuarioDefault = 0L;
			Double saldoDefault = 0.0;
			String cryptoCodigoVerificacion = "";

			for (Tarjeta tarjeta : tarjetas) {
				
				if (tarjeta.getCodigoVerificacion() != null & !"".equals(tarjeta.getCodigoVerificacion())) {
					String codigoVerificacion = tarjeta.getCodigoVerificacion().trim();
					cryptoCodigoVerificacion = AddcelCrypto.encryptHard(codigoVerificacion);
				}

				String cryptoNumero = AddcelCrypto.encryptHard(tarjeta.getNumero().trim());
				String cryptoSerie = AddcelCrypto.encryptHard(tarjeta.getSerie().trim());
				String cryptoCodigoBarras = AddcelCrypto.encryptHard(tarjeta.getCodigoBarras().trim());

				TarjetaInpamex tarjetaInpamex = new TarjetaInpamex();

				tarjetaInpamex.setIdUsuario(idUsuarioDefault);
				tarjetaInpamex.setNumero(cryptoNumero);
				tarjetaInpamex.setSerie(cryptoSerie);
				tarjetaInpamex.setCodigoBarras(cryptoCodigoBarras);
				tarjetaInpamex.setCodigoVerificacion(cryptoCodigoVerificacion);
				tarjetaInpamex.setSaldo(saldoDefault);
				tarjetaInpamex.setClaveCadena(tarjeta.getClaveCadena().trim());
				tarjetaInpamex.setFechaRegistro(new Date());
				tarjetaInpamex.setEstatus(tarjetaEstatusCodigoActiva.charAt(0));
				tarjetaInpamex.setEstatusDescripcion(tarjetaEstatusDescActiva);
				tarjetaInpamex.setFechaModificacion(new Date());

				TarjetaInpamex tarjetaInpamexResult = tarjetaInpamexDao.save(tarjetaInpamex);

				tarjetaResponse = new TarjetaIndividualResponse();
				tarjetaResponse.setIdTarjeta(tarjetaInpamexResult.getIdTarjeta());
				tarjetaResponse.setIdUsuario(tarjetaInpamexResult.getIdUsuario());
				tarjetaResponse.setNumeroTarjeta(tarjeta.getNumero());
				tarjetaResponse.setSaldo(tarjetaInpamexResult.getSaldo());
				tarjetaResponse.setEstatus(tarjetaInpamexResult.getEstatus());
				tarjetaResponse.setEstatusDescripcion(tarjetaInpamexResult.getEstatusDescripcion());

				result.add(tarjetaResponse);
				logger.info("TARJETA AGREGADA CORRECTAMENTE IDTARJETA - "
						+ tarjetaInpamexResult.getIdTarjeta());
			}

			logger.info("END SAVE TARJETAS NUMERO TARJETAS - " + tarjetas.size());
		} catch (Exception e) {
			result = new ArrayList<TarjetaIndividualResponse>();
			logger.error("ERROR AL GUARDAR TARJETAS - " + e.getLocalizedMessage());
		}
		return result;
	}
	
	public String generarMensajeErrorTarjeta(String noTarjeta) {
		String errorTarjeta = errorMensajeTarjetaExistente;
		try {
			errorTarjeta = errorTarjeta + " NUMERO TARJETA - " + noTarjeta;
		} catch (Exception e) {
			logger.error("ERROR GENERAR MENSAJE ERROR TARJETAS EXISTENTES - " + e.getLocalizedMessage());
		}
		return errorTarjeta;
	}
	
	public Optional<List<TarjetaInpamex>> findTarjetasUsuario(Long idUsuario) {
		logger.info("STARTING ENCONTRAR TARJETAS ASIGNADAS A USUARIO IDUSUARIO - " + idUsuario);
		Optional<List<TarjetaInpamex>> result = Optional.empty();
		try {
			result = tarjetaInpamexDao.findTarjetasUsuario(idUsuario);

			logger.info("END ENCONTRAR TARJETAS ASIGNADAS A USUARIO TIENE TARJETAS ASIGNADAS PREVIAMENTE - "
					+ result.isPresent());
		} catch (Exception e) {
			logger.error("ERROR ENCONTRAR TARJETAS ASIGNADAS A USUARIO - " + e.getLocalizedMessage());
		}
		return result;
	}
	
	public Integer deshabilitarTarjetasUsuario(Optional<List<TarjetaInpamex>> listaTarjetas) {
		Integer numeroRegistros = 0;
		logger.info("STARTING DESHABILITAR TARJETAS A USUARIO");
		try {
			for (TarjetaInpamex tarjetaInpamex : listaTarjetas.get()) {
				numeroRegistros = numeroRegistros + 1;

				tarjetaInpamex.setEstatus(tarjetaEstatusCodigoInactiva.charAt(0));
				tarjetaInpamex.setEstatusDescripcion(tarjetaEstatusDescInactiva);
				tarjetaInpamexDao.save(tarjetaInpamex);

				logger.info("TARJETA DESHABILITADA IDTARJETA - " + tarjetaInpamex.getIdTarjeta());
			}
			logger.info("END DESHABILITAR TARJETAS A USUARIO NUMERO DE TARJETAS DESHABILITADAS - " + numeroRegistros);
		} catch (Exception e) {
			logger.error("ERROR DESHABILITAR TARJETAS A USUARIO - " + e.getLocalizedMessage());
		}

		return numeroRegistros;
	}
	
	public Optional<TarjetaInpamex> asignarTarjeta(Optional<TarjetaInpamex> optTarjeta, Long idUsuario) {
		logger.info("STARTING ASIGNAR TARJETA IDUSUARIO - " + idUsuario + " IDTARJETA - "
				+ optTarjeta.get().getIdTarjeta());
		Optional<TarjetaInpamex> result = Optional.empty();
		try {

			TarjetaInpamex tarjetaInpamex = optTarjeta.get();
			tarjetaInpamex.setIdUsuario(idUsuario);
			tarjetaInpamex.setFechaModificacion(new Date());

			TarjetaInpamex tarjetaInpamexResult = tarjetaInpamexDao.save(tarjetaInpamex);
			result = Optional.of(tarjetaInpamexResult);

			logger.info(
					"END ASIGNAR TARJETA IDUSUARIO - " + idUsuario + " IDTARJETA - " + optTarjeta.get().getIdTarjeta());
		} catch (Exception e) {
			logger.error("ERROR AL ASIGNAR TARJETA - " + e.getLocalizedMessage());
		}
		return result;
	}
	
	public Double acumularMontoTransferencias(Optional<List<TransferenciasInpamex>> resultTransferencias,
			Optional<TarjetaInpamex> resultTarjetaAsignada) {

		logger.info("STARTING ACUMULAR MONTO TRANSFERENCIAS DISPONIBLES NUMERO TRANSFERENCIAS - "
				+ resultTransferencias.get().size());
		
		Double result = 0.0;
		Integer numeroRegistros = 0;

		try {

			if (resultTransferencias.isPresent() & resultTarjetaAsignada.isPresent()) {
				for (TransferenciasInpamex transferencia : resultTransferencias.get()) {
					logger.info("CALCULANDO ACUMULADO DE MONTO DE TRANSFERENCIA IDTRANSFERENCIA - "
							+ transferencia.getIdTransferencia() + " IDMTO - " + transferencia.getIdMto() + " MONTO - "
							+ transferencia.getMontoFinal());
					result = result + transferencia.getMontoFinal();

					logger.info("ACTUALIZANDO ESTATUS A ABONO APLICADO DE TRANSFERENCIA IDTRANSFERENCIA - "
							+ transferencia.getIdTransferencia() + " IDMTO - " + transferencia.getIdMto() + " MONTO - "
							+ transferencia.getMontoFinal());
					transferencia.setEstatus(transCodigoEstatusAbonada.charAt(0));
					transferencia.setEstatusDescripcion(transDescEstatusAbonada);
					transferencia.setComentario("ABONO POR ASIGNACION DE TARJETA REALIZADO CORRECTAMENTE POR " + transferencia.getMontoFinal());
					transferenciasInpamexDao.save(transferencia);

					logger.info("GUARDANDO TRANSACCION IDTRANSFERENCIA - " + transferencia.getIdTransferencia()
							+ " IDMTO - " + transferencia.getIdMto() + " MONTO - " + transferencia.getMontoFinal());
					Utils utils = new Utils();
					String noAutorizacion = prefijoTransAbono;
					noAutorizacion = prefijoTransAbono.concat(utils.dateTimeMilliSecond());

					TransaccionesInpamex transaccionesInpamex = new TransaccionesInpamex();

					transaccionesInpamex.setIdUsuario(transferencia.getIdUsuario());
					transaccionesInpamex.setIdTransferencia(transferencia.getIdTransferencia());
					transaccionesInpamex.setIdTarjeta(resultTarjetaAsignada.get().getIdTarjeta());
					transaccionesInpamex.setIdMto(transferencia.getIdMto());

					transaccionesInpamex.setTipo("ABONO");
					transaccionesInpamex.setMonto(transferencia.getMontoFinal());
					transaccionesInpamex.setConcepto("ABONO DESDE ASIGNACION DE TARJETA");
					transaccionesInpamex.setFecha(new Date());
					transaccionesInpamex.setEstatus(transaccionCodigoAbono.charAt(0));
					transaccionesInpamex.setEstatusDescripcion(transaccionDescAbono);
					transaccionesInpamex.setNumeroAutorizacion(noAutorizacion);
					transaccionesInpamex.setClaveCadena("");
					transaccionesInpamex.setComentario("ABONO DESDE ASIGNACION DE TARJETA APLICADO EXITOSAMENTE POR "
							+ transferencia.getMontoFinal());

					transaccionesInpamexDao.save(transaccionesInpamex);

					numeroRegistros = numeroRegistros + 1;
				}
			}

			if (numeroRegistros > 0 & result > 0) {
				logger.info("ACTUALIZANDO SALDO DE TARJETA IDTARJETA - " + resultTarjetaAsignada.get().getIdTarjeta()
						+ " SALDO ANTERIOR - " + resultTarjetaAsignada.get().getSaldo() + " MONTO A ABONAR - "
						+ result);

				Double saldoFinal = resultTarjetaAsignada.get().getSaldo() + result;
				
				resultTarjetaAsignada.get().setSaldo(saldoFinal);
			}

			logger.info("END ACUMULAR MONTO TRANSFERENCIAS DISPONIBLES NUMERO DE TRANSFERENCIAS ABONADAS - "
					+ numeroRegistros + " MONTO TOTAL ABONADO - " + result);
		} catch (Exception e) {
			logger.error("ERROR ACUMULAR MONTO TRANSFERENCIAS DISPONIBLES - " + e.getLocalizedMessage());
		}
		return result;
	}

}
