package com.addcel.inpamex.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.addcel.inpamex.InpamexApplication;
import com.addcel.inpamex.data.dao.TarjetaInpamexDao;
import com.addcel.inpamex.data.dao.TransaccionesInpamexDao;
import com.addcel.inpamex.data.dao.TransferenciasInpamexDao;
import com.addcel.inpamex.data.dao.TransferenciasInpamexTempDao;
import com.addcel.inpamex.data.dao.UsuariosInpamexDao;
import com.addcel.inpamex.data.entity.TarjetaInpamex;
import com.addcel.inpamex.data.entity.TransaccionesInpamex;
import com.addcel.inpamex.data.entity.TransferenciasInpamex;
import com.addcel.inpamex.data.entity.TransferenciasInpamexTemp;
import com.addcel.inpamex.data.entity.UsuariosInpamex;
import com.addcel.inpamex.payload.request.Transferencia;
import com.addcel.inpamex.payload.request.TransferenciaAbonar;
import com.addcel.inpamex.payload.response.TransferenciaIndividualResponse;
import com.addcel.inpamex.payload.response.TransferenciaResponse;
import com.addcel.inpamex.service.ws.inpamex.InpamexWsService;
import com.addcel.inpamex.utils.Utils;

@Service
@Transactional
public class TransferenciaServiceImpl implements TransferenciaService {

	private final static Logger logger = Logger.getLogger(InpamexApplication.class);

	@Value("${trans.temp.estatus.codigo.recibida}")
	private String codigoTransferenciaTempRecibida;
	
	@Value("${trans.temp.estatus.codigo.procesada}")
	private String codigoTransferenciaTempProcesada;
	
	@Value("${error.codigo.idmto.existente}")
	private String errorCodigoIdmto;

	@Value("${error.mensaje.idmto.existente}")
	private String errorMensajeIdmto;

	@Value("${usuario.codigo.nuevo}")
	private String usuarioNuevo;

	@Value("${usuario.codigo.recurrente}")
	private String usuarioRecurrente;

	@Value("${error.codigo.usuario}")
	private String errorCodigoUsuario;

	@Value("${error.mensaje.usuario}")
	private String errorMensajeUsuario;

	@Value("${usuario.estatus.codigo.activo}")
	private String usuarioCodigoEstatusActivo;

	@Value("${error.codigo.usuario.inactivo}")
	private String errorCodigoUsuarioInactivo;

	@Value("${error.mensaje.usuario.inactivo}")
	private String errorMensajeUsuarioInactivo;

	@Value("${tarjeta.estatus.codigo.activa}")
	private String tarjetaEstatusCodigoActiva;

	@Value("${error.codigo.usuario.recurrente.tarjeta}")
	private String errorCodigoUsuarioRecurrenteTarjeta;

	@Value("${error.codigo.mensaje.recurrente.tarjeta}")
	private String errorMensajeUsuarioRecurrenteTarjeta;

	@Value("${error.codigo.tarjeta.inactiva}")
	private String errorCodigoTarjetaInactiva;

	@Value("${error.mensaje.tarjeta.inactiva}")
	private String errorMensajeTarjetaInactiva;

	@Value("${transfer.prefijo}")
	private String transferPrefijo;

	@Value("${transaccion.tipo.prefix.abono}")
	private String transaccionPrefijoAbono;

	@Value("${trans.estatus.codigo.abonada}")
	private String transCodigoEstatusAbonada;

	@Value("${trans.estatus.desc.abonada}")
	private String transDescEstatusAbonada;

	@Value("${trans.estatus.codigo.disponible}")
	private String transCodigoEstatusDisponible;

	@Value("${trans.estatus.desc.disponible}")
	private String transDescEstatusDisponible;

	@Value("${error.codigo.transferencia}")
	private String errorCodigoTransferencia;

	@Value("${error.mensaje.transferencia}")
	private String errorMensajeTransferencia;

	@Value("${error.codigo.transferencia.disponible}")
	private String errorCodigoTransferenciaDisponible;

	@Value("${error.mensaje.transferencia.disponible}")
	private String errorMensajeTransferenciaDisponible;

	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;

	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;

	@Value("${exito.codigo}")
	private String exitoCodigo;

	@Value("${exito.mensaje}")
	private String exitoMensaje;

	@Autowired
	private TransferenciasInpamexTempDao transferenciasInpamexTempDao;

	@Autowired
	private TransferenciasInpamexDao transferenciasInpamexDao;

	@Autowired
	private UsuariosInpamexDao usuariosInpamexDao;

	@Autowired
	private TarjetaInpamexDao tarjetaInpamexDao;

	@Autowired
	private TransaccionesInpamexDao transaccionesInpamexDao;

	@Autowired
	private InpamexWsService inpamexWsService;

	@Override
	@Transactional
	public ResponseEntity<TransferenciaResponse> doTransferencia(Transferencia transferencia) {

		logger.info("####################################################");
		logger.info("STARTING NUEVA TRANSFERENCIA - " + transferencia.toString());

		String tipoUsuario = "";
		String comentarioTransferencia = "";

		ResponseEntity<TransferenciaResponse> responseEntity = null;

		TransferenciaResponse transferenciaResponse = null;
		TransferenciasInpamexTemp transferenciasInpamexTemp = null;

		Optional<TransferenciasInpamexTemp> resultTransferenciasInpamexTempOptional = Optional.empty();
		TransferenciasInpamexTemp resultTransferenciasInpamexTemp = null;
		Optional<TransferenciasInpamex> resultTransferenciaIdMto = Optional.empty();
		Optional<UsuariosInpamex> resultUsuario = Optional.empty();
		Optional<TarjetaInpamex> resultTarjeta = Optional.empty();
		Optional<TarjetaInpamex> resultTarjetaAbono = Optional.empty();
		Optional<TransferenciasInpamex> resultTransferencia = Optional.empty();
		Optional<TransaccionesInpamex> resultTransaccion = Optional.empty();

		try {
			
//			Delay Para validar la existencia de IDMto en la base
			TimeUnit.SECONDS.sleep(2);

//			Validando existencia de Transferencia en Temporal
//			resultTransferenciasInpamexTempOptional = transferenciasInpamexTempDao.findTransferenciaIdMto(transferencia.getIdMto().toUpperCase().trim(), codigoTransferenciaTempProcesada.charAt(0));
//			
//			if (resultTransferenciasInpamexTempOptional.isPresent()) {
//				logger.error(
//						"ERROR AL REALIZAR LA TRANSFERENCIA IDMTO EXISTENTE EN LA BD - " + transferencia.getIdMto());
//				transferenciaResponse = new TransferenciaResponse();
//				transferenciaResponse.setIdError(Integer.parseInt(errorCodigoIdmto));
//				transferenciaResponse.setMensajeError(errorMensajeIdmto);
//				return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
//			}
			
//			Guardando Transferencia en Temporal
//			transferenciasInpamexTemp = new TransferenciasInpamexTemp();
//			transferenciasInpamexTemp.setIdMto(transferencia.getIdMto().toUpperCase().trim());
//			transferenciasInpamexTemp.setIdUsuario(transferencia.getBeneficiario().getIdUsuario());
//			transferenciasInpamexTemp.setEstatus(codigoTransferenciaTempRecibida.charAt(0));
//			resultTransferenciasInpamexTemp = transferenciasInpamexTempDao.save(transferenciasInpamexTemp);
			
//			Validando Existencia de IdMto
			resultTransferenciaIdMto = transferenciasInpamexDao
					.findTransferenciaIdMto(transferencia.getIdMto().toUpperCase().trim());

			if (resultTransferenciaIdMto.isPresent()) {
				logger.error(
						"ERROR AL REALIZAR LA TRANSFERENCIA IDMTO EXISTENTE EN LA BD - " + transferencia.getIdMto());
				transferenciaResponse = new TransferenciaResponse();
				transferenciaResponse.setIdError(Integer.parseInt(errorCodigoIdmto));
				transferenciaResponse.setMensajeError(errorMensajeIdmto);
				return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
			}

//			Validando tipo de usuario Nuevo o Recurrente
			tipoUsuario = this.validarTipoUsuario(transferencia);

			switch (tipoUsuario) {
			case "N":

				comentarioTransferencia = "EL USUARIO NO TIENE UNA TARJETA ASIGNADA, LA TRANSFERENCIA PERMANECERA COMO DISPONIBLE";
				resultTransferencia = this.saveTransferencia(transferencia, 0L, transCodigoEstatusDisponible.charAt(0),
						transDescEstatusDisponible, comentarioTransferencia, 0L);

				if (resultTransferencia.isPresent()) {

//					Armando respuesta
					transferenciaResponse = new TransferenciaResponse();

					transferenciaResponse.setIdTransferencia(resultTransferencia.get().getIdTransferencia());
					transferenciaResponse.setIdUsuario(0L);
					transferenciaResponse.setIdMto(resultTransferencia.get().getIdMto());
					transferenciaResponse.setIdTarjeta(0L);
					transferenciaResponse.setNoAutorizacionTransaccion("0");
					transferenciaResponse.setEstatus(resultTransferencia.get().getEstatus());
					transferenciaResponse.setEstatusDescripcion(resultTransferencia.get().getEstatusDescripcion());
					transferenciaResponse.setIdTransaccion(0L);
					transferenciaResponse
							.setNoAutorizacionTransferencia(resultTransferencia.get().getNumeroAutorizacion());
					transferenciaResponse.setIdError(Integer.parseInt(exitoCodigo));
					transferenciaResponse.setMensajeError(exitoMensaje);
					
//					Actualizando estatus de Transferencia en Temporal
//					transferenciasInpamexTemp = new TransferenciasInpamexTemp();
//					transferenciasInpamexTemp.setIdTransferencia(resultTransferenciasInpamexTemp.getIdTransferencia());
//					transferenciasInpamexTemp.setIdMto(resultTransferenciasInpamexTemp.getIdMto());
//					transferenciasInpamexTemp.setIdUsuario(resultTransferenciasInpamexTemp.getIdUsuario());
//					transferenciasInpamexTemp.setEstatus(codigoTransferenciaTempProcesada.charAt(0));
//					transferenciasInpamexTempDao.save(transferenciasInpamexTemp);

					logger.info("PROCESO FINALIZADO EXITOSAMENTE NUEVA TRANSFERENCIA IDTRANSFERENCIA - "
							+ transferenciaResponse.getIdTransferencia() + " IDTRANSACCION - "
							+ transferenciaResponse.getIdTransaccion());
					return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.CREATED);

				}

				break;

			case "R":

//				Validando Existencia de Usuario
				logger.info("STARTING ENCONTRAR USUARIO RECURRENTE EN BD - "
						+ transferencia.getBeneficiario().getIdUsuario());
				resultUsuario = usuariosInpamexDao.findById(transferencia.getBeneficiario().getIdUsuario());
				logger.info("END ENCONTRAR USUARIO RECURRENTE EN BD - " + resultUsuario.isPresent());

				if (!resultUsuario.isPresent()) {
					logger.error("ERROR AL REALIZAR LA TRANSFERENCIA USUARIO RECURRENTE NO ENCONTRADO EN LA BD - "
							+ transferencia.getBeneficiario());
					transferenciaResponse = new TransferenciaResponse();
					transferenciaResponse.setIdError(Integer.parseInt(errorCodigoUsuario));
					transferenciaResponse.setMensajeError(errorMensajeUsuario);
					return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
				} else {
//						Validando Estatus de Usuario
					logger.info("STARTING VALIDAR ESTATUS DE USUARIO RECURRENTE EN BD - "
							+ resultUsuario.get().getEstatusDescripcion());
					if (!usuarioCodigoEstatusActivo.equals(resultUsuario.get().getEstatus().toString())) {
						logger.error("ERROR AL REALIZAR LA TRANSFERENCIA USUARIO RECURRENTE INACTIVO - "
								+ transferencia.getBeneficiario());
						transferenciaResponse = new TransferenciaResponse();
						transferenciaResponse.setIdError(Integer.parseInt(errorCodigoUsuarioInactivo));
						transferenciaResponse.setMensajeError(errorMensajeUsuarioInactivo);
						return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
					}
				}

//				Validando Existencia de Tarjeta para Usuario
				logger.info("STARTING ENCONTRAR TARJETA DE USUARIO RECURRENTE EN BD - "
						+ transferencia.getBeneficiario().getIdUsuario());
				resultTarjeta = tarjetaInpamexDao.findTarjetaByUsuario(transferencia.getBeneficiario().getIdUsuario());
				logger.info("END ENCONTRAR TARJETA DE USUARIO RECURRENTE EN BD - " + resultTarjeta.isPresent());

				if (!resultTarjeta.isPresent()) {
					logger.error("ERROR AL REALIZAR LA TRANSFERENCIA USUARIO RECURRENTE NO TIENE TARJETA ASIGNADA - "
							+ transferencia.getBeneficiario());
					transferenciaResponse = new TransferenciaResponse();
					transferenciaResponse.setIdError(Integer.parseInt(errorCodigoUsuarioRecurrenteTarjeta));
					transferenciaResponse.setMensajeError(errorMensajeUsuarioRecurrenteTarjeta);
					return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
				} else {
//					Valindando Estatus de Tarjeta
					logger.info("STARTING VALIDAR ESTATUS DE TARJETA EN BD - "
							+ resultTarjeta.get().getEstatusDescripcion());
					if (!tarjetaEstatusCodigoActiva.equals(resultTarjeta.get().getEstatus().toString())) {
						logger.error("ERROR AL REALIZAR LA TRANSFERENCIA USUARIO RECURRENTE NO TIENE TARJETA ACTIVA - "
								+ transferencia.getBeneficiario());
						transferenciaResponse = new TransferenciaResponse();
						transferenciaResponse.setIdError(Integer.parseInt(errorCodigoTarjetaInactiva));
						transferenciaResponse.setMensajeError(errorMensajeTarjetaInactiva);
						return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
					}
				}
				
//				Actualizando estatus de Transferencia en Temporal
//				transferenciasInpamexTemp = new TransferenciasInpamexTemp();
//				transferenciasInpamexTemp.setIdTransferencia(resultTransferenciasInpamexTemp.getIdTransferencia());
//				transferenciasInpamexTemp.setIdMto(resultTransferenciasInpamexTemp.getIdMto());
//				transferenciasInpamexTemp.setIdUsuario(resultTransferenciasInpamexTemp.getIdUsuario());
//				transferenciasInpamexTemp.setEstatus(codigoTransferenciaTempProcesada.charAt(0));
//				transferenciasInpamexTempDao.save(transferenciasInpamexTemp);

//				Realizando Calculo para el Abono para Usuario Recurrente
				if (resultTarjeta.isPresent()
						& tarjetaEstatusCodigoActiva.equals(resultTarjeta.get().getEstatus().toString())) {
					resultTarjetaAbono = this.abonarTransferencia(transferencia.getMontoFinal(), resultTarjeta);
				}

//				Guardando Transferencia en BD
				if (resultTarjetaAbono.isPresent()) {
					comentarioTransferencia = "ABONO POR TRANSFERENCIA REALIZADO CORRECTAMENTE POR "
							+ transferencia.getMontoFinal();
					resultTransferencia = this.saveTransferencia(transferencia, resultUsuario.get().getIdUsuario(),
							transCodigoEstatusAbonada.charAt(0), transDescEstatusAbonada, comentarioTransferencia,
							resultTarjeta.get().getIdTarjeta());
				}

//				Guardando Transaccion en BD
				if (resultTransferencia.isPresent() & resultTarjetaAbono.isPresent()) {
					String comentarioTransaccion = "ABONO POR TRANSFERENCIA REALIZADO CORRECTAMENTE POR "
							+ transferencia.getMontoFinal();
					resultTransaccion = this.saveTransaccion(resultUsuario, resultTransferencia, resultTarjeta,
							transferencia, transCodigoEstatusAbonada.charAt(0), transDescEstatusAbonada,
							comentarioTransaccion);
				}

//				Llamando WS Inpamex para Transferencia Abonada
				if (resultTransferencia.isPresent() & resultTransaccion.isPresent()) {
					inpamexWsService.confirmTransfer(resultTransferencia.get().getIdTransferencia());

//					Armando respuesta
					transferenciaResponse = new TransferenciaResponse();

					transferenciaResponse.setIdTransferencia(resultTransferencia.get().getIdTransferencia());
					transferenciaResponse.setIdUsuario(resultUsuario.get().getIdUsuario());
					transferenciaResponse.setIdMto(transferencia.getIdMto().toUpperCase().trim());
					transferenciaResponse.setIdTarjeta(resultTarjeta.get().getIdTarjeta());
					transferenciaResponse.setNoAutorizacionTransaccion(resultTransaccion.get().getNumeroAutorizacion());
					transferenciaResponse.setEstatus(resultTransferencia.get().getEstatus());
					transferenciaResponse.setEstatusDescripcion(resultTransferencia.get().getEstatusDescripcion());
					transferenciaResponse.setIdTransaccion(resultTransaccion.get().getIdTransaccion());
					transferenciaResponse
							.setNoAutorizacionTransferencia(resultTransferencia.get().getNumeroAutorizacion());
					transferenciaResponse.setIdError(Integer.parseInt(exitoCodigo));
					transferenciaResponse.setMensajeError(exitoMensaje);

					logger.info("PROCESO FINALIZADO EXITOSAMENTE NUEVA TRANSFERENCIA IDTRANSFERENCIA - "
							+ transferenciaResponse.getIdTransferencia() + " IDTRANSACCION - "
							+ transferenciaResponse.getIdTransaccion());
					return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.CREATED);
				}

				break;

			}
			
		} catch (Exception e) {

			logger.error("ERROR AL REALIZAR LA TRANSFERENCIA DO TRANSFERENCIA - " + e.getMessage());
			transferenciaResponse = new TransferenciaResponse();

			transferenciaResponse.setNoAutorizacionTransferencia("0");
			transferenciaResponse.setNoAutorizacionTransaccion("0");
			transferenciaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			transferenciaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TransferenciaResponse>(transferenciaResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return responseEntity;
	}

	@Override
	public ResponseEntity<TransferenciaIndividualResponse> getTransferencia(String idMto) {
		logger.info("####################################################");
		logger.info("STARTING OBTENER TRANSFERENCIA IDMTO - " + idMto.toUpperCase().trim());

		ResponseEntity<TransferenciaIndividualResponse> responseEntity = null;
		Optional<TransferenciasInpamex> resultTransferencia = Optional.empty();

		TransferenciaIndividualResponse transferenciaResponse = null;

		try {

//			Validando existencia de Transferencia
			transferenciaResponse = new TransferenciaIndividualResponse();
			resultTransferencia = transferenciasInpamexDao.findTransferenciaIdMto(idMto.toUpperCase().trim());

			if (!resultTransferencia.isPresent()) {
				logger.error("ERROR AL REALIZAR LA TRANSACCION TRANSFERENCIA NO ENCONTRADA EN BD IDMTO - "
						+ idMto.toUpperCase().trim());
				transferenciaResponse = new TransferenciaIndividualResponse();
				transferenciaResponse.setIdError(Integer.parseInt(errorCodigoTransferencia));
				transferenciaResponse.setMensajeError(errorMensajeTransferencia);
				return new ResponseEntity<TransferenciaIndividualResponse>(transferenciaResponse,
						HttpStatus.OK);

			} else {
				transferenciaResponse = new TransferenciaIndividualResponse();
				transferenciaResponse.setIdTransferencia(resultTransferencia.get().getIdTransferencia());
				transferenciaResponse.setIdUsuario(resultTransferencia.get().getIdUsuario());
				transferenciaResponse.setIdMto(resultTransferencia.get().getIdMto());
				transferenciaResponse.setIdTarjeta(resultTransferencia.get().getIdTarjeta());
				transferenciaResponse.setNoAutorizacionTransferencia(resultTransferencia.get().getNumeroAutorizacion());
				transferenciaResponse.setMontoOriginal(resultTransferencia.get().getMontoOriginal());
				transferenciaResponse.setMontoFinal(resultTransferencia.get().getMontoFinal());
				transferenciaResponse.setTipoCambio(resultTransferencia.get().getTipoCambio());
				transferenciaResponse.setFecha(resultTransferencia.get().getFecha());
				transferenciaResponse.setRemitenteNombre(resultTransferencia.get().getRemitenteNombre());
				transferenciaResponse.setRemitenteApaterno(resultTransferencia.get().getRemitenteApaterno());
				transferenciaResponse.setRemitenteAmaterno(resultTransferencia.get().getRemitenteAmaterno());
				transferenciaResponse.setBeneficiarioNombre(resultTransferencia.get().getBeneficiarioNombre());
				transferenciaResponse.setBeneficiarioApaterno(resultTransferencia.get().getBeneficiarioApaterno());
				transferenciaResponse.setBeneficiarioAmaterno(resultTransferencia.get().getBeneficiarioAmaterno());
				transferenciaResponse.setComentario(resultTransferencia.get().getComentario());
				
				transferenciaResponse.setEstatus(resultTransferencia.get().getEstatus());
				transferenciaResponse.setEstatusDescripcion(resultTransferencia.get().getEstatusDescripcion());
				transferenciaResponse.setIdError(Integer.parseInt(exitoCodigo));
				transferenciaResponse.setMensajeError(exitoMensaje);

				logger.info("PROCESO FINALIZADO EXITOSAMENTE CONSULTA DE TRANSFERENCIA POR IDMTO - "
						+ idMto.toUpperCase().trim());
				return new ResponseEntity<TransferenciaIndividualResponse>(transferenciaResponse, HttpStatus.OK);
			}

		} catch (Exception e) {
			logger.error("ERROR AL OBTENER LA TRANSFERENCIA POR IDMTO - " + e.getMessage());
			transferenciaResponse = new TransferenciaIndividualResponse();

			transferenciaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			transferenciaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TransferenciaIndividualResponse>(transferenciaResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;
	}

	@Override
	public ResponseEntity<TransferenciaResponse> addTransferencia(TransferenciaAbonar transferencia) {
		logger.info("####################################################");
		logger.info("STARTING ABONAR TRANSFERENCIA - " + transferencia.toString());

		ResponseEntity<TransferenciaResponse> responseEntity = null;
		TransferenciaResponse transferenciaResponse = null;

		Optional<UsuariosInpamex> resultUsuario = Optional.empty();
		Optional<TransferenciasInpamex> resultTransferencia = Optional.empty();
		Optional<TransferenciasInpamex> resultTransferenciaAbonada = Optional.empty();
		Optional<TarjetaInpamex> resultTarjeta = Optional.empty();
		Optional<TarjetaInpamex> resultTarjetaAbono = Optional.empty();
		Optional<TransaccionesInpamex> resultTransaccion = Optional.empty();

		try {

//			Validando existencia de Transferencia
			resultTransferencia = transferenciasInpamexDao
					.findTransferenciaIdMto(transferencia.getIdMto().toUpperCase().trim());

			if (!resultTransferencia.isPresent()) {
				logger.error("ERROR AL REALIZAR LA TRANSACCION TRANSFERENCIA NO ENCONTRADA EN BD IDMTO - "
						+ transferencia.getIdMto().toUpperCase().trim());
				transferenciaResponse = new TransferenciaResponse();
				transferenciaResponse.setIdError(Integer.parseInt(errorCodigoTransferencia));
				transferenciaResponse.setMensajeError(errorMensajeTransferencia);
				return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
			} else {
//				Validando Estatus de Transferencia
				logger.info("STARTING VALIDAR ESTATUS DE TRANSFERENCIA EN BD - " + transferencia.getIdMto());
				if (!transCodigoEstatusDisponible.equals(resultTransferencia.get().getEstatus().toString())) {
					logger.error("ERROR AL REALIZAR LA TRANSFERENCIA ESTATUS DE TRANSFERENCIA NO DISPONIBLE - "
							+ transferencia.getIdMto());
					transferenciaResponse = new TransferenciaResponse();
					transferenciaResponse.setIdError(Integer.parseInt(errorCodigoTransferenciaDisponible));
					transferenciaResponse.setMensajeError(errorMensajeTransferenciaDisponible);
					return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
				}
			}

//			Validando Existencia de Usuario
			if (resultTransferencia.isPresent()) {
				logger.info("STARTING ENCONTRAR USUARIO EN BD - " + transferencia.getIdUsuario());
				resultUsuario = usuariosInpamexDao.findById(transferencia.getIdUsuario());
				logger.info("END ENCONTRAR USUARIO EN BD - " + resultUsuario.isPresent());

				if (!resultUsuario.isPresent()) {
					logger.error("ERROR AL ABONAR LA TRANSFERENCIA USUARIO NO ENCONTRADO EN LA BD - "
							+ transferencia.getIdUsuario());
					transferenciaResponse = new TransferenciaResponse();
					transferenciaResponse.setIdError(Integer.parseInt(errorCodigoUsuario));
					transferenciaResponse.setMensajeError(errorMensajeUsuario);
					return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
				} else {

//						Validando Estatus de Usuario
					logger.info("STARTING VALIDAR ESTATUS DE USUARIO EN BD - "
							+ resultUsuario.get().getEstatusDescripcion());
					if (!usuarioCodigoEstatusActivo.equals(resultUsuario.get().getEstatus().toString())) {
						logger.error("ERROR AL REALIZAR LA TRANSFERENCIA USUARIO INACTIVO - "
								+ transferencia.getIdUsuario());
						transferenciaResponse = new TransferenciaResponse();
						transferenciaResponse.setIdError(Integer.parseInt(errorCodigoUsuarioInactivo));
						transferenciaResponse.setMensajeError(errorMensajeUsuarioInactivo);
						return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
					}
				}
			}

			if (resultTransferencia.isPresent() & resultUsuario.isPresent()) {
//				Validando Existencia de Tarjeta para Usuario
				logger.info("STARTING ENCONTRAR TARJETA DE USUARIO EN BD - " + transferencia.getIdUsuario());
				resultTarjeta = tarjetaInpamexDao.findTarjetaByUsuario(transferencia.getIdUsuario());
				logger.info("END ENCONTRAR TARJETA DE USUARIO EN BD - " + resultTarjeta.isPresent());

				if (!resultTarjeta.isPresent()) {
					logger.error("ERROR AL ABONAR LA TRANSFERENCIA USUARIO NO TIENE TARJETA ASIGNADA - "
							+ transferencia.getIdUsuario());
					transferenciaResponse = new TransferenciaResponse();
					transferenciaResponse.setIdError(Integer.parseInt(errorCodigoUsuarioRecurrenteTarjeta));
					transferenciaResponse.setMensajeError(errorMensajeUsuarioRecurrenteTarjeta);
					return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
				} else {
//					Valindando Estatus de Tarjeta
					logger.info("STARTING VALIDAR ESTATUS DE TARJETA EN BD - "
							+ resultTarjeta.get().getEstatusDescripcion());
					if (!tarjetaEstatusCodigoActiva.equals(resultTarjeta.get().getEstatus().toString())) {
						logger.error("ERROR AL ABONAR LA TRANSFERENCIA USUARIO RECURRENTE NO TIENE TARJETA ACTIVA - "
								+ transferencia.getIdUsuario());
						transferenciaResponse = new TransferenciaResponse();
						transferenciaResponse.setIdError(Integer.parseInt(errorCodigoTarjetaInactiva));
						transferenciaResponse.setMensajeError(errorMensajeTarjetaInactiva);
						return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
					}
				}

//				Realizando Calculo para el Abono para Usuario Recurrente
				if (resultTransferencia.isPresent() & resultUsuario.isPresent() & resultTarjeta.isPresent()) {
					resultTarjetaAbono = this.abonarTransferencia(resultTransferencia.get().getMontoFinal(),
							resultTarjeta);
				}

//				Actualizando Transferencia
				if (resultTarjetaAbono.isPresent()) {
					String comentarioTransferencia = "ABONO REALIZADO CORRECTAMENTE POR "
							+ resultTransferencia.get().getMontoFinal();
					resultTransferenciaAbonada = this.updateTransferencia(resultTransferencia, resultUsuario,
							resultTarjetaAbono, transCodigoEstatusAbonada.charAt(0), transDescEstatusAbonada,
							comentarioTransferencia);
				}

//				Guardando Transaccion
				if (resultTransferenciaAbonada.isPresent()) {
					String comentarioTransaccion = "ABONO REALIZADO CORRECTAMENTE POR "
							+ resultTransferencia.get().getMontoFinal();
					Transferencia transferenciaAux = new Transferencia();

					transferenciaAux.setIdMto(resultTransferencia.get().getIdMto());
					transferenciaAux.setMontoFinal(resultTransferencia.get().getMontoFinal());

					resultTransaccion = this.saveTransaccion(resultUsuario, resultTransferencia, resultTarjeta,
							transferenciaAux, transCodigoEstatusAbonada.charAt(0), transDescEstatusAbonada,
							comentarioTransaccion);
				}

//				Llamando WS Inpamex para Transferencia Abonada
				if (resultTransferencia.isPresent() & resultTransaccion.isPresent()) {
					inpamexWsService.confirmTransfer(resultTransferencia.get().getIdTransferencia());

//					Armando respuesta
					transferenciaResponse = new TransferenciaResponse();
					transferenciaResponse.setIdTransferencia(resultTransferenciaAbonada.get().getIdTransferencia());
					transferenciaResponse.setIdUsuario(resultTransferenciaAbonada.get().getIdUsuario());
					transferenciaResponse.setIdTarjeta(resultTransferenciaAbonada.get().getIdTarjeta());
					transferenciaResponse.setIdTransaccion(resultTransaccion.get().getIdTransaccion());
					transferenciaResponse.setIdMto(resultTransferenciaAbonada.get().getIdMto());
					transferenciaResponse.setNoAutorizacionTransaccion(resultTransaccion.get().getNumeroAutorizacion());
					transferenciaResponse
							.setNoAutorizacionTransferencia(resultTransferenciaAbonada.get().getNumeroAutorizacion());
					transferenciaResponse.setEstatus(resultTransferenciaAbonada.get().getEstatus());
					transferenciaResponse
							.setEstatusDescripcion(resultTransferenciaAbonada.get().getEstatusDescripcion());

					transferenciaResponse.setIdError(Integer.parseInt(exitoCodigo));
					transferenciaResponse.setMensajeError(exitoMensaje);

					logger.info("PROCESO FINALIZADO EXITOSAMENTE ABONO DE TRANSFERENCIA ID - "
							+ transferenciaResponse.getIdTransferencia());
					return new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.CREATED);

				}
			}

		} catch (Exception e) {
			logger.error("ERROR AL ABONAR LA TRANSFERENCIA ADD TRANSFERENCIA - " + e.getMessage());
			transferenciaResponse = new TransferenciaResponse();

			transferenciaResponse.setNoAutorizacionTransferencia("0");
			transferenciaResponse.setNoAutorizacionTransaccion("0");
			transferenciaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			transferenciaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TransferenciaResponse>(transferenciaResponse,
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return responseEntity;
	}

	public String validarTipoUsuario(Transferencia transferencia) {
		logger.info("STARTING VALIDAR TIPO DE USUARIO IDUSUARIO - " + transferencia.getBeneficiario().getIdUsuario());
		String tipoUsuario = "";
		try {
			if (transferencia.getBeneficiario().getIdUsuario() == 0) {
				logger.info("SE DETECTO USUARIO NUEVO");
				tipoUsuario = usuarioNuevo;
			} else {
				logger.info("SE DETECTO USUARIO RECURRENTE");
				tipoUsuario = usuarioRecurrente;
			}
			logger.info("END VALIDAR TIPO DE USUARIO");
		} catch (Exception e) {
			logger.error("ERROR VALIDAR TIPO DE USUARIO - " + e.getLocalizedMessage());
		}
		return tipoUsuario;
	}

	private Optional<TarjetaInpamex> abonarTransferencia(Double monto, Optional<TarjetaInpamex> resultTarjeta) {

		logger.info("STARTING ABONAR TRANSFERENCIA A TARJETA");
		Optional<TarjetaInpamex> result = Optional.empty();

		try {

			double saldoAnterior = resultTarjeta.get().getSaldo();
			double saldoFinal = saldoAnterior + monto;

			TarjetaInpamex tarjetaInpamex = resultTarjeta.get();
			tarjetaInpamex.setSaldo(saldoFinal);

			TarjetaInpamex tarjetaInpamexResult = tarjetaInpamexDao.save(tarjetaInpamex);

			result = Optional.of(tarjetaInpamexResult);
			logger.info("END ABONAR TRANSFERENCIA A TARJETA - " + tarjetaInpamexResult.getIdTarjeta() + " MONTO - "
					+ monto);
		} catch (Exception e) {
			logger.error("ERROR AL REALIZAR EL ABONO A TARJETA - " + e.getLocalizedMessage());
		}

		return result;
	}

	private Optional<TransferenciasInpamex> saveTransferencia(Transferencia transferencia, Long idUsuario,
			char estatusCodigoTransferencia, String estatusDescTransferencia, String comentarioTransferencia,
			Long idTarjeta) {

		logger.info("STARTING GUARDAR TRANSFERENCIA EN BD");
		Optional<TransferenciasInpamex> result = Optional.empty();

		try {
			Utils utils = new Utils();
			String noAutorizacion = transferPrefijo.concat(utils.dateTimeMilliSecond());

			TransferenciasInpamex transferenciasInpamex = new TransferenciasInpamex();

			transferenciasInpamex.setIdMto(transferencia.getIdMto().toUpperCase().trim());
			transferenciasInpamex.setIdTarjeta(idTarjeta);
			transferenciasInpamex.setIdUsuario(idUsuario);
			transferenciasInpamex.setNumeroAutorizacion(noAutorizacion);
			transferenciasInpamex.setMontoOriginal(transferencia.getMontoOriginal());
			transferenciasInpamex.setMontoFinal(transferencia.getMontoFinal());
			transferenciasInpamex.setTipoCambio(transferencia.getTipoCambio());

			DateFormat sourceFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String dateAsString = transferencia.getFecha();
			Date fecha = sourceFormat.parse(dateAsString);
			transferenciasInpamex.setFecha(fecha);

			transferenciasInpamex.setEstatus(estatusCodigoTransferencia);
			transferenciasInpamex.setEstatusDescripcion(estatusDescTransferencia);

			transferenciasInpamex.setRemitenteNombre(
					utils.removerAcentos(transferencia.getRemitente().getNombre().toUpperCase().trim()));
			transferenciasInpamex.setRemitenteApaterno(
					utils.removerAcentos(transferencia.getRemitente().getApaterno().toUpperCase().trim()));

			if (transferencia.getRemitente().getAmaterno() != null
					& !"".equals(transferencia.getRemitente().getAmaterno())) {
				transferenciasInpamex.setRemitenteAmaterno(
						utils.removerAcentos(transferencia.getRemitente().getAmaterno().toUpperCase().trim()));
			} else {
				transferenciasInpamex.setRemitenteAmaterno("");
			}

			transferenciasInpamex.setBeneficiarioNombre(
					utils.removerAcentos(transferencia.getBeneficiario().getNombre().toUpperCase().trim()));
			transferenciasInpamex.setBeneficiarioApaterno(
					utils.removerAcentos(transferencia.getBeneficiario().getApaterno().toUpperCase().trim()));

			if (transferencia.getBeneficiario().getAmaterno() != null
					& !"".equals(transferencia.getBeneficiario().getAmaterno())) {
				transferenciasInpamex.setBeneficiarioAmaterno(
						utils.removerAcentos(transferencia.getBeneficiario().getAmaterno().toUpperCase().trim()));
			} else {
				transferenciasInpamex.setBeneficiarioAmaterno("");
			}

			transferenciasInpamex.setComentario(comentarioTransferencia);
			transferenciasInpamex.setInpamexEstatus("");
			transferenciasInpamex.setInpamexEstatusDesc("");
			transferenciasInpamex.setInpamexRequest("");
			transferenciasInpamex.setInpamexResponse("");

			TransferenciasInpamex transferenciasInpamexResult = transferenciasInpamexDao.save(transferenciasInpamex);
			result = Optional.of(transferenciasInpamexResult);
			logger.info("END GUARDAR TRANSFERENCIA EN BD - " + transferenciasInpamexResult.getIdTransferencia());
		} catch (Exception e) {
			logger.error("ERROR AL GUARDAR TRANSFERENCIA EN BD - " + e.getLocalizedMessage());
		}

		return result;
	}

	private Optional<TransaccionesInpamex> saveTransaccion(Optional<UsuariosInpamex> resultUsuario,
			Optional<TransferenciasInpamex> resultTransferencia, Optional<TarjetaInpamex> resultTarjeta,
			Transferencia transferencia, char estatusCodigoTransferencia, String estatusDescTransferencia,
			String comentarioTransaccion) {

		logger.info("STARTING GUARDAR TRANSACCION DE ABONO EN BD ID TARJETA - " + resultTarjeta.get().getIdTarjeta()
				+ " ID TRANSFERENCIA - " + resultTransferencia.get().getIdTransferencia());
		Optional<TransaccionesInpamex> result = Optional.empty();

		try {

			Utils utils = new Utils();
			String noAutorizacion = transaccionPrefijoAbono.concat(utils.dateTimeMilliSecond());

			TransaccionesInpamex transaccionesInpamex = new TransaccionesInpamex();

			transaccionesInpamex.setIdUsuario(resultUsuario.get().getIdUsuario());
			transaccionesInpamex.setIdTransferencia(resultTransferencia.get().getIdTransferencia());
			transaccionesInpamex.setIdTarjeta(resultTarjeta.get().getIdTarjeta());
			transaccionesInpamex.setIdMto(transferencia.getIdMto().toUpperCase().trim());

			transaccionesInpamex.setTipo("ABONO");
			transaccionesInpamex.setMonto(transferencia.getMontoFinal());
			transaccionesInpamex.setConcepto("ABONO A TARJETA");
			transaccionesInpamex.setFecha(new Date());
			transaccionesInpamex.setEstatus(estatusCodigoTransferencia);
			transaccionesInpamex.setEstatusDescripcion(estatusDescTransferencia);
			transaccionesInpamex.setNumeroAutorizacion(noAutorizacion);
			transaccionesInpamex.setComentario(comentarioTransaccion);

			TransaccionesInpamex transaccionesInpamexResult = transaccionesInpamexDao.save(transaccionesInpamex);

			result = Optional.of(transaccionesInpamexResult);
			logger.info("END GUARDAR TRANSACCION EN BD - " + transaccionesInpamex.getIdTransaccion());
		} catch (Exception e) {
			logger.error("ERROR AL GUARDAR LA TRANSACCION - " + e.getLocalizedMessage());
		}
		return result;
	}

	public Optional<TransferenciasInpamex> updateTransferencia(Optional<TransferenciasInpamex> resultTransferencia,
			Optional<UsuariosInpamex> resultUsuario, Optional<TarjetaInpamex> resultTarjeta,
			char estatusCodigoTransferencia, String estatusDescTransferencia, String comentario) {

		logger.info("STARTING ACTUALIZAR TRANSFERENCIA");
		TransferenciasInpamex transferenciasInpamex = null;
		TransferenciasInpamex transferenciasInpamexResult = null;
		Optional<TransferenciasInpamex> result = Optional.empty();

		try {

			transferenciasInpamex = resultTransferencia.get();

			transferenciasInpamex.setIdUsuario(resultUsuario.get().getIdUsuario());
			transferenciasInpamex.setIdTarjeta(resultTarjeta.get().getIdTarjeta());
			transferenciasInpamex.setEstatus(estatusCodigoTransferencia);
			transferenciasInpamex.setEstatusDescripcion(estatusDescTransferencia);
			transferenciasInpamex.setComentario(comentario);

			transferenciasInpamexResult = transferenciasInpamexDao.save(transferenciasInpamex);

			result = Optional.of(transferenciasInpamexResult);

			logger.info("END ACTUALIZAR TRANSFERENCIA ID - " + result.get().getIdTransferencia());
		} catch (Exception e) {
			logger.error("ERROR AL ACTUALIZAR LA TRANSFERENCIA - " + e.getLocalizedMessage());
		}

		return result;

	}

}
