package com.addcel.inpamex.service;

import org.springframework.http.ResponseEntity;

import com.addcel.inpamex.payload.request.Usuario;
import com.addcel.inpamex.payload.request.UsuarioUpdate;
import com.addcel.inpamex.payload.response.UsuarioEntityResponse;
import com.addcel.inpamex.payload.response.UsuarioResponse;
import com.addcel.inpamex.payload.response.UsuariosEntityResponse;

public interface UsuarioService {
	
	public ResponseEntity<UsuarioResponse> saveUsuario(Usuario usuario);
	public ResponseEntity<UsuarioResponse> updateUsuario(UsuarioUpdate usuario);
	public ResponseEntity<UsuarioResponse> disableUsuario(Long idUsuario);
	public ResponseEntity<UsuarioResponse> enableUsuario(Long idUsuario);
	public ResponseEntity<UsuarioEntityResponse> getUsuario(Long idUsuario);
	public ResponseEntity<UsuariosEntityResponse> getUsuarios();
	public ResponseEntity<UsuarioEntityResponse> getUsuario(String curp);

}
