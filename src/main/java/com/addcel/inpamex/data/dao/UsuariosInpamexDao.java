package com.addcel.inpamex.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.addcel.inpamex.data.entity.UsuariosInpamex;

@Repository
public interface UsuariosInpamexDao extends JpaRepository<UsuariosInpamex, Long> {

	@Query("SELECT c FROM UsuariosInpamex c WHERE c.idUsuario = :idUsuario")
	public Optional<UsuariosInpamex> findUsuarioId(@Param("idUsuario") Long idUsuario);
	
	@Query("SELECT c FROM UsuariosInpamex c")
	public List<UsuariosInpamex> findUsuarioAll();
	
	@Query("SELECT c FROM UsuariosInpamex c WHERE c.noDocumento = :noDocumento")
	public Optional<UsuariosInpamex> findUsuarioDocumento(@Param("noDocumento") String noDocumento);

	@Query("SELECT c FROM UsuariosInpamex c WHERE c.idUsuario = :idUsuario AND c.estatus = :estatus")
	public Optional<UsuariosInpamex> findUsuarioActivo(@Param("idUsuario") Long idUsuario,
			@Param("estatus") Character estatus);
	
	@Query("SELECT c FROM UsuariosInpamex c WHERE c.curp = :curp")
	public Optional<UsuariosInpamex> findUsuarioByCURP(@Param("curp") String curp);


}
