package com.addcel.inpamex.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.addcel.inpamex.data.entity.CatNacionalidadInpamex;

@Repository
public interface CatNacionalidadInpamexDao extends JpaRepository<CatNacionalidadInpamex, Long> {
	
	@Query(value = "SELECT c FROM CatNacionalidadInpamex c WHERE c.clave = :clave")
	public Optional<CatNacionalidadInpamex> findByClave(String clave);
	
	@Query(value = "SELECT c FROM CatNacionalidadInpamex c WHERE c.estatus = :estatus")
	public Optional<List<CatNacionalidadInpamex>> findByEstatus(Character estatus);

}
