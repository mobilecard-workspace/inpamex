package com.addcel.inpamex.data.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.addcel.inpamex.data.entity.TransferenciasInpamexTemp;

@Repository
public interface TransferenciasInpamexTempDao extends JpaRepository<TransferenciasInpamexTemp, Long> {

	@Query(value = "SELECT c FROM TransferenciasInpamexTemp c WHERE c.idMto = :idMto AND c.estatus = :estatus")
	public Optional<TransferenciasInpamexTemp> findTransferenciaIdMto(@Param("idMto") String idMto, @Param("estatus") Character estatus);
	
}
