package com.addcel.inpamex.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.addcel.inpamex.data.entity.TarjetaInpamex;

@Repository
public interface TarjetaInpamexDao extends JpaRepository<TarjetaInpamex, Long> {
	
	@Query("SELECT c FROM TarjetaInpamex c WHERE c.numero = ?1")
	public Optional<TarjetaInpamex> findTarjetaNumero(String noTarjeta);
	
	@Query("SELECT c FROM TarjetaInpamex c WHERE c.numero = :parametro OR c.codigoBarras = :parametro")
	public Optional<TarjetaInpamex> findTarjetaNumeroCodigoBarra(@Param("parametro") String parametro);
	
	@Query("SELECT c FROM TarjetaInpamex c WHERE c.codigoBarras = ?1")
	public Optional<TarjetaInpamex> findTarjetaCodigoBarras(String codigoBarras);
	
	@Query("SELECT c FROM TarjetaInpamex c WHERE c.codigoBarras = :codigoBarras AND c.claveCadena = :claveCadena")
	public Optional<TarjetaInpamex> findTarjetaCodigoBarrasClaveCadena(@Param("codigoBarras") String codigoBarras, @Param("claveCadena") String claveCadena);
	
	@Query("SELECT c FROM TarjetaInpamex c WHERE c.idUsuario = ?1")
	public Optional<TarjetaInpamex> findTarjetaByUsuario(Long idUsuario);
	
	@Query("SELECT c FROM TarjetaInpamex c WHERE c.idUsuario = ?1 AND c.estatus = 'A'")
	public Optional<TarjetaInpamex> findTarjetaByUsuarioActiva(Long idUsuario);
	
	@Query("SELECT c FROM TarjetaInpamex c WHERE c.idUsuario = :idUsuario AND c.idTarjeta= :idTarjeta AND c.estatus = 'A'")
	public Optional<TarjetaInpamex> buscarTarjetaActiva(@Param("idUsuario") Long idUsuario, @Param("idTarjeta") Long idTarjeta);
	
	@Query("SELECT c FROM TarjetaInpamex c WHERE c.idTarjeta= :idTarjeta")
	public Optional<TarjetaInpamex> buscarTarjetaId(@Param("idTarjeta") Long idTarjeta);
	
	@Query("SELECT c FROM TarjetaInpamex c WHERE c.idUsuario = :idUsuario")
	public Optional<List<TarjetaInpamex>> findTarjetasUsuario(@Param("idUsuario") Long idUsuario);
	
}
