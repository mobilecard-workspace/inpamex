package com.addcel.inpamex.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.addcel.inpamex.data.entity.TransferenciasInpamex;

@Repository
public interface TransferenciasInpamexDao extends JpaRepository<TransferenciasInpamex, Long> {
	
	@Query(value = "SELECT c FROM TransferenciasInpamex c WHERE c.idMto = :idMto")
	public Optional<TransferenciasInpamex> findTransferenciaIdMto(@Param("idMto") String idMto);
	
	@Query(value = "SELECT * FROM transferencias_inpamex c WHERE c.estatus = ?1 AND c.fecha LIKE ?2% ORDER BY c.id_transferencia ASC", nativeQuery = true)
	public List<Optional<TransferenciasInpamex>> findTransfersVencidas(Character estatus, String fecha);

}
