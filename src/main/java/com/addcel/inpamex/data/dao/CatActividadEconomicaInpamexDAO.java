package com.addcel.inpamex.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.addcel.inpamex.data.entity.CatActividadEconomicaInpamex;

@Repository
public interface CatActividadEconomicaInpamexDAO extends JpaRepository<CatActividadEconomicaInpamex, Long> {

	@Query(value = "SELECT c FROM CatActividadEconomicaInpamex c WHERE c.clave = :clave")
	public Optional<CatActividadEconomicaInpamex> findByClave(String clave);
	
	@Query(value = "SELECT c FROM CatActividadEconomicaInpamex c WHERE c.estatus = :estatus")
	public Optional<List<CatActividadEconomicaInpamex>> findByEstatus(Character estatus);
	
}
