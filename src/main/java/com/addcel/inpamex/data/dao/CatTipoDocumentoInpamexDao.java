package com.addcel.inpamex.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.addcel.inpamex.data.entity.CatTipoDocumentoInpamex;

@Repository
public interface CatTipoDocumentoInpamexDao extends JpaRepository<CatTipoDocumentoInpamex, Long> {
	
	@Query(value = "SELECT c FROM CatTipoDocumentoInpamex c WHERE c.clave = :clave")
	public Optional<CatTipoDocumentoInpamex> findByClave(String clave);
	
	@Query(value = "SELECT c FROM CatTipoDocumentoInpamex c WHERE c.estatus = :estatus")
	public Optional<List<CatTipoDocumentoInpamex>> findByEstatus(Character estatus);

}
