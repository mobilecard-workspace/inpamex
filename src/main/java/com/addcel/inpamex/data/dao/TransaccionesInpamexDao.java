package com.addcel.inpamex.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.addcel.inpamex.data.entity.TransaccionesInpamex;

@Repository
public interface TransaccionesInpamexDao extends JpaRepository<TransaccionesInpamex, Long> {

	@Query("SELECT c FROM TransaccionesInpamex c WHERE c.idTarjeta = :idTarjeta AND c.idUsuario = :idUsuario")
	public Optional<List<TransaccionesInpamex>> findTransaccions(@Param("idTarjeta") Long idTarjeta, @Param("idUsuario") Long idUsuario);

	@Query("SELECT c FROM TransaccionesInpamex c WHERE c.idTarjeta = :idTarjeta")
	public Optional<List<TransaccionesInpamex>> findTransaccionsByTarjeta(@Param("idTarjeta") Long idTarjeta);

	public TransaccionesInpamex findByNumeroAutorizacion(String numero);

	@Modifying
	@Query("update TransaccionesInpamex tp set tp.estatusDescripcion = :devolucion where tp.numeroAutorizacion = :numero")
	public void actualizaEstadoDevolucion(@Param("numero")String numero, @Param("devolucion") String devolucion);


	
}
