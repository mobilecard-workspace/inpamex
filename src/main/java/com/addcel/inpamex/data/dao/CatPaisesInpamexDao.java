package com.addcel.inpamex.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.addcel.inpamex.data.entity.CatPaisesInpamex;

@Repository
public interface CatPaisesInpamexDao extends JpaRepository<CatPaisesInpamex, Long> {
	
	@Query(value = "SELECT c FROM CatPaisesInpamex c WHERE c.clave = :clave")
	public Optional<CatPaisesInpamex> findByClave(String clave);
	
	@Query(value = "SELECT c FROM CatPaisesInpamex c WHERE c.estatus = :estatus")
	public Optional<List<CatPaisesInpamex>> findByEstatus(Character estatus);

}
