package com.addcel.inpamex.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "cat_tipo_documento_inpamex")
@Setter
@Getter
@ToString
public class CatTipoDocumentoInpamex implements Serializable {
	
	private static final long serialVersionUID = -2648370556116778024L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;

	@NotEmpty
	@Size(max = 128)
	@Column(name = "clave")
	private String clave;

	@NotEmpty
	@Size(max = 512)
	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "estatus")
	private Character estatus;

}
