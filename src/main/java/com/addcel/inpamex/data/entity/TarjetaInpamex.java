package com.addcel.inpamex.data.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "tarjetas_inpamex")
@Setter
@Getter
@ToString
public class TarjetaInpamex implements Serializable {
	
	private static final long serialVersionUID = -8806855603774437176L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tarjeta", nullable = false)
	private Long idTarjeta;
	
	@Min(value = 0)
	@Column(name = "id_usuario")
	private Long idUsuario;

	@NotEmpty
	@Size(max = 512)
	@Column(name = "numero")
	private String numero;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "serie")
	private String serie;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "codigo_barras")
	private String codigoBarras;
	
	@Size(max = 512)
	@Column(name = "codigo_verificacion")
	private String codigoVerificacion;
	
	@Min(value = 0)
	@Column(name = "saldo")
	private Double saldo;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "clave_cadena")
	private String claveCadena;
	
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_registro", nullable = false)
	private Date fechaRegistro;
	
	@Column(name = "estatus")
	private Character estatus;
	
	@NotEmpty
	@Size(max = 128)
	@Column(name = "estatus_descripcion")
	private String estatusDescripcion;
	
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_modificacion", nullable = false)
	private Date fechaModificacion;

}
