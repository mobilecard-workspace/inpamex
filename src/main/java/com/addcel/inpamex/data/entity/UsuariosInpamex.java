package com.addcel.inpamex.data.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "usuarios_inpamex")
@Setter
@Getter
@ToString
public class UsuariosInpamex implements Serializable {
	
	private static final long serialVersionUID = -7351616101702122822L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario", nullable = false)
	private Long idUsuario;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "nombre")
	private String nombre;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "apaterno")
	private String apaterno;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "amaterno")
	private String amaterno;

	@NotNull
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_nacimiento", nullable = false)
	private Date fechaNacimiento;
	
	@Column(name = "nacionalidad")
	private Integer nacionalidad;
	
	@NotEmpty
	@Size(max = 3)
	@Column(name = "pais_nacimiento")
	private String paisNacimiento;
	
	@Size(max = 50)
	@Column(name = "estado_nacimiento")
	private String estadoNacimiento;
	
	@NotEmpty
	@Size(max = 1)
	@Column(name = "genero")
	private String genero;
	
	@NotEmpty
	@Size(max = 255)
	@Column(name = "domicilio_calle")
	private String domicilioCalle;
	
	@NotEmpty
	@Size(max = 80)
	@Column(name = "domicilio_noexterior")
	private String domicilioNoExterior;
	
	@Size(max = 80)
	@Column(name = "domicilio_nointerior")
	private String domicilioNoInterior;
	
	@NotEmpty
	@Size(max = 255)
	@Column(name = "domicilio_municipio")
	private String domicilioMunicipio;
	
	@NotEmpty
	@Size(max = 255)
	@Column(name = "domicilio_colonia")
	private String domicilioColonia;
	
	@Size(max = 5)
	@Column(name = "domicilio_cp")
	private String domicilioCp;
	
	@NotEmpty
	@Size(max = 50)
	@Column(name = "domicilio_ciudad")
	private String domicilioCiudad;
	
	@NotEmpty
	@Size(max = 32)
	@Column(name = "domicilio_estado")
	private String domicilioEstado;
	
	@NotEmpty
	@Size(max = 3)
	@Column(name = "domicilio_pais")
	private String domicilioPais;
	
	@NotEmpty
	@Size(max = 16)
	@Column(name = "telefono")
	private String telefono;

	@Size(max = 32)
	@Column(name = "curp")
	private String curp;
	
	@Size(max = 32)
	@Column(name = "rfc")
	private String rfc;
	
	@Size(max = 255)
	@Column(name = "email")
	private String email;
	
	@NotEmpty
	@Size(max = 7)
	@Column(name = "clave_actividad_economica")
	private String claveActividadEconomica;
	
	@NotEmpty
	@Size(max = 50)
	@Column(name = "tipo_documento")
	private String tipoDocumento;
	
	@NotEmpty
	@Size(max = 50)
	@Column(name = "numero_documento")
	private String noDocumento;
	
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_registro", nullable = false)
	private Date fechaRegistro;

	@Column(name = "estatus")
	private Character estatus;
	
	@Size(max = 128)
	@Column(name = "estatus_descripcion")
	private String estatusDescripcion;
	
	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_modificacion", nullable = false)
	private Date fechaModificacion;

}
