package com.addcel.inpamex.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "transferencias_inpamex_temp")
@Setter
@Getter
@ToString
public class TransferenciasInpamexTemp implements Serializable {
	
	private static final long serialVersionUID = 4588725589957676018L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_transferencia", nullable = false)
	private Long idTransferencia;
	
	@Min(value = 0)
	@Column(name = "id_usuario")
	private Long idUsuario;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "id_mto")
	private String idMto;
	
	@Column(name = "estatus")
	private Character estatus;

}
