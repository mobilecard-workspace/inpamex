package com.addcel.inpamex.data.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "transferencias_inpamex")
@Setter
@Getter
@ToString
public class TransferenciasInpamex implements Serializable {

	private static final long serialVersionUID = 1166485383753595858L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_transferencia", nullable = false)
	private Long idTransferencia;
	
	@Min(value = 0)
	@Column(name = "id_usuario")
	private Long idUsuario;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "id_mto")
	private String idMto;
	
	@Min(value = 0)
	@Column(name = "id_tarjeta")
	private Long idTarjeta;
	
	@NotEmpty
	@Size(max = 256)
	@Column(name = "numero_autorizacion")
	private String numeroAutorizacion;
	
	@Column(name = "monto_original")
	private Double montoOriginal;
	
	@Column(name = "monto_final")
	private Double montoFinal;
	
	@Column(name = "tipo_cambio")
	private Double tipoCambio;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha", nullable = false)
	private Date fecha;

	@Column(name = "estatus")
	private Character estatus;

	@Size(max = 128)
	@Column(name = "estatus_descripcion")
	private String estatusDescripcion;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "remitente_nombre")
	private String remitenteNombre;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "remitente_apaterno")
	private String remitenteApaterno;
	
	@Size(max = 512)
	@Column(name = "remitente_amaterno")
	private String remitenteAmaterno;
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "beneficiario_nombre")
	private String beneficiarioNombre; 
	
	@NotEmpty
	@Size(max = 512)
	@Column(name = "beneficiario_apaterno")
	private String beneficiarioApaterno; 
	
	@Size(max = 512)
	@Column(name = "beneficiario_amaterno")
	private String beneficiarioAmaterno; 
	
	@Column(name = "comentario")
	private String comentario;
	
	@Column(name = "inpamex_estatus")
	private String inpamexEstatus;
	
	@Size(max = 512)
	@Column(name = "inpamex_estatus_desc")
	private String inpamexEstatusDesc;

	@Column(name = "inpamex_request")
	private String inpamexRequest;
	
	@Column(name = "inpamex_response")
	private String inpamexResponse;
	
}
