package com.addcel.inpamex.data.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "transacciones_inpamex")
@Setter
@Getter
@ToString
public class TransaccionesInpamex implements Serializable {

	private static final long serialVersionUID = 7094664868600203163L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_transaccion", nullable = false)
	private Long idTransaccion;
	
//	@NotNull
//	@ManyToOne(optional = false)
//	@JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario", nullable = false)
//	private UsuariosInpamex usuariosInpamex;
	
	@Min(value = 0)
	@Column(name = "id_usuario")
	private Long idUsuario;
	
	@Min(value = 0)
	@Column(name = "id_transferencia")
	private Long idTransferencia;
	
	@Min(value = 0)
	@Column(name = "id_tarjeta")
	private Long idTarjeta;
	
	@NotNull
	@Size(max = 512)
	@Column(name = "id_mto")
	private String idMto;
	
	@NotNull
	@Size(max = 512)
	@Column(name = "tipo")
	private String tipo;
	
	@NotNull
	@Column(name = "monto")
	private Double monto;
	
	@Column(name = "concepto")
	private String concepto;
	
	@Column(name = "clave_cadena")
	private String claveCadena;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha", nullable = false)
	private Date fecha;

	@Column(name = "estatus")
	private Character estatus;

	@Size(max = 128)
	@Column(name = "estatus_descripcion")
	private String estatusDescripcion;
	
	@NotNull
	@Size(max = 256)
	@Column(name = "numero_autorizacion")
	private String numeroAutorizacion;

	@Column(name = "comentario")
	private String comentario;
	
}
