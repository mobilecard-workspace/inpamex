package com.addcel.inpamex.controller;

import javax.validation.constraints.Min;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.inpamex.payload.request.TransaccionRequest;
import com.addcel.inpamex.payload.response.GeneralResponse;
import com.addcel.inpamex.payload.response.TransaccionFullResponse;
import com.addcel.inpamex.service.TransaccionService;

@RestController
@RequestMapping("/api/transaccion")
@Validated
public class TransaccionController {
	
	private final static Logger logger = Logger.getLogger(TransaccionController.class);
	
	@Value("${error.codigo.badrequest}")
	private String errorCodigoBadRequest;

	@Value("${error.mensaje.badrequest}")
	private String errorMensajeBadRequest;

	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;

	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;
	
	@Autowired
	private TransaccionService transaccionService;
	
	@GetMapping("/consulta/tarjeta")
	public ResponseEntity<TransaccionFullResponse> findTransaccionsByTarjeta(
			@RequestParam(required = true) @Min(value = 0, message = "EL VALOR idTarjeta DEBE SER MAYOR A 0") Long idTarjeta
			) {

		ResponseEntity<TransaccionFullResponse> responseEntity = null;

		try {
			responseEntity = transaccionService.findTransaccionsByTarjeta(idTarjeta);
		} catch (Exception e) {

			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE TRANSACCIONES POR TARJETA IDTARJETA CONTROLLER - " + e.getLocalizedMessage());
			TransaccionFullResponse transaccionFullResponse = new TransaccionFullResponse();
			transaccionFullResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			transaccionFullResponse.setMensajeError(errorMensajeErrorInterno);
			return new ResponseEntity<TransaccionFullResponse>(transaccionFullResponse, HttpStatus.OK);
		}
		return responseEntity;
	}
	
	@PutMapping(value = "/devolucion")
	public ResponseEntity<GeneralResponse> devolucion(@RequestBody TransaccionRequest request) {
		logger.info("Inicia el servicio de devolucion "+request.toString());
		return transaccionService.transaccionDevolucion(request);
	}

}
