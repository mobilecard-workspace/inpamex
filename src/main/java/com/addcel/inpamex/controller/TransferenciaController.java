package com.addcel.inpamex.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.inpamex.InpamexApplication;
import com.addcel.inpamex.payload.request.Transferencia;
import com.addcel.inpamex.payload.request.TransferenciaAbonar;
import com.addcel.inpamex.payload.response.TransferenciaIndividualResponse;
import com.addcel.inpamex.payload.response.TransferenciaResponse;
import com.addcel.inpamex.service.TransferenciaService;

@RestController
@RequestMapping("/api/transferencia")
public class TransferenciaController {
	
	private final static Logger logger = Logger.getLogger(InpamexApplication.class);
	
	@Value("${error.codigo.badrequest}")
	private String errorCodigoBadRequest;
	
	@Value("${error.mensaje.badrequest}")
	private String errorMensajeBadRequest;
	
	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;
	
	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;
	
	@Autowired
	private TransferenciaService transferenciaService;
	
	@PostMapping("/nueva")
	public ResponseEntity<TransferenciaResponse> doTransferencia(@Valid @RequestBody Transferencia transferencia, Errors errors) {
		
		ResponseEntity<TransferenciaResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				TransferenciaResponse transferenciaResponse = new TransferenciaResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION TRANSFERENCIA NUEVA - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				transferenciaResponse.setErrors(listaError);
				transferenciaResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				transferenciaResponse.setMensajeError(errorMensajeBadRequest);
				responseEntity = new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = transferenciaService.doTransferencia(transferencia);
		    }
		} catch (Exception e) {
			logger.error("ERROR AL REALIZAR LA TRANSFERENCIA DOTRANSFERENCIA CONTROLLER - " + e.getLocalizedMessage());
			TransferenciaResponse transferenciaResponse = new TransferenciaResponse();
			transferenciaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			transferenciaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
	}
	
	@GetMapping("/consulta")
	public ResponseEntity<TransferenciaIndividualResponse> getBalanceTarjetaCodigoBarras(
			@RequestParam(required = true) @NotEmpty(message = "EL VALOR idMto DE TRANSFERENCIA ES REQUERIDO") String idMto
			) {

		ResponseEntity<TransferenciaIndividualResponse> responseEntity = null;

		try {
			responseEntity = transferenciaService.getTransferencia(idMto);
		} catch (Exception e) {

			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE TRANSFERENCIA POR IDMTO - " + e.getLocalizedMessage());
			TransferenciaIndividualResponse tarjetaResponse = new TransferenciaIndividualResponse();
			tarjetaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TransferenciaIndividualResponse>(tarjetaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PostMapping("/abonar")
	public ResponseEntity<TransferenciaResponse> addTransferencia(@Valid @RequestBody TransferenciaAbonar transferencia, Errors errors) {
		
		ResponseEntity<TransferenciaResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				TransferenciaResponse transferenciaResponse = new TransferenciaResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION ABONAR TRANSFERENCIA - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				transferenciaResponse.setErrors(listaError);
				transferenciaResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				transferenciaResponse.setMensajeError(errorMensajeBadRequest);
				responseEntity = new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = transferenciaService.addTransferencia(transferencia);
		    }
		} catch (Exception e) {
			logger.error("ERROR AL ABONAR LA TRANSFERENCIA ADDTRANSFERENCIA CONTROLLER - " + e.getLocalizedMessage());
			TransferenciaResponse transferenciaResponse = new TransferenciaResponse();
			transferenciaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			transferenciaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TransferenciaResponse>(transferenciaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return responseEntity;
	}

}
