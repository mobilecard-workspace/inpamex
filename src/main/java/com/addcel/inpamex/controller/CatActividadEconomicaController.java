package com.addcel.inpamex.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.inpamex.payload.request.CatalogoRequest;
import com.addcel.inpamex.payload.request.ListaCatalogos;
import com.addcel.inpamex.payload.response.CatalogoResponse;
import com.addcel.inpamex.service.catalogos.CatActividadEconomicaService;

@RestController
@RequestMapping("/api/catalogo/actividadEconomica")
@Validated
public class CatActividadEconomicaController {
	
	private final static Logger logger = Logger.getLogger(CatActividadEconomicaController.class);
	
	@Value("${error.codigo.badrequest}")
	private String errorCodigoBadRequest;

	@Value("${error.mensaje.badrequest}")
	private String errorMensajeBadRequest;

	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;

	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;
	
	@Autowired
	private CatActividadEconomicaService catActividadEconomicaService;
	
	@PostMapping("/agregar")
	public ResponseEntity<CatalogoResponse> save(@Valid @RequestBody ListaCatalogos listaCatalogos, Errors errors) {
		
		ResponseEntity<CatalogoResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				CatalogoResponse catalogoResponse = new CatalogoResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION AGREGAR CATALOGO - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				catalogoResponse.setErrors(listaError);
				catalogoResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				catalogoResponse.setMensajeError(errorMensajeBadRequest);

				responseEntity = new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = catActividadEconomicaService.save(listaCatalogos);
		    }
		} catch (Exception e) {
			logger.error("ERROR AL AGREGAR CATALOGOS - " + e.getLocalizedMessage());
			CatalogoResponse catalogoResponse = new CatalogoResponse();
			catalogoResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			catalogoResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PostMapping("/agregar/individual")
	public ResponseEntity<CatalogoResponse> saveSingle(@Valid @RequestBody CatalogoRequest catalogo, Errors errors) {
		
		ResponseEntity<CatalogoResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				CatalogoResponse catalogoResponse = new CatalogoResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION AGREGAR CATALOGO - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				catalogoResponse.setErrors(listaError);
				catalogoResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				catalogoResponse.setMensajeError(errorMensajeBadRequest);

				responseEntity = new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = catActividadEconomicaService.saveSingle(catalogo);
		    }
		} catch (Exception e) {
			logger.error("ERROR AL AGREGAR CATALOGOS - " + e.getLocalizedMessage());
			CatalogoResponse catalogoResponse = new CatalogoResponse();
			catalogoResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			catalogoResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/consulta")
	public ResponseEntity<CatalogoResponse> findByEstatus() {

		ResponseEntity<CatalogoResponse> responseEntity = null;

		try {
			responseEntity = catActividadEconomicaService.findByEstatusActivo();
		} catch (Exception e) {

			logger.error(
					"ERROR AL REALIZAR LA CONSULTA CATALOGO CONTROLLER - " + e.getLocalizedMessage());
			CatalogoResponse catalogoResponse = new CatalogoResponse();
			catalogoResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			catalogoResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<CatalogoResponse>(catalogoResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

}
