package com.addcel.inpamex.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.inpamex.InpamexApplication;
import com.addcel.inpamex.payload.request.Usuario;
import com.addcel.inpamex.payload.request.UsuarioUpdate;
import com.addcel.inpamex.payload.request.idUsuario;
import com.addcel.inpamex.payload.response.UsuarioEntityResponse;
import com.addcel.inpamex.payload.response.UsuarioResponse;
import com.addcel.inpamex.payload.response.UsuariosEntityResponse;
import com.addcel.inpamex.service.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
@Validated
public class UsuarioController {
	
	private final static Logger logger = Logger.getLogger(InpamexApplication.class);
	
	@Value("${error.codigo.badrequest}")
	private String errorCodigoBadRequest;
	
	@Value("${error.mensaje.badrequest}")
	private String errorMensajeBadRequest;
	
	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;
	
	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("/nuevo")
	public ResponseEntity<UsuarioResponse> saveUsuario(@Valid @RequestBody Usuario usuario, Errors errors) {
		
		ResponseEntity<UsuarioResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				UsuarioResponse usuarioResponse = new UsuarioResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION GUARDAR USUARIO - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				usuarioResponse.setErrors(listaError);
				usuarioResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				usuarioResponse.setMensajeError(errorMensajeBadRequest);
				responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.BAD_REQUEST);
			} else {
				responseEntity = usuarioService.saveUsuario(usuario);
			}
		} catch (Exception e) {
			logger.error("ERROR AL GUARDAR EL USUARIO SAVEUSUARIO CONTROLLER - " + e.getLocalizedMessage());
			UsuarioResponse usuarioResponse = new UsuarioResponse();
			usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PostMapping("/actualizar")
	public ResponseEntity<UsuarioResponse> updateUsuario(@Valid @RequestBody UsuarioUpdate usuario, Errors errors) {
		
		ResponseEntity<UsuarioResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				UsuarioResponse usuarioResponse = new UsuarioResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION ACTUALIZAR USUARIO - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				usuarioResponse.setErrors(listaError);
				usuarioResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				usuarioResponse.setMensajeError(errorMensajeBadRequest);
				responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.BAD_REQUEST);
			} else {
				responseEntity = usuarioService.updateUsuario(usuario);
			}
		} catch (Exception e) {
			logger.error("ERROR AL ACTUALIZAR EL USUARIO UPDATE SUARIO CONTROLLER - " + e.getLocalizedMessage());
			UsuarioResponse usuarioResponse = new UsuarioResponse();
			usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PostMapping("/deshabilitar")
	public ResponseEntity<UsuarioResponse> disableUsuario(@Valid @RequestBody idUsuario usuario, Errors errors) {
		
		ResponseEntity<UsuarioResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				UsuarioResponse usuarioResponse = new UsuarioResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION DESHABILITAR USUARIO - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				usuarioResponse.setErrors(listaError);
				usuarioResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				usuarioResponse.setMensajeError(errorMensajeBadRequest);
				responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = usuarioService.disableUsuario(usuario.getIdUsuario());
		    }
		} catch (Exception e) {
			
			logger.error("ERROR AL REALIZAR PETICION DESHABILITAR USUARIO CONTROLLER - " + e.getLocalizedMessage());
			UsuarioResponse usuarioResponse = new UsuarioResponse();
			usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PostMapping("/habilitar")
	public ResponseEntity<UsuarioResponse> enableUsuario(@Valid @RequestBody idUsuario usuario, Errors errors) {
		
		ResponseEntity<UsuarioResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				UsuarioResponse usuarioResponse = new UsuarioResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION HABILITAR USUARIO - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				usuarioResponse.setErrors(listaError);
				usuarioResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				usuarioResponse.setMensajeError(errorMensajeBadRequest);
				responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = usuarioService.enableUsuario(usuario.getIdUsuario());
		    }
		} catch (Exception e) {
			
			logger.error("ERROR AL REALIZAR PETICION HABILITAR USUARIO CONTROLLER - " + e.getLocalizedMessage());
			UsuarioResponse usuarioResponse = new UsuarioResponse();
			usuarioResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioResponse>(usuarioResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/consulta")
	public ResponseEntity<UsuarioEntityResponse> getUsuario(
			@RequestParam(required = true) @NotNull(message = "EL VALOR idUsuario DE TARJETA ES REQUERIDO") @Min(value = 1, message = "EL VALOR idUsuario DE TARJETA DEBE SER MAYOR O IGUAL A 1") Long idUsuario) {

		ResponseEntity<UsuarioEntityResponse> responseEntity = null;

		try {
			responseEntity = usuarioService.getUsuario(idUsuario);
		} catch (Exception e) {

			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE USUARIO CONTROLLER - " + e.getLocalizedMessage());
			UsuarioEntityResponse usuarioEntityResponse = new UsuarioEntityResponse();
			usuarioEntityResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioEntityResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioEntityResponse>(usuarioEntityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/consultaCurp")
	public ResponseEntity<UsuarioEntityResponse> getUsuarioCurp(
			@RequestParam(required = true) @NotNull(message = "EL CURP ES REQUERIDO") String curp) {

		ResponseEntity<UsuarioEntityResponse> responseEntity = null;

		try {
			responseEntity = usuarioService.getUsuario(curp);
		} catch (Exception e) {

			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE USUARIO CONTROLLER - " + e.getLocalizedMessage());
			UsuarioEntityResponse usuarioEntityResponse = new UsuarioEntityResponse();
			usuarioEntityResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuarioEntityResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuarioEntityResponse>(usuarioEntityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/todo")
	public ResponseEntity<UsuariosEntityResponse> getUsuario() {

		ResponseEntity<UsuariosEntityResponse> responseEntity = null;

		try {
			responseEntity = usuarioService.getUsuarios();
		} catch (Exception e) {

			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE USUARIO CONTROLLER - " + e.getLocalizedMessage());
			UsuariosEntityResponse usuariosEntityResponse = new UsuariosEntityResponse();
			usuariosEntityResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			usuariosEntityResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<UsuariosEntityResponse>(usuariosEntityResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	

}
