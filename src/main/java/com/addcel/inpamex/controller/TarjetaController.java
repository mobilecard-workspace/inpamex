package com.addcel.inpamex.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.inpamex.InpamexApplication;
import com.addcel.inpamex.payload.request.ListaTarjetas;
import com.addcel.inpamex.payload.request.Tarjeta;
import com.addcel.inpamex.payload.request.TarjetaAsignar;
import com.addcel.inpamex.payload.response.TarjetaAsignResponse;
import com.addcel.inpamex.payload.response.TarjetaResponse;
import com.addcel.inpamex.payload.response.TarjetaTransaccionCodigoBarras;
import com.addcel.inpamex.payload.response.TarjetasGeneralResponse;
import com.addcel.inpamex.payload.response.TransaccionResponse;
import com.addcel.inpamex.service.TarjetaService;

@RestController
@RequestMapping("/api/tarjeta")
@Validated
public class TarjetaController {

	private final static Logger logger = Logger.getLogger(InpamexApplication.class);

	@Value("${error.codigo.badrequest}")
	private String errorCodigoBadRequest;

	@Value("${error.mensaje.badrequest}")
	private String errorMensajeBadRequest;

	@Value("${error.codigo.errorinterno}")
	private String errorCodigoErrorInterno;

	@Value("${error.mensaje.errorinterno}")
	private String errorMensajeErrorInterno;

	@Autowired
	private TarjetaService tarjetaService;
	
	@GetMapping("/consulta/saldo/codigoBarras")
	public ResponseEntity<TarjetaResponse> getBalanceTarjetaCodigoBarras(
			@RequestParam(required = true) @NotEmpty(message = "EL VALOR codigoBarras DE TARJETA ES REQUERIDO") String codigoBarras,
			@RequestParam(required = true) @NotEmpty(message = "EL VALOR claveCadena DE TARJETA ES REQUERIDO") String claveCadena
			) {

		ResponseEntity<TarjetaResponse> responseEntity = null;

		try {
			responseEntity = tarjetaService.getBalanceTarjetaCodigoBarras(codigoBarras, claveCadena);
		} catch (Exception e) {

			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE SALDO POR CODIGO BARRAS GETBALANCETARJETA CONTROLLER - " + e.getLocalizedMessage());
			TarjetaResponse tarjetaResponse = new TarjetaResponse();
			tarjetaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetaResponse>(tarjetaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@GetMapping("/consulta/numero")
	public ResponseEntity<TarjetaResponse> findTarjetaNumero(
			@RequestParam(required = true) @NotEmpty(message = "EL VALOR numero DE TARJETA ES REQUERIDO") String numero
			) {

		ResponseEntity<TarjetaResponse> responseEntity = null;

		try {
			responseEntity = tarjetaService.findTarjetaNumero(numero);
		} catch (Exception e) {

			logger.error(
					"ERROR AL REALIZAR LA CONSULTA DE TARJETA POR NUMERO CONTROLLER - " + e.getLocalizedMessage());
			TarjetaResponse tarjetaResponse = new TarjetaResponse();
			tarjetaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetaResponse>(tarjetaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PostMapping("/transaccion/codigoBarras")
	public ResponseEntity<TransaccionResponse> doTransaccionCodigoBarras( @RequestBody TarjetaTransaccionCodigoBarras transaccion, Errors errors) {
		
		ResponseEntity<TransaccionResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				TransaccionResponse transaccionResponse = new TransaccionResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION TRANSACCION NUEVA POR CODIGO BARRAS - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				transaccionResponse.setErrors(listaError);
				transaccionResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				transaccionResponse.setMensajeError(errorMensajeBadRequest);
				responseEntity = new ResponseEntity<TransaccionResponse>(transaccionResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = tarjetaService.doTransaccionCodigoBarras(transaccion);
		    }
		} catch (Exception e) {
			
			logger.error("ERROR AL REALIZAR LA TRANSACCION TRANSACCION NUEVA POR CODIGO BARRAS DOTRANSACCION CONTROLLER - " + e.getLocalizedMessage());
			TransaccionResponse transaccionResponse = new TransaccionResponse();
			transaccionResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			transaccionResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TransaccionResponse>(transaccionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PostMapping("/asignar")
	public ResponseEntity<TarjetaAsignResponse> asignTarjeta(@Valid @RequestBody TarjetaAsignar tarjetaAsignar, Errors errors) {
		
		ResponseEntity<TarjetaAsignResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				TarjetaAsignResponse tarjetaResponse = new TarjetaAsignResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION ASIGNAR TARJETA - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				tarjetaResponse.setErrors(listaError);
				tarjetaResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				tarjetaResponse.setMensajeError(errorMensajeBadRequest);
				responseEntity = new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = tarjetaService.asignTarjeta(tarjetaAsignar);
		    }
		} catch (Exception e) {
			
			logger.error("ERROR AL ASIGNAR TARJETA - " + e.getLocalizedMessage());
			TarjetaAsignResponse tarjetaResponse = new TarjetaAsignResponse();
			tarjetaResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetaResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetaAsignResponse>(tarjetaResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@PostMapping("/agregar")
	public ResponseEntity<TarjetasGeneralResponse> addTarjeta(@Valid @RequestBody ListaTarjetas tarjetas, Errors errors) {
		
		ResponseEntity<TarjetasGeneralResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION AGREGAR TARJETAS - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				tarjetasGeneralResponse.setErrors(listaError);
				tarjetasGeneralResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				tarjetasGeneralResponse.setMensajeError(errorMensajeBadRequest);

				responseEntity = new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = tarjetaService.saveTarjetas(tarjetas.getTarjetas());
		    }
		} catch (Exception e) {
			logger.error("ERROR AL AGREGAR TARJETAS - " + e.getLocalizedMessage());
			TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();
			tarjetasGeneralResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetasGeneralResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	@PostMapping("/agregar/individual")
	public ResponseEntity<TarjetasGeneralResponse> saveTarjeta(@Valid @RequestBody Tarjeta tarjeta, Errors errors) {
		
		ResponseEntity<TarjetasGeneralResponse> responseEntity = null;
		
		try {
			if (errors.hasErrors()) {
				TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();
				logger.info("SE PRESENTARON ERRORES DURANTE LA PETICION AGREGAR TARJETA - BAD REQUEST");
				List<String> listaError = new ArrayList<String>();
				for (ObjectError objectError : errors.getAllErrors()) {
					listaError.add(objectError.getDefaultMessage());
				}
				tarjetasGeneralResponse.setErrors(listaError);
				tarjetasGeneralResponse.setIdError(Integer.parseInt(errorCodigoBadRequest));
				tarjetasGeneralResponse.setMensajeError(errorMensajeBadRequest);

				responseEntity = new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse, HttpStatus.BAD_REQUEST);
		    } else {
		    	responseEntity = tarjetaService.saveTarjeta(tarjeta);
		    }
		} catch (Exception e) {
			logger.error("ERROR AL AGREGAR TARJETAS - " + e.getLocalizedMessage());
			TarjetasGeneralResponse tarjetasGeneralResponse = new TarjetasGeneralResponse();
			tarjetasGeneralResponse.setIdError(Integer.parseInt(errorCodigoErrorInterno));
			tarjetasGeneralResponse.setMensajeError(errorMensajeErrorInterno);
			responseEntity = new ResponseEntity<TarjetasGeneralResponse>(tarjetasGeneralResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
}
