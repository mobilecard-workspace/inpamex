package com.addcel.inpamex.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.inpamex.payload.response.HeartBeatResponse;

@RestController
@RequestMapping("/status")
public class AppController {
	
	private final static Logger logger = Logger.getLogger(AppController.class);
	
	@GetMapping("/")
	public ResponseEntity<HeartBeatResponse> healthz () {
		HeartBeatResponse heartBeatResponse = new HeartBeatResponse();
		heartBeatResponse.setIdError(0);
		heartBeatResponse.setMensajeError("SERVICIO INPAMEX ACTIVO");
		logger.info("SERVICIO INPAMEX ACTIVO");
		return new ResponseEntity<HeartBeatResponse>(heartBeatResponse, HttpStatus.OK);
	}

}
