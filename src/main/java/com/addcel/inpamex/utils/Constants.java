package com.addcel.inpamex.utils;

public class Constants {

	public final static String DEVOLUCION = "DEVOLUCION";
	public static final Integer ERROR_400 = 400;
	public static final String DESC_ERROR_400 = "Error en el número de autorizacion: ";
	public static final Integer ERROR_404 = 404;
	public static final String DESC_ERROR_404 = "No se encontro el número de autorizacion: ";
	public static final Integer ERROR_500 = 500;
	public static final String DESC_ERROR_500 = "Hubo el siguiente error interno: ";
	public static final String DESC_ERROR_400_MONTO = "Error en el monto: ";
	public static final Integer OK_200 = 200;
	public static final String OK_DEVOLUCION = "Exito, Se actualizo de forma correcta el estatus";
	
	
}
