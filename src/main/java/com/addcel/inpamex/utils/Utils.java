package com.addcel.inpamex.utils;

import java.time.ZonedDateTime;

import org.apache.log4j.Logger;

public class Utils {
	
	private final static Logger logger = Logger.getLogger(Utils.class);
	
	public String dateTimeMilliSecond() {
		String uniqueNumber = "";
		try {
			Long dateTimeMilli = ZonedDateTime.now().toInstant().toEpochMilli();
			uniqueNumber = dateTimeMilli.toString();
		} catch (Exception e) {
			logger.error("ERROR EN METODO  dateTimeMilliSecond - " + e.getMessage());
		}
		return uniqueNumber;
	}
	
	public String uniqueNumber(String uniqueNumber, String tipoMov) {
		String unique = "";
		String separator = "_";
		try {
			unique = tipoMov.concat(separator);
			unique = unique.concat(uniqueNumber);
		} catch (Exception e) {
			logger.error("ERROR EN METODO  uniqueNumber - " + e.getMessage());
		}
		return unique;
	}

	public String removerAcentos (String param) {
		
		String cadenaOriginal = "ÁáÉéÍíÓóÚúÜü";
		String cadenaFinal = "AaEeIiOoUuNnUu";
		String output = "";
		
		try {
			
			if (param!= null & !"".equals(param)) {
				
				char[] array = param.toCharArray();
			    for (int index = 0; index < array.length; index++) {
			        int pos = cadenaOriginal.indexOf(array[index]);
			        if (pos > -1) {
			            array[index] = cadenaFinal.charAt(pos);
			        }
			    }
			    output = new String(array);
			}
			
		} catch (Exception e) {
			output = param;
			logger.error("ERROR EN METODO  removerAcentos - " + e.getMessage());
		}
		
		return output;
	}
}
